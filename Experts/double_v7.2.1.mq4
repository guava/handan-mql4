//+------------------------------------------------------------------+
//--- v6
//|正确增倍
//|前天波动参照可设置
//--- v7
//--- 精简重复代码
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#include "../Libraries/guava/fhtz2OrderDoSend.mq4"
#include "../Libraries/guava/fhLocker.mq4"
#include "../Libraries/guava/temple.mq4"
#include "../Libraries/guava/fhTimeManager.mq4"
#include "../Libraries/guava/StopOrder.mq4"

#include "../Libraries/core/fh_system_bus.mq4"
#include "../Libraries/sarindicator/fhtz_Sar_indicator.mq4"
#include "../Libraries/sarindicator/fhtz_yesterday.mq4"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
input double volumePerOrder=1;//手数
double volumePerOrderNow;
input int maxSlippage=5;//最大滑点
input double mxxxxx=1;//反手获利倍数 (1.5)
input double volumeDouble=3.0;//订单量增倍

int maxOrderNumber=50;//最大单数(4)
int hourOfStartTime=2;//开始时间
input int hourOfEndTime=23;//结束时间

input bool closeOrderEveryday=true;//是否每日关闭订单

bool cycleFinished=false; //当天完成了购买周期
int orderTimesInCycle=0;
string dateSave;

string orderType;

int stoploss=150;
double profit=1000;
OrderDoSend *ods;
//bool pofectTime=false;

bool  orderOpeningNow=false;
int orderNumber;
double price;

double profit2;
//tomorrow higher-lower
double higher=0;
double   lower=100000;
int higher_low_point=1500000;//19-次日9点波动 默认1500，不采用该策略设置为1500000
double higher_lower_lock=false;
double higher_lower_too_big=false;

//变色操作相关
//bool bars_lock=false;
//int bars_now=0;
StopOrder *stopOrder;
Locker *locker;
int protect_close_ourSelf=23456;//变色平仓设23456  不变色平仓设100 

input int indicator_macd_time_frame=PERIOD_M15;//均线时间
input int indicator_macd_period=100;//均线周期
                                    //bus
//Sar_bus *sarbus;
BUS_System *bus_system;

TimeManager *time_manager;

YesterdaySituation *yesterday_situation;
int file_handle;
bool YesterdaySituation_isopen=true; //前日波动大于150是否开启
string method_volume="stoploss2volume"; //double normal stoploss2volume(已亏损/止盈点数=手数)

double pre_AccountProfit;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

int OnInit()
  {
//bbbbb(new CCat);
   bus_system=new Sar_bus();

   dateSave=TimeToStr(TimeCurrent(),TIME_DATE);
   ods=new OrderDoSend();
   profit2=profit;
   locker=new Locker();
   stopOrder=new StopOrder();
   time_manager=new TimeManager(hourOfStartTime,hourOfEndTime,closeOrderEveryday);

   yesterday_situation=new YesterdaySituation(time_manager,hourOfStartTime,higher_low_point,YesterdaySituation_isopen);

   file_handle=FileOpen("guava.csv",FILE_READ|FILE_WRITE|FILE_CSV);
   if(file_handle!=INVALID_HANDLE)
     {
      FileWrite(file_handle,"Time","RSI","MACD","ATR");
      FileWrite(file_handle,"2016-7-2 10:42:04","0.15","-0.4","56");
     }

   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
/*
void bbbbb(CAnimal *animal)
  {
   animal.Sound();
  }
  */
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   if(file_handle!=INVALID_HANDLE)
     {
      FileClose(file_handle);
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   locker.barsChanged();
//新柱要做的操作
   bus_system.barsReflash(locker);

//交易时间 自动平仓
   bus_system.timeManager(time_manager);
//对昨天数据处理
   yesterday_situation.everTickDo();

   double green1=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",0,1);
   double red1=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",1,1);
   double green2=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",0,2);
   double red2=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",1,2);
//double gmacd=iMACD(NULL,0,12,26,9,PRICE_CLOSE,MODE_MAIN,0);

/*
   int test_step=7;
   double c2=iCustom(NULL,0,"test",1,2);
   double c1=iCustom(NULL,0,"test",1,1);
   double c0=iCustom(NULL,0,"test",1,0);
   double o2=iCustom(NULL,0,"test",0,2);
   double o1=iCustom(NULL,0,"test",0,1);
   double o0=iCustom(NULL,0,"test",0,0);
*/
//double test_sr0=iCustom(NULL,0,"testregression",1,0);
//double test_sr1=iCustom(NULL,0,"testregression",1,1);
/*
   double test_sss[4];
   ArrayInitialize(test_sss,0);
   for(int gi=0;gi<4;gi++)
     {
      double indicate_macd=iCustom(NULL,0,"MACD",12,26,9,0,gi);
      double indicate_cci=iCustom(NULL,0,"CCI",14,0,gi);
      //double indicate_rsi=iCustom(NULL,0,"RSI",14,0,i);
      double indicate_atr=iCustom(NULL,0,"ATR",14,0,gi);
      double indicate_roc=iCustom(NULL,0,"ROC1",10,false,0,gi);
      //Print(indicate_macd+":"+indicate_cci+":"+indicate_atr+":"+indicate_roc+":");
      //double indicate_dma=iCustom(NULL,0,"DMA",3,3,7,5,25,5,0,1,1,1,2,i);
      test_sss[gi]=0.0050629793+0.0897455507*indicate_roc-0.0350771667*indicate_macd-0.0320918933*indicate_atr+0.0001599607*indicate_cci;
     }

   double test_sr0=(test_sss[0]+test_sss[1]+test_sss[2])/3;
   double test_sr1=(test_sss[3]+test_sss[1]+test_sss[2])/3;
*/
/*
   double indicate_rsi=iCustom(NULL,0,"RSI",14,0,0);
   double indicate_macd=iCustom(NULL,0,"MACD",12,26,9,0,0);
   double indicate_atr=iCustom(NULL,0,"ATR",14,0,0);
   Print(indicate_atr);
*/

//首次开仓
   double indicate_macd0=iCustom(NULL,0,"MACD",12,26,9,0,0);
   double indicate_macd1=iCustom(NULL,0,"MACD",12,26,9,0,1);
   double indicate_atr0=iCustom(NULL,0,"ATR",14,0,0);
   double indicate_testregression=iCustom(NULL,0,"test2",3,0);
   double indicate_testregression_yesterday=iCustom(NULL,0,"test2",3,1);
   double indicate_testregression_2=iCustom(NULL,0,"test2",3,2);
   if(time_manager.get_dateCurrent()==dateSave && !locker.get_lock())
     {
      //red2>0 green1>0 sell
      //Alert(yesterday_situation.get_is_higher_lower_too_big()+":"+time_manager.getPofectTime());
      //if(cycleFinished==false && yesterday_situation.get_is_higher_lower_too_big())

      if(yesterday_situation.get_is_higher_lower_too_big())
        {
         int total=OrdersTotal();

         //Print(indicate_testregression+":guava:"+indicate_testregression_yesterday);
/*
         if(total==0 && time_manager.getPofectTime() && orderTimesInCycle<maxOrderNumber && indicate_testregression<-0.01 && indicate_testregression<indicate_testregression_yesterday && 
            (indicate_macd0>0.10 ||indicate_macd0<-0.05) && (green1>0) && indicate_macd0<indicate_macd1)//&& gmacd>Bid
           {
            Print("cycle finished:"+cycleFinished+" (orderTimesInCycle):"+orderTimesInCycle);
            ods.SetOrderDoSend(volumePerOrder,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
            orderNumber=doSell();
            volumePerOrderNow=volumePerOrder;
            locker.set_lock();
           }else{
            //Print("guava:"+indicate_testregression);
           }
         */

         if(total==0 && time_manager.getPofectTime() && orderTimesInCycle<maxOrderNumber && indicate_testregression<-0.041 && indicate_testregression<indicate_testregression_yesterday && 
            (indicate_testregression_yesterday>0.00 || indicate_testregression_2>0.00 || true) && indicate_macd0<indicate_macd1)//&& gmacd>Bid
           {
            ods.SetOrderDoSend(volumePerOrder,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
            orderNumber=doSell();
            volumePerOrderNow=volumePerOrder;
            locker.set_lock();
              }else{
            //Print("guava:"+time_manager.getPofectTime());
           }

         if(total==0 && time_manager.getPofectTime() && orderTimesInCycle<maxOrderNumber && indicate_testregression>0.01 && indicate_testregression>indicate_testregression_yesterday && 
            indicate_macd0>-0.15 && (indicate_macd0<0.05) && (red1>0) && indicate_macd0>indicate_macd1 && false)//&& Ask>gmacd
           {

            //Alert("cycle finished:"+cycleFinished);
            ods.SetOrderDoSend(volumePerOrder,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
            orderNumber=doBuy();
            volumePerOrderNow=volumePerOrder;
            locker.set_lock();

              }else{
            //Print("buy:"+ (indicate_macd0>-0.18));
            //Print(indicate_macd0);
           }

        }
        }else{
      //开始第二天
      dateSave=time_manager.get_dateCurrent();
      cycleFinished=false;
      orderTimesInCycle=0;
      time_manager.setPorfectime(false);
      orderOpeningNow=false;
      //volumePerOrderNow=volumePerOrder;//非当日亏损是否double 不设置结果比较好
     }

//主动平仓
   int orderCounter1=OrdersTotal();
//if(orderCounter1==0)pre_AccountProfit=0;

   if(OrderSelect(0,SELECT_BY_POS)==true)
     {

      if(OrderType()==OP_BUY && indicate_testregression<0 && !locker.get_lock())
        {
         Print("test差变色"+AccountProfit());
         closeAllOrders();
        }
      if(OrderType()==OP_SELL && indicate_testregression>-0.011 && !locker.get_lock())
        {
         Print("test差变色"+AccountProfit());
         closeAllOrders();
        }

      //移动止损45points
      if(!locker.get_lock())stopOrder.flashPrice();
      //防止当日特大变动150点
      stopOrder.bigLostToday();

/*
      if(pre_AccountProfit>100*Point && AccountProfit()<100*Point)
        {
        Alert(pre_AccountProfit+":"+AccountProfit());
         closeAllOrders();
         pre_AccountProfit=0;
        }
       if(pre_AccountProfit<AccountProfit())pre_AccountProfit=AccountProfit();
      */
     }

  }
//+------------------------------------------------------------------+

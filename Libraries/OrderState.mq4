//+------------------------------------------------------------------+
//|                                                    OderState.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class OrderState
  {
protected:
   bool              hasOrder;
   int               orderType;

public:
   double            price;
   double            hight;
   double            low;
   int               orderNumber;
   int               trailingStop;
   void OrderState()
     {
      hasOrder=false;
      orderType=0;
      trailingStop=50;
      hight=0;
      low=0;
     }
   void set(bool hasorder,int ordertype)
     {
      hasOrder=hasorder;
      orderType=ordertype;
     }
   bool getHasorder()
     {
      return hasOrder;
     }
   int getOrderType()
     {
      return orderType;
     }
   void setOrderType(int n)
     {
      orderType=n;
     }
   void setHasOrder(bool n)
     {
      this.hasOrder=n;
     }

   bool reflashHL()
     {

      if(Ask>hight)hight=Ask;
      if(Bid<low)low=Bid;
      if(orderType>0)
        {
         //buy action
         if(((hight-Ask)>50*Point) && (Ask>price))
           {
            return true;
           }
           
      double L0=iCustom(NULL,0,"test",3,3,0);
      if(Ask<L0 && Ask<price)return true;
/*
         if((price-Ask)>trailingStop*Point && Ask<price)
           {
            return true;
           }
         */
        }

      return false;
     }

  };
//+------------------------------------------------------------------+

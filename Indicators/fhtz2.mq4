//修改内容：
//    1、增加了邮件提醒。
//
//修改时间：2016年1月20日 11:06:01
//



#property indicator_chart_window
//#property indicator_separate_window 
#property indicator_buffers 5
#property indicator_color1 Green
#property indicator_color2 Red
#property indicator_color3 Red
#property indicator_color4 Aqua



extern bool VCFX_ShowTime     = TRUE;           //显示背景时间    
extern bool VCFX_SoundWarning = TRUE;           //警告声音
extern int  vc_g_nStartTime   = 9;              //开始时间
extern int  vc_g_nEndTime     = 19;             //结束时间

extern bool  vc_g_bIsStartupMail   = false;         //开启邮件提醒
extern int  vc_g_nStartTimeMail   = 9;              //邮件发送时间
extern int  vc_g_nEndTimeMail     = 19;             //邮件结束时间

extern double  VCFX_Leverage  = 50.0;           //杠杆倍数
extern double  VCFX_BaseLots  = 0.2;            //下单量
extern double  vc_g_dTPP      = 30.0;           //黄金获利点数
extern double  VCFX_MaxLots   = 2.0;            //平台最大单笔下单量限制

extern string  VCFX_Symbol="GBPJPY";       //货币对

extern string MusicBuy  = "alert.wav";    //多单信号
extern string MusicSell = "news.wav";     //空单信号

#define  VC_LABLE_OFFSET       30             //买卖信号当前K线之间的距离

string gs_unused_180="方浩投资喊单系统";

int Timeframe=0;

int gi_220 = 7;
int gi_224 = 17;
int gi_228 = 6;
int g_str2int_232=30;
bool gi_unused_236=TRUE;
double vc_nSARStepSmall = 0.001;
double vc_nSARMaxValume = 0.2;

double vc_iGreen[];
double vc_iRed[];

double vc_iSell[];
double vc_iBuy[];

int gi_300=315532800;
int g_bars_304=240;
int gi_308 = 0;
int gi_312 = 0;
double gd_316=0.0;
int gi_324 = -1;
int gi_328 = -1;
int gi_332 = 0;
double g_price_336 = 0.0;
double g_price_344 = 0.0;
double g_price_352 = 0.0;
int gi_unused_360=0;
int g_bars_364=300;
int gi_368 = 0;
int gi_372 = 0;
double gd_376=0.0;
double gd_384;
int g_datetime_392;
int g_datetime_396;



int vc_g_nLableLot_X = 5;
int vc_g_nLableLot_Y = 100;
int vc_g_nLableLotVolume_X = 40;
int vc_g_nLableLotVolume_Y = 100;


string   VC_GOLD="GOLD";       //交易品种
double   vc_g_dAccumulatedLoss   = 0.0;     //亏损累计
double   vc_g_dFixedProfit       = 0.0;     //亏损累计

double   vc_TPP   = 30.0;  //获利点数
double   vc_STP   = -1.0;  //变色点数
//double   vc_Lots  = 0.2;   //应下手数

int vc_g_nLableTP_X = 5;
int vc_g_nLableTP_Y = 115;
int vc_g_nLableTPVolume_X = 40;
int vc_g_nLableTPVolume_Y = 115;

#define VC_WINGDINGS_SYMBOLS     167   //Z指标线符号
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {

   string ls_0;
   //ObjectsDeleteAll();
   if(StringLen(ls_0)>10) gi_unused_236=FALSE;
   g_str2int_232=StrToInteger(ls_0);
   SetIndexBuffer(0,vc_iGreen);
   SetIndexStyle(0,DRAW_ARROW);
   SetIndexArrow(0,VC_WINGDINGS_SYMBOLS);

   SetIndexBuffer(1,vc_iRed);
   SetIndexStyle(1,DRAW_ARROW);
   SetIndexArrow(1,VC_WINGDINGS_SYMBOLS);

   IndicatorShortName("开仓指标");

   SetIndexBuffer(2,vc_iBuy);
   SetIndexStyle(2,DRAW_ARROW,EMPTY,2);
   SetIndexArrow(2,233);

   SetIndexBuffer(3,vc_iSell);
   SetIndexStyle(3,DRAW_ARROW,EMPTY,2);
   SetIndexArrow(3,234);

   CTime();

   return (0);
  }

int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
                

  {

   int lia_4[];
   int lia_8[];
   double iMa_100;
   double diSAR_1;
   //int li_28;
   int index_32=0;
   int ind_counted_36=IndicatorCounted();
   int li_40=Bars-ind_counted_36;
   int index_44=0;

   for(index_32=0; index_44<li_40; index_44++)
     {

      iMa_100 = iMA(NULL, Timeframe, 50, 0, MODE_EMA, PRICE_MEDIAN, index_44 + 1);
      diSAR_1 = iSAR(NULL, Timeframe, vc_nSARStepSmall, vc_nSARMaxValume, index_44 + 1);
      //Print("0、ima_12 = ", ima_12, ", ----- ima_20 = ", ima_20); 
      VC_DrawHLine("VC_ChangeColor",diSAR_1);  //画止损线

      if(diSAR_1>High[index_44+1])//&& ima_12 <= ima_20 )
        {
         vc_iGreen[index_44]  = iMa_100;
         vc_iRed[index_44]    = -1;
        }
      else
        {
           {
            vc_iRed[index_44]    = iMa_100;
            vc_iGreen[index_44]  = -1;

           }
        }
     }

   return (0);
  }


void CTime()
  {
//if(GlobalVariableCheck("timezone")==FALSE) GlobalVariableSet("timezone",gi_228);
//gi_228 = GlobalVariableGet("timezone");
   gi_220 = 12 - gi_228;
   gi_224 = 22 - gi_228;
  }

void VC_DrawHLine(string strName,double price)
  {
   RefreshRates();
   ObjectDelete(strName);
   ObjectCreate(strName,OBJ_HLINE,0,0,price);
   ObjectSet(strName,OBJPROP_COLOR,Yellow);
   ObjectSet(strName,OBJPROP_STYLE,STYLE_DOT);
   ObjectSet(strName,OBJPROP_PRICE1,price);
   ObjectSet(strName,OBJPROP_WIDTH,1);
   ObjectSet(strName,OBJPROP_BACK,false);
   ObjectSet(strName,OBJPROP_RAY,false);
  }
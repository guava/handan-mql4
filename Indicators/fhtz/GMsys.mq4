//+------------------------------------------------------------------+
//|                                                        GMsys.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#property indicator_chart_window
//#property  indicator_separate_window 
int      step=7;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   ObjectsDeleteAll();
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   int  i=0;
   double red=iMA(NULL,NULL,step,0,MODE_EMA,PRICE_OPEN,i);
   double green=iMA(NULL,NULL,step,0,MODE_EMA,PRICE_CLOSE,i);
   double green_red=green-red;
   double open_=Open[i];
   double close_=Close[i];
   double high_=High[i];
   double low_=Low[i];
   double green0=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",0,i);
   double red0=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",1,i);
   double indicate_macd=iCustom(NULL,0,"MACD",12,26,9,0,i);
   double indicate_cci=iCustom(NULL,0,"CCI",14,0,i);
   double indicate_rsi=iCustom(NULL,0,"RSI",14,0,i);
   double indicate_atr=iCustom(NULL,0,"ATR",14,0,i);
   double indicate_roc=iCustom(NULL,0,"ROC1",10,false,0,i);
   double indicate_dma=iCustom(NULL,0,"DMA",3,3,7,5,25,5,0,1,1,1,2,i);
   double indicate_sar=iSAR(NULL, 0, 0.01, 0.2, i);

   i=1;
   double red1=iMA(NULL,NULL,step,0,MODE_EMA,PRICE_OPEN,i);
   double green1=iMA(NULL,NULL,step,0,MODE_EMA,PRICE_CLOSE,i);
   double green_red1=green-red;
   double open_1=Open[i];
   double close_1=Close[i];
   double high_1=High[i];
   double low_1=Low[i];
   double green01=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",0,i);
   double red01=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",1,i);
   double indicate_macd1=iCustom(NULL,0,"MACD",12,26,9,0,i);
   double indicate_cci1=iCustom(NULL,0,"CCI",14,0,i);
   double indicate_rsi1=iCustom(NULL,0,"RSI",14,0,i);
   double indicate_atr1=iCustom(NULL,0,"ATR",14,0,i);
   double indicate_roc1=iCustom(NULL,0,"ROC1",10,false,0,i);
   double indicate_dma1=iCustom(NULL,0,"DMA",3,3,7,5,25,5,0,1,1,1,2,i);
   double indicate_sar1=iSAR(NULL, 0, 0.01, 0.2, i);
//VC_DrawLable("Email",gcf_Email,1500,10,9);
   if(indicate_macd>indicate_macd1){
      VC_DrawLable("MACD","MACD: ↑",1500,10,9);
   }else{
      VC_DrawLable("MACD","MACD: ↓",1500,10,9);
   }
   
   if(indicate_cci>indicate_cci1){
      VC_DrawLable("cci","CCI: ↑",1500,30,9);
   }else{
      VC_DrawLable("cci","CCI: ↓",1500,30,9);
   }
   
   if(indicate_rsi>indicate_rsi1){
      VC_DrawLable("rsi","RSI: ↑",1500,50,9);
   }else{
      VC_DrawLable("rsi","RSI: ↓",1500,50,9);
   }
   
   if(indicate_rsi>indicate_atr){
      VC_DrawLable("atr","ATR: ↑",1500,70,9);
   }else{
      VC_DrawLable("atr","ATR: ↓",1500,70,9);
   }
   
   if(indicate_roc>indicate_roc1){
      VC_DrawLable("roc","ROC: ↑",1500,90,9);
   }else{
      VC_DrawLable("roc","ROC: ↓",1500,90,9);
   }
   
   if(indicate_dma>indicate_dma1){
      VC_DrawLable("dma","DMA: ↑",1500,110,9);
   }else{
      VC_DrawLable("dma","DMA: ↓",1500,110,9);
   }
   
   if(indicate_sar>indicate_sar1){
      VC_DrawLable("sar","SAR: ↑",1500,130,9);
   }else{
      VC_DrawLable("sar","SAR: ↓",1500,130,9);
   }
   
   
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
void VC_DrawLable(string strName,string strText,int nX,int nY,int nFontSize)
  {
   RefreshRates();
   ObjectCreate(strName,OBJ_LABEL,0,0,0);
   ObjectSetText(strName,strText,nFontSize,"Arial",Yellow);
   ObjectSet(strName,OBJPROP_CORNER,0);
   ObjectSet(strName,OBJPROP_XDISTANCE,nX);
   ObjectSet(strName,OBJPROP_YDISTANCE,nY);
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                 yinlizhibiao.mq4 |
//|                       Copyright ?2009, MetaQuotes Software Corp. |
//|                                        http://www.metaquotes.net |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, 方浩投资"
#property link      "http://www.fanghaotz.com"

#property indicator_chart_window
#property indicator_buffers 7
#property indicator_color1 Red
#property indicator_color2 DarkOrchid
#property indicator_color3 Red
#property indicator_color4 Red
#property indicator_color5 Blue
#property indicator_color6 Red
#property indicator_color7 White

int 因子=16801;  //15706;//19701月1日到2013年1月1日的天数，今天是2013，因子为15706
extern color  TGColor= DarkGreen;
extern color  DZColor= Blue;
int shijian=6;//0是美国时间，6是欧洲时间，13是北京时间

color DNAYS=Red;
color XYS1=Red;
color XYS2=Green;
bool is_slid= false;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+

//int Gnian[11]={0,354,708,1093,1447,1831,2186,2540,2924,3278,3632};
//int Gtian[24]={0,30,60,89,118,148,177,206,236,265,295,324,354,383,413,442,472,501,530,560,589,619,648,678};
//int GL[24]={0,31,59,90,120,151,181,212,243,273,304,334,365,396,424,455,485,516,546,577,608,638,669,699};
int NL[14]={29,30,29,30,30,29,30,29,30,29,30,29,30,29};

double ExtMapBuffer1[];
double ExtMapBuffer2[];
double ExtMapBuffer3[];
double ExtMapBuffer4[];
double ExtMapBuffer5[];

double guava_H[];
double guava_L[];
int KZ;
int ExtCountedBars=0;
datetime tt1;
datetime tt2;
double pp1;
double pp2;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int init()
  {
//----

   SetIndexStyle(0,DRAW_ARROW);//
   SetIndexArrow(0,90);
   SetIndexBuffer(0,ExtMapBuffer1);
   SetIndexEmptyValue(0,0.0);

   SetIndexStyle(1,DRAW_ARROW);//
   SetIndexArrow(1,110);
   SetIndexBuffer(1,ExtMapBuffer2);
   SetIndexEmptyValue(1,0.0);
   SetIndexStyle(2,DRAW_LINE);
   SetIndexBuffer(2,ExtMapBuffer3);
   SetIndexStyle(3,DRAW_LINE);
   SetIndexBuffer(3,ExtMapBuffer4);
   SetIndexStyle(4,DRAW_LINE);
   SetIndexBuffer(4,ExtMapBuffer5);

   SetIndexStyle(5,DRAW_LINE);
   SetIndexBuffer(5,guava_H);
   SetIndexStyle(6,DRAW_LINE);
   SetIndexBuffer(6,guava_L);

//KZ=25;
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   ObjectsDeleteAll();
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
/*
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
*/
  {

   int limit;

   int counted_bars=IndicatorCounted();
//---- check for possible errors
   if(counted_bars<0) return(-1);
//---- last counted bar will be recounted
   if(counted_bars>0) counted_bars--;
//   limit=Bars-counted_bars;
//----
   int ii=1;
   int SJC;
   int XHKS;
   int XSXS;
   int ZQS;
   int SJSXZC;
   if(Period()==1)      {return(-1);}
   if(Period()==5)      {return(-1);}
   if(Period()==15)
     {
      limit=4000;
      SJC=5400;
      XHKS=TimeHour(Time[0])*4;
      XSXS=4;
      ZQS=96;
      SJSXZC=900;
     }
   if(Period()==30)
     {
      limit=2000;
      SJC=3600;
      XHKS=TimeHour(Time[0])*2;
      XSXS=2;
      ZQS=48;
      SJSXZC=1800;
     }
   if(Period()==60)
     {
      limit=1000;
      SJC=0;
      XHKS=TimeHour(Time[0]);
      XSXS=1;
      ZQS=24;
      SJSXZC=3600;
     }
   if(Period()==240)    {return(-1);}
   if(Period()==1440)   {return(-1);}
   if(Period()==10080)  {return(-1);}
   if(Period()==43200)  {return(-1);}




   double temple_DH=0;
   double temple_DL=0;
//for(int gi=limit; gi>-1; gi--)
   for(int gi=limit; gi>0; gi--)
     {
      //Print("ZQS:"+ZQS+";XSXS:"+XSXS);
      //iHighest(symbolsymbol,timeframe,timeseries,Number of bars,Shift showing the bar)
      double H1=High[iHighest(0,0,MODE_HIGH,ZQS,TimeHour(Time[gi])*XSXS)];
      double H3=High[iHighest(0,0,MODE_HIGH,ZQS,(TimeHour(Time[gi])+48)*XSXS)];
      double L1=Low[iLowest(0,0,MODE_LOW,ZQS,TimeHour(Time[gi])*XSXS)];
      double L3=Low[iLowest(0,0,MODE_LOW,ZQS,(TimeHour(Time[gi])+48)*XSXS)];

      double DL=L1-(H3-L1)/2;
      double DH=H1+(H1-L3)/2;
      //iClose(symbol,timeframe,shift)
      double time_n=0;
      double time_seconde=(TimeCurrent()-TimeSeconds(TimeCurrent())-TimeMinute(TimeCurrent())*60-TimeHour(TimeCurrent())*60*60-Time[gi]);
      if(time_seconde<0){
      time_n=0;
      }else{
          time_n=(TimeCurrent()-TimeSeconds(TimeCurrent())-TimeMinute(TimeCurrent())*60-TimeHour(TimeCurrent())*60*60-Time[gi])/(24*60*60)+1;
      }
      
      //double time_n=(TimeCurrent()-Time[gi])/(1440*60);
      if(gi>15)
        {
         Print("shift:"+time_n);
        }else{
         Print("shift:nne");
        }
      double EH=iClose(0,PERIOD_D1,1+time_n)+(H1-L1)/2;
      double EL=iClose(0,PERIOD_D1,1+time_n)-(H1-L1)/2;
      if((iHigh(0,PERIOD_D1,1+time_n)-iLow(0,PERIOD_D1,1+time_n))/iClose(0,PERIOD_D1,1+time_n)>0.015)
        {
         //Print(TimeHour(Time[0])+":case1:"+TimeHour(Time[gi]));
         DH=EH;
         DL=EL;
        }
      if((MathAbs(DL-iClose(0,PERIOD_D1,1+time_n)))/iOpen(0,PERIOD_D1,1+time_n)>0.02)
        {//EH=iClose(0,PERIOD_D1,1)+(High[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(Time[0])*XSXS)]-Low[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(Time[0])*XSXS)])/2;
         DL=iClose(0,PERIOD_D1,1+time_n)-(High[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(Time[gi])*XSXS)]-Low[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(Time[gi])*XSXS)])/2;
         //Print(DL+"::"+iClose(0,PERIOD_D1,1+time_n)+":case21:"+TimeHour(Time[gi]));
        }
      if((MathAbs(DH-iClose(0,PERIOD_D1,1+time_n)))/iOpen(0,PERIOD_D1,1+time_n)>0.02)
        {
         DH=iClose(0,PERIOD_D1,1+time_n)+(High[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(Time[gi])*XSXS)]-Low[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(Time[gi])*XSXS)])/2;
         //Print(TimeHour(Time[0])+":case22:"+TimeHour(Time[gi]));
         //EL=iClose(0,PERIOD_D1,1)-(High[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(Time[0])*XSXS)]-Low[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(Time[0])*XSXS)])/2 ;
        }
      if(EH>DH)
        {
         DH=EH;
         //Print(TimeHour(Time[0])+":use eh:"+TimeHour(Time[gi]));
           }else{
         //Print("use DH:"+TimeHour(Time[gi]));
        }
      if(EL<DL)
        {
         DL=EL;
         //Print(TimeHour(Time[0])+":use el:"+EL);
        }
      DL=EL;
      //Print(DL+":guava:"+EL+":"+TimeHour(Time[gi]));
      //if(ZF1<0.02 && ZF2<0.02 && ZF3<0.02)
      //{
      //writeDHL("DH"+0,XSJA2,XJGA1,XSJA1,XJGA2,Red,1,true);
      //writeDHL("DL"+0,XSJB1,XJGB1,XSJB2,XJGB2,Red,1,true);

      if(TimeHour(Time[gi])==0)
        {
         temple_DH=DH;
         temple_DL=DL;
        }
      if(is_slid==true)
        {
         temple_DH=DH;
         temple_DL=DL;
        }
      guava_H[gi]=temple_DH;
      guava_L[gi]=temple_DL;
     }

   return(0);
  }
//+------------------------------------------------------------------+

void writetext(string Labelname,string data,int x,int y,color ColorValue,int FontSize)//通过Object写文字
  {
   ObjectDelete(Labelname);
   ObjectCreate(Labelname,OBJ_LABEL,0,0,0);
   ObjectSetText(Labelname,data,FontSize,"黑体",ColorValue);
   ObjectSet(Labelname,OBJPROP_CORNER,0);
   ObjectSet(Labelname,OBJPROP_XDISTANCE,x);
   ObjectSet(Labelname,OBJPROP_YDISTANCE,y);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void writejx(string jname,color c1,datetime t1,datetime t2,double p1,double p2)//通过Object写文字
  {
   ObjectDelete(jname);
   ObjectCreate(jname,OBJ_RECTANGLE,0,0,0,0,0);
   ObjectSet(jname,OBJPROP_STYLE,STYLE_SOLID);
   ObjectSet(jname,OBJPROP_COLOR,c1);
   ObjectSet(jname,OBJPROP_TIME1,t1);
   ObjectSet(jname,OBJPROP_PRICE1,p1);
   ObjectSet(jname,OBJPROP_TIME2,t2);
   ObjectSet(jname,OBJPROP_PRICE2,p2);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void writeDNA(string DNA,datetime tttt1,double pppp1,datetime tttt2,double pppp2,color YS,int CX,bool SX)//通过Object写文字
  {
   ObjectDelete(DNA);
   ObjectCreate(DNA,OBJ_TREND,0,0,0,0,0);
   ObjectSet(DNA,OBJPROP_TIME1,tttt1);
   ObjectSet(DNA,OBJPROP_PRICE1,pppp1);
   ObjectSet(DNA,OBJPROP_TIME2,tttt2);
   ObjectSet(DNA,OBJPROP_PRICE2,pppp2);
   ObjectSet(DNA,OBJPROP_RAY,SX);
   ObjectSet(DNA,OBJPROP_WIDTH,CX);
   ObjectSet(DNA,OBJPROP_COLOR,YS);
//ObjectSet(DNA, OBJPROP_STYLE, STYLE_DOT);

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void writeDHL(string DNA,datetime tttt1,double pppp1,datetime tttt2,double pppp2,color YS,int CX,bool SX)//通过Object写文字
  {
   ObjectDelete(DNA);
   ObjectCreate(DNA,OBJ_TREND,0,0,0,0,0);
   ObjectSet(DNA,OBJPROP_TIME1,tttt1);
   ObjectSet(DNA,OBJPROP_PRICE1,pppp1);
   ObjectSet(DNA,OBJPROP_TIME2,tttt2);
   ObjectSet(DNA,OBJPROP_PRICE2,pppp2);
   ObjectSet(DNA,OBJPROP_RAY,SX);
   ObjectSet(DNA,OBJPROP_WIDTH,CX);
   ObjectSet(DNA,OBJPROP_COLOR,YS);
   ObjectSet(DNA,OBJPROP_STYLE,STYLE_DOT);

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void writeDW(string DW,string data2,datetime ttttt1,double ppppp1,color ColorValue,int FontSize)//通过Object写文字
  {
   ObjectDelete(DW);
   ObjectCreate(DW,OBJ_TEXT,0,0,0);
   ObjectSetText(DW,data2,FontSize,"黑体",ColorValue);
   ObjectSet(DW,OBJPROP_TIME1,ttttt1);
   ObjectSet(DW,OBJPROP_PRICE1,ppppp1);

//ObjectSet(DNA, OBJPROP_RAY, False);
  }
//+------------------------------------------------------------------+

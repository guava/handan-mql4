//+------------------------------------------------------------------+
//|                                                         test.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <MovingAverages.mqh>
//--- indicator settings
#property  indicator_separate_window 
//#property indicator_chart_window  //主图
#property  indicator_buffers 4
#property  indicator_color1  Red//Silver
#property  indicator_color2  White
#property  indicator_color3  Yellow 
#property  indicator_color4  Green 
//--- input parameters
int      step1=3;
int step2=12;
//--- buffers
double openp[];
double closep[];
double hight[];
double blow[];
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   IndicatorBuffers(4);
   SetIndexBuffer(0,openp);
   SetIndexBuffer(1,closep);
   SetIndexBuffer(2,hight);
   SetIndexBuffer(3,blow); 
   
   //--- indicator line
   SetIndexStyle(0,DRAW_LINE);
   //SetIndexBuffer(0,openp);
   //--- indicator line
   SetIndexStyle(1,DRAW_LINE);
   //SetIndexBuffer(1,closep);
   
   SetIndexStyle(2,DRAW_ARROW);
   //SetIndexBuffer(2,hight);
   SetIndexStyle(3,DRAW_ARROW);
   
   
   IndicatorShortName("ATRP");
   SetIndexLabel(0,"ATRP");
   SetIndexDrawBegin(0,step2+1);
   SetIndexDrawBegin(1,step2+1);
   SetIndexDrawBegin(2,step2+1);
   SetIndexDrawBegin(3,step2+1);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   int i,limit;
   if(rates_total<=8)
      return(0);
   limit=rates_total-prev_calculated;
   if(prev_calculated>0)
      limit++;
   //Print(Bid+":"+Ask);
   //buy(Ask->bid)
   //sell(bid -> Ask)
//--- macd counted in the 1-st buffer
   for(i=0; i<limit-step2; i++){
   /*
      double sum1=0;
      double sum2=0;
      
      sum1=(iMA(NULL,NULL,step1,0,MODE_EMA,PRICE_HIGH,i)-iMA(NULL,NULL,step1,0,MODE_EMA,PRICE_LOW,i));
      sum2=(iMA(NULL,NULL,step2,0,MODE_EMA,PRICE_HIGH,i)-iMA(NULL,NULL,step2,0,MODE_EMA,PRICE_LOW,i));
      double sum_ma=(sum1+sum2)/2*0.8;
      */
      //openp[i]=iMA(NULL,NULL,step,0,MODE_EMA,PRICE_OPEN,i);
      //closep[i]=iMA(NULL,NULL,step,0,MODE_EMA,PRICE_CLOSE,i);
      hight[i]=Close[i+1]+( (iMA(NULL,NULL,step1,0,MODE_EMA,PRICE_HIGH,i)-iMA(NULL,NULL,step1,0,MODE_EMA,PRICE_LOW,i)) 
      +(iMA(NULL,NULL,step2,0,MODE_EMA,PRICE_HIGH,i)-iMA(NULL,NULL,step2,0,MODE_EMA,PRICE_LOW,i)) ) /2*0.8;
      blow[i]=Close[i+1]-( (iMA(NULL,NULL,step1,0,MODE_EMA,PRICE_HIGH,i)-iMA(NULL,NULL,step1,0,MODE_EMA,PRICE_LOW,i)) 
      +(iMA(NULL,NULL,step2,0,MODE_EMA,PRICE_HIGH,i)-iMA(NULL,NULL,step2,0,MODE_EMA,PRICE_LOW,i)) ) /2*0.8;
   }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
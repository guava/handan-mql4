//+------------------------------------------------------------------+
//|                                                        guava.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//#include "../Libraries/OrderState.mq4"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class OrderDoSend
  {
protected:
   double            volumePerOrder;
   int               maxSlippage;
   int               points_bad;
   int               points_good;
public:
   void SetOrderDoSend(double volumePerOrder_,int maxSlippage_,int points_bad_,int points_good_)
     {
      volumePerOrder=volumePerOrder_;
      maxSlippage=maxSlippage_;
      points_bad=points_bad_+200;
      points_good=points_good_+200;
     }

   int openBuyAction()
     {
      int orderNumber2=OrderSend(Symbol(),OP_BUY,volumePerOrder,Ask,maxSlippage,Ask-points_bad*Point,Ask+points_good*Point);
      return orderNumber2;
     }

   int openSellAction()
     {
      int orderNumber1=OrderSend(Symbol(),OP_SELL,volumePerOrder,Bid,maxSlippage,Bid+points_bad*Point,Bid-points_good*Point);
      return orderNumber1;
     }

   bool buyCloseAction(int order_number)
     {
      bool bca=OrderClose(order_number,volumePerOrder,Bid,5,White);
      return bca;
     }

   bool sellCloseAction(int order_number)
     {
      bool bca=OrderClose(order_number,volumePerOrder,Ask,5,White);
      return bca;
     }

  };
//+------------------------------------------------------------------+

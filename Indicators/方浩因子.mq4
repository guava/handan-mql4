//+------------------------------------------------------------------+
//|                                                 yinlizhibiao.mq4 |
//|                       Copyright ?2009, MetaQuotes Software Corp. |
//|                                        http://www.metaquotes.net |
//+------------------------------------------------------------------+
#property copyright "Copyright ?2009, MetaQuotes Software Corp."
#property link      "http://www.metaquotes.net"

#property indicator_chart_window
#property indicator_buffers 7
#property indicator_color1 Red
#property indicator_color2 DarkOrchid
#property indicator_color3 Red
#property indicator_color4 Red
#property indicator_color5 Blue
#property indicator_color6 Yellow
#property indicator_color7 Yellow

int 因子=16801;  //15706;//19701月1日到2013年1月1日的天数，今天是2013，因子为15706
extern color  TGColor= DarkGreen;
extern color  DZColor= Blue;
int shijian=6;//0是美国时间，6是欧洲时间，13是北京时间

color DNAYS=Red;
color XYS1=Red;
color XYS2=Green;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+

//int Gnian[11]={0,354,708,1093,1447,1831,2186,2540,2924,3278,3632};
//int Gtian[24]={0,30,60,89,118,148,177,206,236,265,295,324,354,383,413,442,472,501,530,560,589,619,648,678};
//int GL[24]={0,31,59,90,120,151,181,212,243,273,304,334,365,396,424,455,485,516,546,577,608,638,669,699};
int NL[14]={29,30,29,30,30,29,30,29,30,29,30,29,30,29};

double ExtMapBuffer1[];
double ExtMapBuffer2[];
double ExtMapBuffer3[];
double ExtMapBuffer4[];
double ExtMapBuffer5[];

double guava_H[];
double guava_L[];
int KZ;
int ExtCountedBars=0;
datetime tt1;
datetime tt2;
double pp1;
double pp2;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int init()
  {
//----

   SetIndexStyle(0,DRAW_ARROW);//
   SetIndexArrow(0,90);
   SetIndexBuffer(0,ExtMapBuffer1);
   SetIndexEmptyValue(0,0.0);

   SetIndexStyle(1,DRAW_ARROW);//
   SetIndexArrow(1,110);
   SetIndexBuffer(1,ExtMapBuffer2);
   SetIndexEmptyValue(1,0.0);
   SetIndexStyle(2,DRAW_LINE);
   SetIndexBuffer(2,ExtMapBuffer3);
   SetIndexStyle(3,DRAW_LINE);
   SetIndexBuffer(3,ExtMapBuffer4);
   SetIndexStyle(4,DRAW_LINE);
   SetIndexBuffer(4,ExtMapBuffer5);
   
   SetIndexStyle(5,DRAW_LINE);
   SetIndexBuffer(5,guava_H);
   SetIndexStyle(6,DRAW_LINE);
   SetIndexBuffer(6,guava_L);

//KZ=25;
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   ObjectsDeleteAll();
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
//int start()
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {

   int limit;
   double temp;

   int counted_bars=IndicatorCounted();
//---- check for possible errors
   if(counted_bars<0) return(-1);
//---- last counted bar will be recounted
   if(counted_bars>0) counted_bars--;
//   limit=Bars-counted_bars;
//----
   int ii=1;
   int SJC;
   int XHKS;
   int XSXS;
   int ZQS;
   int SJSXZC;
   if(Period()==1)      {return(-1);}
   if(Period()==5)      {return(-1);}
   if(Period()==15)
     {
      limit=4000;
      SJC=5400;
      XHKS=TimeHour(TimeCurrent())*4;
      XSXS=4;
      ZQS=96;
      SJSXZC=900;
     }
   if(Period()==30)
     {
      limit=2000;
      SJC=3600;
      XHKS=TimeHour(TimeCurrent())*2;
      XSXS=2;
      ZQS=48;
      SJSXZC=1800;
     }
   if(Period()==60)
     {
      limit=1000;
      SJC=0;
      XHKS=TimeHour(TimeCurrent());
      XSXS=1;
      ZQS=24;
      SJSXZC=3600;
     }
   if(Period()==240)    {return(-1);}
   if(Period()==1440)   {return(-1);}
   if(Period()==10080)  {return(-1);}
   if(Period()==43200)  {return(-1);}




//if(KZ>TimeHour(TimeCurrent()))
//{

   int TDAY=(TimeCurrent()+6*3600)/86400;
   int BYear=TDAY-15332;
//int BYear=TDAY-因子;

   for(int N=0;N<14;N++)
     {
      if(BYear>NL[N])
         BYear=BYear-NL[N];

     }
//   int B2009=TDAY-15007;
//      int today2=BYear;
   int today=BYear;
//   int C2009=TDAY-14245;

   if(today>15)today=today-15;

   int CJ1=today*0.8;
   double CCJJ1=today*0.8;

   if(CJ1>=7)CJ1=CJ1-6;
   if(CCJJ1>=7)CCJJ1=CCJJ1-6;
   int CJ2=CJ1+12;



   int FZS=(CCJJ1-CJ1)*60;
   int TGS=BYear%10+6;
   int DZS=BYear%12+8;
   int JQS=BYear%15-9;
   if(TGS>10)TGS=TGS-10;
   if(DZS>12)DZS=DZS-12;

   int bjCJ1=CJ1+shijian;
   int bjCJ2=CJ2+shijian;
   if(bjCJ1>24)bjCJ1=bjCJ1-24;
   if(bjCJ2>24)bjCJ2=bjCJ2-24;

//------------------------------
   int JL=0;
   if((TGS==1 || TGS==4 || TGS==5) && (DZS!=3 && DZS!=5 && DZS!=6))
     {

      JL=202;

     }
   else
     {

      if((TGS!=1 && TGS!=4 && TGS!=5) && (DZS==3 || DZS==5 || DZS==6))
        {
         JL=152;
        }
      else
        {
         JL=177;
        }
     }

   writetext(0,"方浩今日潮极时间:",0,20,Red,14);
   writetext(1,CJ1+":"+FZS,130,20,TGColor,12);
   writetext(2,CJ2+":"+FZS,205,20,DZColor,12);
   writetext(3,"_____",130,20,Red,15);
   writetext(4,"_____",180,20,Red,15);
   writetext(5,"|",127,27,Red,10);
   writetext(6,"|",JL,27,Red,10);
   writetext(7,"|",227,27,Red,10);

//------------------------------

//   writeDNA("DNA"+g,ttt1,ppp1,ttt2,ppp2,DNAYS,1,false);

//---------------------------------

   int p=TimeHour(TimeCurrent())-CJ1;

   int today1;
   int CJ11;
   int CJ21;
   int YH1;
   int YH2;
   double W;

   double TH;
   double TL;
   double NH;
   double NL;

   today1=today-1;
   if(today1<1)today1=14;
   CJ11=today1*0.8;
   CJ21=today1*0.8+12;
   YH1=(TimeHour(TimeCurrent())+(24-CJ11))*XSXS;
   YH2=(TimeHour(TimeCurrent())+(24-CJ21))*XSXS;
   W=(Close[YH1]+Close[YH2]+2*Close[TimeHour(TimeCurrent())*XSXS])/4;
   int KSS=TimeHour(TimeCurrent())*XSXS;
   TH=W+0.5*(High[iHighest(0,0,MODE_HIGH,ZQS,KSS)]-Low[iLowest(0,0,MODE_LOW,ZQS,KSS)]);
   TL=W-0.5*(High[iHighest(0,0,MODE_HIGH,ZQS,KSS)]-Low[iLowest(0,0,MODE_LOW,ZQS,KSS)]);
   NH=W+(Close[YH1]-Close[YH2]);
   NL=W-(Close[YH1]-Close[YH2]);

//------------------------------------------------------------------------
   double today3=today-3;
   if(today3<1)today1=14;
   int CJ13=today3*0.8;
   int CJ23=today3*0.8+12;
   
   
   for(int gi=0; gi<limit; gi++)
     {
   double H1=High[iHighest(0,0,MODE_HIGH,ZQS,TimeHour(TimeCurrent())*XSXS)];
   double H3=High[iHighest(0,0,MODE_HIGH,ZQS,(TimeHour(TimeCurrent())+48)*XSXS)];
   double L1=Low[iLowest(0,0,MODE_LOW,ZQS,TimeHour(TimeCurrent())*XSXS)];
   double L3=Low[iLowest(0,0,MODE_LOW,ZQS,(TimeHour(TimeCurrent())+48)*XSXS)];

   double DL=L1-(H3-L1)/2;
   double DH=H1+(H1-L3)/2;
   double EH=iClose(0,PERIOD_D1,1+gi)+(H1-L1)/2;
   double EL=iClose(0,PERIOD_D1,1+gi)-(H1-L1)/2;
   if((iHigh(0,PERIOD_D1,1+gi)-iLow(0,PERIOD_D1,1+gi))/iClose(0,PERIOD_D1,1+gi)>0.015)
     {
      DH=EH;
      DL=EL;
     }
   if((MathAbs(DL-iClose(0,PERIOD_D1,1+gi)))/iOpen(0,PERIOD_D1,1+gi)>0.02)
     {//EH=iClose(0,PERIOD_D1,1)+(High[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(TimeCurrent())*XSXS)]-Low[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(TimeCurrent())*XSXS)])/2;
      DL=iClose(0,PERIOD_D1,1+gi)-(High[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(TimeCurrent())*XSXS)]-Low[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(TimeCurrent())*XSXS)])/2;
     }
   if((MathAbs(DH-iClose(0,PERIOD_D1,1+gi)))/iOpen(0,PERIOD_D1,1+gi)>0.02)
     {
      DH=iClose(0,PERIOD_D1,1+gi)+(High[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(TimeCurrent())*XSXS)]-Low[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(TimeCurrent())*XSXS)])/2;
      //EL=iClose(0,PERIOD_D1,1)-(High[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(TimeCurrent())*XSXS)]-Low[iHighest(0,0,MODE_HIGH,ZQS*3,TimeHour(TimeCurrent())*XSXS)])/2 ;
     }
   if(EH>DH)DH=EH;
   if(EL<DL)DL=EL;
   double XJGA1=H3;
   double XJGA2=L1;
   datetime XSJA1=Time[iHighest(0,0,MODE_HIGH,ZQS,TimeHour(TimeCurrent())*XSXS)];
   datetime XSJA2=Time[iLowest(0,0,MODE_LOW,ZQS,(TimeHour(TimeCurrent())+48)*XSXS)];


   double XJGB1=H1;
   double XJGB2=L3;
   datetime XSJB1=Time[iHighest(0,0,MODE_HIGH,ZQS,(TimeHour(TimeCurrent())+48)*XSXS)];
   datetime XSJB2=Time[iLowest(0,0,MODE_LOW,ZQS,TimeHour(TimeCurrent())*XSXS)];
   datetime DHLSJ1=(18-TimeHour(TimeCurrent()))*3600+TimeCurrent();
   datetime DHLSJ2=(24-TimeHour(TimeCurrent()))*3600+TimeCurrent();
   double ZF1=(iHigh(0,PERIOD_D1,1)-iLow(0,PERIOD_D1,1))/iOpen(0,PERIOD_D1,1);
   double ZF2=(iHigh(0,PERIOD_D1,2)-iLow(0,PERIOD_D1,2))/iOpen(0,PERIOD_D1,2);
   double ZF3=(iHigh(0,PERIOD_D1,3)-iLow(0,PERIOD_D1,3))/iOpen(0,PERIOD_D1,3);
//if(ZF1<0.02 && ZF2<0.02 && ZF3<0.02)
//{
//writeDHL("DH"+0,XSJA2,XJGA1,XSJA1,XJGA2,Red,1,true);
//writeDHL("DL"+0,XSJB1,XJGB1,XSJB2,XJGB2,Red,1,true);
   //writeDHL("DH"+1,DHLSJ1,DL,DHLSJ2,DL,Blue,1,false);
   //writeDHL("DL"+2,DHLSJ1,DH,DHLSJ2,DH,Blue,1,false);
   
   
      guava_H[gi]=DH;
      guava_L[gi]=DL;
     }
//}
//else
//{
//}
//------------------------------------------------------------------------
   datetime DS=TimeCurrent()-TimeMinute(TimeCurrent())*60;
   int ZJSJ;
   int CSSS=p*XSXS;
   datetime TGSJ=DS-p*3600+shijian*3600;
//datetime TGSJ=Time[CSSS];
//ZJSJ=Time[CSSS]+3600*9
   datetime DZSJ=TGSJ+12*3600+SJC;
   double TGJG=Low[0];
//datetime DZSJ=DS-p*3600+12*3600+SJC;
   double DZJG=Low[0];
//if(CJ1==12)DZSJ=DZSJ-3600;
//    writetext(110,TGSJ+":"+DZSJ,130,50,TGColor,12); 




   if(TGS==1)
     {
      if(today==1 || today==15)writeDW("DWTG"+0,"*T",TGSJ,TGJG,Red,15);

      else
        {
         if(JQS==0)writeDW("DWTG"+0,"#T",TGSJ,TGJG,Red,15);
         else
           {
            writeDW("DWTG"+0,"T",TGSJ,TGJG,Red,15);
           }
        }
     }
   if(TGS==2)
     {
      if(today==1 || today==15)writeDW("DWTG"+0,"*",TGSJ,TGJG,Red,15);

      else
        {
         if(JQS==0)writeDW("DWTG"+0,"#",TGSJ,TGJG,Red,15);
         else
           {
            writeDW("DWTG"+0,"",TGSJ,TGJG,Red,15);
           }
        }
     }
   if(TGS==3)
     {
      if(today==1 || today==15)writeDW("DWTG"+0,"*V",TGSJ,TGJG,Red,15);

      else
        {
         if(JQS==0)writeDW("DWTG"+0,"#V",TGSJ,TGJG,Red,15);
         else
           {
            writeDW("DWTG"+0,"V",TGSJ,TGJG,Red,15);
           }
        }
     }
   if(TGS==4)
     {
      if(today==1 || today==15)writeDW("DWTG"+0,"*sS",TGSJ,TGJG,Red,15);

      else
        {
         if(JQS==0)writeDW("DWTG"+0,"#sS",TGSJ,TGJG,Red,15);
         else
           {
            writeDW("DWTG"+0,"sS",TGSJ,TGJG,Red,15);
           }
        }
     }
   if(TGS==5)
     {
      if(today==1 || today==15)writeDW("DWTG"+0,"*L",TGSJ,TGJG,Red,15);

      else
        {
         if(JQS==0)writeDW("DWTG"+0,"#L",TGSJ,TGJG,Red,15);
         else
           {
            writeDW("DWTG"+0,"L",TGSJ,TGJG,Red,15);
           }
        }
     }
   if(TGS==6)
     {
      if(today==1 || today==15)writeDW("DWTG"+0,"*Y",TGSJ,TGJG,Red,15);

      else
        {
         if(JQS==0)writeDW("DWTG"+0,"#Y",TGSJ,TGJG,Red,15);
         else
           {
            writeDW("DWTG"+0,"Y",TGSJ,TGJG,Red,15);
           }
        }
     }
   if(TGS==7)
     {
      if(today==1 || today==15)writeDW("DWTG"+0,"*K",TGSJ,TGJG,Red,15);

      else
        {
         if(JQS==0)writeDW("DWTG"+0,"#K",TGSJ,TGJG,Red,15);
         else
           {
            writeDW("DWTG"+0,"K",TGSJ,TGJG,Red,15);
           }
        }
     }
   if(TGS==8)
     {
      if(today==1 || today==15)writeDW("DWTG"+0,"*C",TGSJ,TGJG,Red,15);

      else
        {
         if(JQS==0)writeDW("DWTG"+0,"#C",TGSJ,TGJG,Red,15);
         else
           {
            writeDW("DWTG"+0,"C",TGSJ,TGJG,Red,15);
           }
        }
     }
   if(TGS==9)
     {
      if(today==1 || today==15)writeDW("DWTG"+0,"*",TGSJ,TGJG,Red,15);

      else
        {
         if(JQS==0)writeDW("DWTG"+0,"#",TGSJ,TGJG,Red,15);
         else
           {
            writeDW("DWTG"+0,"",TGSJ,TGJG,Red,15);
           }
        }
     }
   if(TGS==10)
     {
      if(today==1 || today==15)writeDW("DWTG"+0,"*mM",TGSJ,TGJG,Red,15);

      else
        {
         if(JQS==0)writeDW("DWTG"+0,"#mM",TGSJ,TGJG,Red,15);
         else
           {
            writeDW("DWTG"+0,"mM",TGSJ,TGJG,Red,15);
           }
        }
     }

//---------------------------------------------
   if(DZS==1) writeDW("DWDZ"+0,"",DZSJ,DZJG,Blue,15);
   if(DZS==2) writeDW("DWDZ"+0,"",DZSJ,DZJG,Blue,15);

   if(DZS==3) writeDW("DWDZ"+0,"T",DZSJ,DZJG,Blue,15);

   if(DZS==4) writeDW("DWDZ"+0,"",DZSJ,DZJG,Blue,15);

   if(DZS==5) writeDW("DWDZ"+0,"L",DZSJ,DZJG,Blue,15);
   if(DZS==6) writeDW("DWDZ"+0,"sS",DZSJ,DZJG,Blue,15);
   if(DZS==7) writeDW("DWDZ"+0,"V",DZSJ,DZJG,Blue,15);

   if(DZS==8) writeDW("DWDZ"+0,"Y",DZSJ,DZJG,Blue,15);

   if(DZS==9) writeDW("DWDZ"+0,"K",DZSJ,DZJG,Blue,15);

   if(DZS==10) writeDW("DWDZ"+0,"C",DZSJ,DZJG,Blue,15);

   if(DZS==11) writeDW("DWDZ"+0,"D",DZSJ,DZJG,Blue,15);

   if(DZS==12) writeDW("DWDZ"+0,"mM",DZSJ,DZJG,Blue,15);

   if((TGS==1 || TGS==4 || TGS==5) && (DZS!=3 && DZS!=5 && DZS!=6))
     {

      ZJSJ=TGSJ+3600*9;

     }
   else
     {

      if((TGS!=1 && TGS!=4 && TGS!=5) && (DZS==3 || DZS==5 || DZS==6))
        {

         ZJSJ=TGSJ+3600*3;

        }
      else
        {

         ZJSJ=TGSJ+3600*6;

        }

     }
//    writetext(65434,"今日潮极时间:"+ZJSJ+","+DZSJ,100,40,Red,14); 
   writejx("TG"+0,TGColor,TGSJ,ZJSJ,TH,NH);

   writejx("DZ"+0,DZColor,ZJSJ,DZSJ,TL,NL);

//-------------------------------------------

   if(Symbol()!="USDJPY")
     {
      int h=1;
      datetime ttt2=TimeCurrent()-p*3600;
      int iii=1;
      for(int g=(p*4)+12;g>p*4;g--)
        {

         datetime ttt1=ttt2;
         double ppp1=iClose(0,PERIOD_M15,g+1)*0.99;
         ttt2=TimeCurrent()-p*3600+h*3600;
         double ppp2=iClose(0,PERIOD_M15,g)*0.99;
         if(ppp1==0 || ppp2==0){}
         else
           {
            writeDNA("DNA"+iii,ttt1,ppp1,ttt2,ppp2,DNAYS,1,false);
           }
         h++;
         iii++;
        }
     }
   else
     {
      //-------------------------------------------------------

      //if((today4<=9 && p>=3) && ())

      h=1;
      ttt2=TimeCurrent()-p*3600;
      for(g=(p+3)*4;g>(p+3)*4-12;g--)
        {

         ttt1=ttt2;
         ppp1=iClose(0,PERIOD_M15,g+1)*0.99;
         ttt2=TimeCurrent()-p*3600+h*3600;
         ppp2=iClose(0,PERIOD_M15,g)*0.99;
         if(ppp1==0 || ppp2==0){}
         else
           {
            writeDNA("DNA"+iii,ttt1,ppp1,ttt2,ppp2,DNAYS,1,false);
           }
         h++;
         iii++;
        }
      //----------------------------------------------
     }
   KZ=TimeHour(Time[1]);
//--------------------------------------------------
   datetime sj=TimeCurrent()-(p*3600)+12*3600;
   double jg=Low[p];

//}
//else
//{
//}

//----------------------------------------------
   switch(TGS)
     {
      case 1:
         writetext(8,"未来五天:",0,40,Red,14);
         writetext(9,"TVsSL",130,40,TGColor,12);
         break;
      case 2:
         writetext(8,"未来五天:",0,40,Red,14);
         writetext(9,"VsSLY",130,40,TGColor,12);
         break;
      case 3:
         writetext(8,"未来五天:",0,40,Red,14);
         writetext(9,"VsSLYK",130,40,TGColor,12);
         break;
      case 4:
         writetext(8,"未来五天:",0,40,Red,14);
         writetext(9,"sSLYKC",130,40,TGColor,12);
         break;
      case 5:
         writetext(8,"未来五天:",0,40,Red,14);
         writetext(9,"LYKC",130,40,TGColor,12);
         break;
      case 6:
         writetext(8,"未来五天:",0,40,Red,14);
         writetext(9,"YKCmM",130,40,TGColor,12);
         break;
      case 7:
         writetext(8,"未来五天:",0,40,Red,14);
         writetext(9,"KCmMT",130,40,TGColor,12);
         break;
      case 8:
         writetext(8,"未来五天:",0,40,Red,14);
         writetext(9,"CmMT",130,40,TGColor,12);
         break;
      case 9:
         writetext(8,"未来五天:",0,40,Red,14);
         writetext(9,"mMTV",130,40,TGColor,12);
         break;
      case 10:
         writetext(8,"未来五天:",0,40,Red,14);
         writetext(9,"mMTVsS",130,40,TGColor,12);
         break;
     }

   switch(DZS)
     {
      case 1:
         writetext(10,"TL",130,60,DZColor,12);
         break;
      case 2:
         writetext(10,"TLsS",130,60,DZColor,12);
         break;
      case 3:
         writetext(10,"TLsSV",130,60,DZColor,12);
         break;
      case 4:
         writetext(10,"LsSVY",130,60,DZColor,12);
         break;
      case 5:
         writetext(10,"LsSVYK",130,60,DZColor,12);
         break;
      case 6:
         writetext(10,"sSVYKC",130,60,DZColor,12);
         break;
      case 7:
         writetext(10,"VYKCD",130,60,DZColor,12);
         break;
      case 8:
         writetext(10,"YKCDmM",130,60,DZColor,12);
         break;
      case 9:
         writetext(10,"KCDmM",130,60,DZColor,12);
         break;
      case 10:
         writetext(10,"CDmM",130,60,DZColor,12);
         break;
      case 11:
         writetext(10,"DmMT",130,60,DZColor,12);
         break;
      case 12:
         writetext(10,"mMT",130,60,DZColor,12);
         break;
     }

//-----------------------------------------------

//------------------------------循环
   for(int i=0; i<limit; i++)
     {
      int NL1[14]={29,30,29,30,30,29,30,29,30,29,30,29,30,29};

      TDAY=(Time[i]+6*3600)/86400;
      BYear=TDAY-因子;

      for(N=0;N<14;N++)
        {
         if(BYear>NL1[N])
            BYear=BYear-NL1[N];

        }

      today=BYear;

      if(today>15)today=today-15;

      CCJJ1=today*0.8;

      CJ1=today*0.8;

      if(CJ1>=7)CJ1=CJ1-6;
      if(CCJJ1>=7)CCJJ1=CCJJ1-6;

      CJ2=CJ1+12;

      TGS=BYear%10+6;
      DZS=BYear%12+8;
      JQS=BYear%15-9;

      if(TGS>10)TGS=TGS-10;
      if(DZS>12)DZS=DZS-12;
      ExtMapBuffer3[i]=iMA(NULL,0,10,0,MODE_SMA,PRICE_HIGH,i);
      ExtMapBuffer4[i]=iMA(NULL,0,10,0,MODE_SMA,PRICE_LOW,i);
      ExtMapBuffer5[i]=iMA(NULL,0,100,0,MODE_SMA,PRICE_CLOSE,i);
      if(((TimeHour(Time[i])>=CJ1 && TimeHour(Time[i])<=CJ1+2) || (TimeHour(Time[i])>=CJ1+6) && (TimeHour(Time[i])<=CJ1+12)) && (iADX(NULL,PERIOD_M15,20,PRICE_HIGH,MODE_MAIN,i+1)<20 && iADX(NULL,PERIOD_M15,20,PRICE_HIGH,MODE_MAIN,i+2)>iADX(NULL,PERIOD_M15,20,PRICE_HIGH,MODE_MAIN,i+1) && iADX(NULL,PERIOD_M15,20,PRICE_HIGH,MODE_MAIN,i)>iADX(NULL,PERIOD_M15,20,PRICE_HIGH,MODE_MAIN,i+1)))ExtMapBuffer2[i]=Low[i]*0.9995;

      if(TimeDay(Time[i])!=TimeDay(TimeCurrent()))

        {

         //-----------------------------------------------xH
/* 
  if(((TimeHour(Time[i])>=CJ1 && TimeHour(Time[i])<=CJ1+3) || (TimeHour(Time[i])==CJ1+6)) && (iStochastic(NULL,PERIOD_M15,9,3,3,MODE_SMA,0,MODE_MAIN,i)>iStochastic(NULL,PERIOD_M15,9,3,3,MODE_SMA,0,MODE_SIGNAL,i) && iStochastic(NULL,PERIOD_M15,9,3,3,MODE_SMA,0,MODE_MAIN,i+1)<iStochastic(NULL,PERIOD_M15,9,3,3,MODE_SMA,0,MODE_SIGNAL,i+1) && iStochastic(NULL,PERIOD_M15,9,3,3,MODE_SMA,0,MODE_MAIN,i)<20 && iStochastic(NULL,PERIOD_M15,9,3,3,MODE_SMA,0,MODE_SIGNAL,i)<20))ExtMapBuffer4[i]=Low[i]*0.999;
  if(((TimeHour(Time[i])>=CJ1 && TimeHour(Time[i])<=CJ1+3) || (TimeHour(Time[i])==CJ1+6)) && ((iMACD(NULL,PERIOD_M15,12,26,9,PRICE_CLOSE,MODE_MAIN,i)>iMACD(NULL,PERIOD_M15,12,26,9,PRICE_CLOSE,MODE_SIGNAL,i) && iMACD(NULL,PERIOD_M15,12,26,9,PRICE_CLOSE,MODE_MAIN,i+1)<iMACD(NULL,PERIOD_M15,12,26,9,PRICE_CLOSE,MODE_SIGNAL,i+1) && iMACD(NULL,PERIOD_M15,12,26,9,PRICE_CLOSE,MODE_MAIN,i)<0)))ExtMapBuffer1[i]=Low[i];

  
  
  
  if((TimeHour(Time[i])>=CJ1 && TimeHour(Time[i])<=CJ2) && (iStochastic(NULL,PERIOD_M15,9,3,3,MODE_SMA,0,MODE_MAIN,i)<iStochastic(NULL,PERIOD_M15,9,3,3,MODE_SMA,0,MODE_SIGNAL,i) && iStochastic(NULL,PERIOD_M15,9,3,3,MODE_SMA,0,MODE_MAIN,i+1)>iStochastic(NULL,PERIOD_M15,9,3,3,MODE_SMA,0,MODE_SIGNAL,i+1) && iStochastic(NULL,PERIOD_M15,9,3,3,MODE_SMA,0,MODE_MAIN,i)>80 && iStochastic(NULL,PERIOD_M15,9,3,3,MODE_SMA,0,MODE_SIGNAL,i)>80))ExtMapBuffer5[i]=High[i]*1.001;
  if((TimeHour(Time[i])>=CJ1 && TimeHour(Time[i])<=CJ2) && ((iMACD(NULL,PERIOD_M15,12,26,9,PRICE_CLOSE,MODE_MAIN,i)<iMACD(NULL,PERIOD_M15,12,26,9,PRICE_CLOSE,MODE_SIGNAL,i) && iMACD(NULL,PERIOD_M15,12,26,9,PRICE_CLOSE,MODE_MAIN,i+1)>iMACD(NULL,PERIOD_M15,12,26,9,PRICE_CLOSE,MODE_SIGNAL,i+1) && iMACD(NULL,PERIOD_M15,12,26,9,PRICE_CLOSE,MODE_MAIN,i)>0)))ExtMapBuffer2[i]=High[i];
 
  
  if((TimeHour(Time[i])>=CJ1 && TimeHour(Time[i])<=CJ2) && (iADX(NULL,PERIOD_M15,20,PRICE_HIGH,MODE_MAIN,i+1)<20 && iADX(NULL,PERIOD_M15,20,PRICE_HIGH,MODE_MAIN,i+2)>iADX(NULL,PERIOD_M15,20,PRICE_HIGH,MODE_MAIN,i+1) && iADX(NULL,PERIOD_M15,20,PRICE_HIGH,MODE_MAIN,i)>iADX(NULL,PERIOD_M15,20,PRICE_HIGH,MODE_MAIN,i+1)))ExtMapBuffer3[i]=Low[i]*0.9995;
*/
         //------------------------------------------------xH

         if(TimeHour(Time[i])==CJ1 && TimeMinute(Time[i])==0)
           {

            TGSJ=Time[i];
            TGJG=Low[i];
            DZSJ=Time[i]+3600*12+SJC;
            DZJG=Low[i-12*XSXS];
            if(TGS==1)
              {
               if(today==1 || today==15)writeDW("DWTG"+ii,"*T",TGSJ,TGJG,Red,15);

               else
                 {
                  if(JQS==0)writeDW("DWTG"+ii,"#T",TGSJ,TGJG,Red,15);
                  else
                    {
                     writeDW("DWTG"+ii,"T",TGSJ,TGJG,Red,15);
                    }
                 }
              }
            if(TGS==2)
              {
               if(today==1 || today==15)writeDW("DWTG"+ii,"*",TGSJ,TGJG,Red,15);

               else
                 {
                  if(JQS==0)writeDW("DWTG"+ii,"#",TGSJ,TGJG,Red,15);
                  else
                    {
                     writeDW("DWTG"+ii,"",TGSJ,TGJG,Red,15);
                    }
                 }
              }
            if(TGS==3)
              {
               if(today==1 || today==15)writeDW("DWTG"+ii,"*V",TGSJ,TGJG,Red,15);

               else
                 {
                  if(JQS==0)writeDW("DWTG"+ii,"#V",TGSJ,TGJG,Red,15);
                  else
                    {
                     writeDW("DWTG"+ii,"V",TGSJ,TGJG,Red,15);
                    }
                 }
              }
            if(TGS==4)
              {
               if(today==1 || today==15)writeDW("DWTG"+ii,"*sS",TGSJ,TGJG,Red,15);

               else
                 {
                  if(JQS==0)writeDW("DWTG"+ii,"#sS",TGSJ,TGJG,Red,15);
                  else
                    {
                     writeDW("DWTG"+ii,"sS",TGSJ,TGJG,Red,15);
                    }
                 }
              }
            if(TGS==5)
              {
               if(today==1 || today==15)writeDW("DWTG"+ii,"*L",TGSJ,TGJG,Red,15);

               else
                 {
                  if(JQS==0)writeDW("DWTG"+ii,"#L",TGSJ,TGJG,Red,15);
                  else
                    {
                     writeDW("DWTG"+ii,"L",TGSJ,TGJG,Red,15);
                    }
                 }
              }
            if(TGS==6)
              {
               if(today==1 || today==15)writeDW("DWTG"+ii,"*Y",TGSJ,TGJG,Red,15);

               else
                 {
                  if(JQS==0)writeDW("DWTG"+ii,"#Y",TGSJ,TGJG,Red,15);
                  else
                    {
                     writeDW("DWTG"+ii,"Y",TGSJ,TGJG,Red,15);
                    }
                 }
              }
            if(TGS==7)
              {
               if(today==1 || today==15)writeDW("DWTG"+ii,"*K",TGSJ,TGJG,Red,15);

               else
                 {
                  if(JQS==0)writeDW("DWTG"+ii,"#K",TGSJ,TGJG,Red,15);
                  else
                    {
                     writeDW("DWTG"+ii,"K",TGSJ,TGJG,Red,15);
                    }
                 }
              }
            if(TGS==8)
              {
               if(today==1 || today==15)writeDW("DWTG"+ii,"*C",TGSJ,TGJG,Red,15);

               else
                 {
                  if(JQS==0)writeDW("DWTG"+ii,"#C",TGSJ,TGJG,Red,15);
                  else
                    {
                     writeDW("DWTG"+ii,"C",TGSJ,TGJG,Red,15);
                    }
                 }
              }
            if(TGS==9)
              {
               if(today==1 || today==15)writeDW("DWTG"+ii,"*",TGSJ,TGJG,Red,15);

               else
                 {
                  if(JQS==0)writeDW("DWTG"+ii,"#",TGSJ,TGJG,Red,15);
                  else
                    {
                     writeDW("DWTG"+ii,"",TGSJ,TGJG,Red,15);
                    }
                 }
              }
            if(TGS==10)
              {
               if(today==1 || today==15)writeDW("DWTG"+ii,"*mM",TGSJ,TGJG,Red,15);

               else
                 {
                  if(JQS==0)writeDW("DWTG"+ii,"#mM",TGSJ,TGJG,Red,15);
                  else
                    {
                     writeDW("DWTG"+ii,"mM",TGSJ,TGJG,Red,15);
                    }
                 }
              }

/* if(TGS==2)  if(today==1 || today==15)writeDW("DWTG"+ii,"*",TGSJ,TGJG,Red,15); 
  else {writeDW("DWTG"+ii,"",TGSJ,TGJG,Red,15); 
       } 
  if(TGS==3)  if(today==1 || today==15)writeDW("DWTG"+ii,"*V",TGSJ,TGJG,Red,15); 
  else {writeDW("DWTG"+ii,"V",TGSJ,TGJG,Red,15); 
       } 
  if(TGS==4)  if(today==1 || today==15)writeDW("DWTG"+ii,"*sS",TGSJ,TGJG,Red,15); 
  else {writeDW("DWTG"+ii,"sS",TGSJ,TGJG,Red,15); 
       } 
  if(TGS==5)  if(today==1 || today==15)writeDW("DWTG"+ii,"*L",TGSJ,TGJG,Red,15); 
  else {writeDW("DWTG"+ii,"L",TGSJ,TGJG,Red,15); 
       } 
  if(TGS==6)  if(today==1 || today==15)writeDW("DWTG"+ii,"*Y",TGSJ,TGJG,Red,15); 
  else {writeDW("DWTG"+ii,"Y",TGSJ,TGJG,Red,15); 
       } 
  if(TGS==7)  if(today==1 || today==15)writeDW("DWTG"+ii,"*K",TGSJ,TGJG,Red,15); 
  else {writeDW("DWTG"+ii,"K",TGSJ,TGJG,Red,15); 
       } 
  if(TGS==8)  if(today==1 || today==15)writeDW("DWTG"+ii,"*C",TGSJ,TGJG,Red,15); 
  else {writeDW("DWTG"+ii,"C",TGSJ,TGJG,Red,15); 
       } 
  if(TGS==9)  if(today==1 || today==15)writeDW("DWTG"+ii,"*",TGSJ,TGJG,Red,15); 
  else {writeDW("DWTG"+ii,"",TGSJ,TGJG,Red,15); 
       } 
  if(TGS==10)  if(today==1 || today==15)writeDW("DWTG"+ii,"*mM",TGSJ,TGJG,Red,15); 
  else {writeDW("DWTG"+ii,"mM",TGSJ,TGJG,Red,15); 
       }   
   */
            //---------------------------------------------
            if(DZS==1) writeDW("DWDZ"+ii,"",DZSJ,DZJG,Blue,15);
            if(DZS==2) writeDW("DWDZ"+ii,"",DZSJ,DZJG,Blue,15);

            if(DZS==3) writeDW("DWDZ"+ii,"T",DZSJ,DZJG,Blue,15);

            if(DZS==4) writeDW("DWDZ"+ii,"",DZSJ,DZJG,Blue,15);

            if(DZS==5) writeDW("DWDZ"+ii,"L",DZSJ,DZJG,Blue,15);
            if(DZS==6) writeDW("DWDZ"+ii,"sS",DZSJ,DZJG,Blue,15);
            if(DZS==7) writeDW("DWDZ"+ii,"V",DZSJ,DZJG,Blue,15);

            if(DZS==8) writeDW("DWDZ"+ii,"Y",DZSJ,DZJG,Blue,15);

            if(DZS==9) writeDW("DWDZ"+ii,"K",DZSJ,DZJG,Blue,15);

            if(DZS==10) writeDW("DWDZ"+ii,"C",DZSJ,DZJG,Blue,15);

            if(DZS==11) writeDW("DWDZ"+ii,"D",DZSJ,DZJG,Blue,15);

            if(DZS==12) writeDW("DWDZ"+ii,"mM",DZSJ,DZJG,Blue,15);
            today1=today-1;
            if(today1<1)today1=14;
            CJ11=today1*0.8;
            CJ21=today1*0.8+12;
            YH1=i+(TimeHour(Time[i])+(24-CJ11))*XSXS;
            YH2=i+(TimeHour(Time[i])+(24-CJ21))*XSXS;
            W=(Close[YH1]+Close[YH2]+2*Close[TimeHour(Time[i])*XSXS+i])/4;
            KSS=i+TimeHour(Time[i])*XSXS;
            TH=W+0.5*(High[iHighest(0,0,MODE_HIGH,ZQS,KSS)]-Low[iLowest(0,0,MODE_LOW,ZQS,KSS)]);
            TL=W-0.5*(High[iHighest(0,0,MODE_HIGH,ZQS,KSS)]-Low[iLowest(0,0,MODE_LOW,ZQS,KSS)]);
            NH=W+(Close[YH1]-Close[YH2]);
            NL=W-(Close[YH1]-Close[YH2]);



            if((TGS==1 || TGS==4 || TGS==5) && (DZS!=3 && DZS!=5 && DZS!=6))
              {

               ZJSJ=Time[i]+3600*9;

              }
            else
              {

               if((TGS!=1 && TGS!=4 && TGS!=5) && (DZS==3 || DZS==5 || DZS==6))
                 {

                  ZJSJ=Time[i]+3600*3;

                 }
               else
                 {

                  ZJSJ=Time[i]+3600*6;

                 }

              }
            writejx("TG"+ii,TGColor,TGSJ,ZJSJ,TH,NH);

            writejx("DZ"+ii,DZColor,ZJSJ,DZSJ,TL,NL);

            ii++;
           }

        }
      else
        {

        }
      //------------------------------循环

     }
//----

   return(0);
  }
//+------------------------------------------------------------------+

void writetext(string Labelname,string data,int x,int y,color ColorValue,int FontSize)//通过Object写文字
  {
   ObjectDelete(Labelname);
   ObjectCreate(Labelname,OBJ_LABEL,0,0,0);
   ObjectSetText(Labelname,data,FontSize,"黑体",ColorValue);
   ObjectSet(Labelname,OBJPROP_CORNER,0);
   ObjectSet(Labelname,OBJPROP_XDISTANCE,x);
   ObjectSet(Labelname,OBJPROP_YDISTANCE,y);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void writejx(string jname,color c1,datetime t1,datetime t2,double p1,double p2)//通过Object写文字
  {
   ObjectDelete(jname);
   ObjectCreate(jname,OBJ_RECTANGLE,0,0,0,0,0);
   ObjectSet(jname,OBJPROP_STYLE,STYLE_SOLID);
   ObjectSet(jname,OBJPROP_COLOR,c1);
   ObjectSet(jname,OBJPROP_TIME1,t1);
   ObjectSet(jname,OBJPROP_PRICE1,p1);
   ObjectSet(jname,OBJPROP_TIME2,t2);
   ObjectSet(jname,OBJPROP_PRICE2,p2);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void writeDNA(string DNA,datetime tttt1,double pppp1,datetime tttt2,double pppp2,color YS,int CX,bool SX)//通过Object写文字
  {
   ObjectDelete(DNA);
   ObjectCreate(DNA,OBJ_TREND,0,0,0,0,0);
   ObjectSet(DNA,OBJPROP_TIME1,tttt1);
   ObjectSet(DNA,OBJPROP_PRICE1,pppp1);
   ObjectSet(DNA,OBJPROP_TIME2,tttt2);
   ObjectSet(DNA,OBJPROP_PRICE2,pppp2);
   ObjectSet(DNA,OBJPROP_RAY,SX);
   ObjectSet(DNA,OBJPROP_WIDTH,CX);
   ObjectSet(DNA,OBJPROP_COLOR,YS);
//ObjectSet(DNA, OBJPROP_STYLE, STYLE_DOT);

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void writeDHL(string DNA,datetime tttt1,double pppp1,datetime tttt2,double pppp2,color YS,int CX,bool SX)//通过Object写文字
  {
   ObjectDelete(DNA);
   ObjectCreate(DNA,OBJ_TREND,0,0,0,0,0);
   ObjectSet(DNA,OBJPROP_TIME1,tttt1);
   ObjectSet(DNA,OBJPROP_PRICE1,pppp1);
   ObjectSet(DNA,OBJPROP_TIME2,tttt2);
   ObjectSet(DNA,OBJPROP_PRICE2,pppp2);
   ObjectSet(DNA,OBJPROP_RAY,SX);
   ObjectSet(DNA,OBJPROP_WIDTH,CX);
   ObjectSet(DNA,OBJPROP_COLOR,YS);
   ObjectSet(DNA,OBJPROP_STYLE,STYLE_DOT);

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void writeDW(string DW,string data2,datetime ttttt1,double ppppp1,color ColorValue,int FontSize)//通过Object写文字
  {
   ObjectDelete(DW);
   ObjectCreate(DW,OBJ_TEXT,0,0,0);
   ObjectSetText(DW,data2,FontSize,"黑体",ColorValue);
   ObjectSet(DW,OBJPROP_TIME1,ttttt1);
   ObjectSet(DW,OBJPROP_PRICE1,ppppp1);

//ObjectSet(DNA, OBJPROP_RAY, False);
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                           fhtz_Sar_indicator.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#include "../core/fh_system_bus.mq4"
#include "../guava/fhTimeManager.mq4"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class Sar_bus : public BUS_System
  {
public:
   virtual void barsReflash(Locker *locker)
     {
      locker.barsChanged();
     }
   virtual void timeManager(TimeManager *tm)
     {
      //是否交易时间
      tm.isPofectTime();
      //自动关闭
      tm.auto_closeOreder();
     }
   virtual double calculateVolume(OrderDoSend *ods,string method,double volumePerOrderNow,double volumeDouble,double mxxxxx,double profit)
     {
      //双倍或指定倍数
      if(method=="double")
        {
         volumePerOrderNow=volumePerOrderNow*volumeDouble;
         ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
        }
       //不变
       if(method=="normal")
        {
         //volumePerOrderNow=volumePerOrderNow*volumeDouble;
         ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
        }
       //已亏损/止盈点数=手数
       if(method=="stoploss2volume")
        {
         volumePerOrderNow=(OrderClosePrice()-OrderOpenPrice())*OrderLots()*mxxxxx/(profit*Point);
         if(volumePerOrderNow<0)volumePerOrderNow=volumePerOrderNow*-1;
         Alert(OrderClosePrice()+"::"+(OrderClosePrice()-OrderOpenPrice())/Point+":"+profit*Point+":="+volumePerOrderNow);
         ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
        }
        
        return volumePerOrderNow;
     }
  };
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                       model1.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict


#include "../Libraries/Locker.mq4"
#include "../Libraries/Notice.mq4"
#include "../Libraries/OrderState.mq4"
#include "../Libraries/Actions.mq4"
#include "../Libraries/AutoClose.mq4"
input double volumePerOrder=1;//手数
input int maxSlippage=5;//最大滑点

input bool closeOnEveryday=true; //每天关闭所有定单
input datetime tBegin_Data=D'23:00';//每天关闭所有订单时间
input bool closeOnFriday=true;   //周五23:30关闭所有定单
input string jianyi="每次操作单位为本金除以10000";
//方浩投资落的单

//int points_good=800;
//int points_bad=250;
int points_good=50;
int points_bad=1750;
Notice *notice;
Locker *locker;
OrderState *orderState;
Actions *actions;
AutoClose *autoclose;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
   notice=new Notice();
   locker=new Locker();
   orderState=new OrderState();
   actions=new Actions(locker,volumePerOrder,maxSlippage,points_bad,points_good);
   autoclose=new AutoClose(closeOnEveryday,tBegin_Data,closeOnFriday,jianyi);
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnTick()
  {
//autoclose.doclose();  //定时关闭订单
//autoclose.getNewOrder();  //喊单
   notice.initPerTick();                //数据准备
                                        //dataCreater.show();
//volumePerOrder=AccountEquity()/10000;
   locker.barsChanged();                     //解锁柱级锁
   int notice_info=notice.getNoticeInfo(orderState); //获取操作信号
   //if(orderState.getHasorder()==true && locker.get("bars")==false)
   if(orderState.getHasorder()==true)
     {
      bool fdzs=orderState.reflashHL();
      if(fdzs)
         //if(false)
        {
         OrderDoSend      *orderDoSend=new OrderDoSend();
         orderDoSend.SetOrderDoSend(orderState,volumePerOrder,maxSlippage,points_bad,points_good);
         orderDoSend.buyCloseAction(orderState.orderNumber);
         locker.unlock("bars");
        }
        if(notice_info%2==0){
            int i=actions.chooseAction(notice_info,orderState);
        }
     }

//有操作信号 不是同向或者没有订单
   if(notice_info!=0
      && ((orderState.getOrderType()*notice_info>0) || orderState.getHasorder()==false)
      && locker.get("bars")==false && orderState.getHasorder()==false)
     {
      int i=actions.chooseAction(notice_info,orderState);
     }
     
      
     
     
   if(orderState.getHasorder()==true)
     {
      actions.closeAuto(orderState);
     }
   locker.barsnowSet();
  }
//+------------------------------------------------------------------+

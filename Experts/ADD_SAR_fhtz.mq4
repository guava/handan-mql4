//+------------------------------------------------------------------+
//|                                                     SAR_fhtz.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#include "../Libraries/fhtz2OrderDoSend.mq4"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+

int addtimes=0;
int addtimestop=3;
input double volumePerOrder=0.01;//手数
double volumePerOrderNow;
input double volumeStep=0.01;
input int step_add=50;

input int maxSlippage=5;//最大滑点
input double mxxxxx=1.5;//反手获利倍数
int maxOrderNumber=3;//最大单数
int hourOfStartTime=9;//开始时间
input int hourOfEndTime=17;//结束时间

input bool closeOrderEveryday=true;//是否每日关闭订单
int orderNumber;
double price;

bool cycleFinished=false; //当天完成了购买周期
int orderTimesInCycle=0;
string dateSave;

string orderType;

int stoploss=1500;
int profit=100;
OrderDoSend *ods;
bool pofectTime=false;
bool  orderOpeningNow=false;

//止盈止损管理 money management
double mm_MA_cost=0;
double mm_Sum_volume=0;

//变色 柱计算
int barsnow=0;
bool lock_donot_close=false;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
   dateSave=TimeToStr(TimeCurrent(),TIME_DATE);
   ods=new OrderDoSend();
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   if(Bars>barsnow)
     {
      lock_donot_close=false;
     }
   string dateCurrent=TimeToStr(TimeCurrent(),TIME_DATE);
   int nowhour=TimeHour(TimeCurrent());
   if(nowhour>(hourOfStartTime-1) && nowhour<hourOfEndTime)
     {
      pofectTime=true;
        }else{
      pofectTime=false;
     }
//周五11：30关闭所有订单
   if(4<DayOfWeek() && DayOfWeek()<6 && Hour()==23 && Minute()>30)
     {
      closeAllOrders();
      Alert("d5 here:"+DayOfWeek());
     }

//当日关闭所有订单
   if(0<DayOfWeek() && DayOfWeek()<5 && Hour()==23 && Minute()>50 && closeOrderEveryday==true)
     {
      closeAllOrders();
      //Alert("d1-4 here:"+nowhour+":"+hourOfEndTime);
     }

   double green0=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",0,0);
   double green1=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",0,1);
   double green2=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",0,2);
   double red0=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",1,0);

   ods=new OrderDoSend();

   int ttOrder=OrdersTotal();
   if(ttOrder>0)
     {
      //计算止盈止损
      if(orderType=="buy" && ((Bid-mm_MA_cost)>profit*Point || (mm_MA_cost-Bid)>stoploss*Point))
        {
         closeAllOrders();
         Alert("c1 here");
         addtimes=0;
        }

      if(orderType=="sell" && ((Ask-mm_MA_cost)>stoploss*Point || (mm_MA_cost-Ask)>profit*Point))
        {
         closeAllOrders();
         Alert("c2 here");
         addtimes=0;
        }

      if((green2*green1)<0 && lock_donot_close==false)
        {
         closeAllOrders();
         lock_donot_close=true;
         Alert("c3 here");
         addtimes=0;
        }
     }
   int ttOrder2=OrdersTotal();
   if(ttOrder2==0 && pofectTime==true)
     {
      if(green1>0)
        {
         firstSell();
           }else{
         firstBuy();
        }
     }
   if(ttOrder2>0 && pofectTime==true)
     {
      if(orderType=="sell")
        {
         //Alert(addtimes+":"+addtimestop);
         if((price-Bid)>step_add*Point && addtimes<(addtimestop-1))
           {
            Alert("going to do sell");
            volumePerOrderNow=volumePerOrderNow+volumeStep;
            nofirstSell();
           }
           }else{
         if((Ask-price)>step_add*Point && addtimes<(addtimestop-1))
           {
            volumePerOrderNow=volumePerOrderNow+volumeStep;
            nofirstBuy();
           }
        }
     }
   barsnow=Bars;
  }
//+------------------------------------------------------------------+
void firstSell()
  {
   ods.SetOrderDoSend(volumePerOrder,maxSlippage,stoploss+100,profit+100);
   orderNumber=ods.openSellAction();
   if(orderNumber!=-1)
     {
      orderOpeningNow=true;
      orderType="sell";
      orderTimesInCycle=orderTimesInCycle+1;
      volumePerOrderNow=volumePerOrder;
      bool oooo=OrderSelect(orderNumber,SELECT_BY_TICKET);
      if(oooo)
        {
         mm_MA_cost=OrderOpenPrice();
         mm_Sum_volume=OrderLots();
         price=OrderOpenPrice();
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void nofirstSell()
  {
   ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+100,profit+100);
   orderNumber=ods.openSellAction();
//Alert(orderNumber+" is do "+addtimes+" times sell");
   if(orderNumber!=-1)
     {
      orderOpeningNow=true;
      orderType="sell";
      orderTimesInCycle=orderTimesInCycle+1;
      addtimes=addtimes+1;
      //Alert("do "+addtimes+" times sell");
      //volumePerOrderNow=volumePerOrder;
      bool oooo=OrderSelect(orderNumber,SELECT_BY_TICKET);
      if(oooo)
        {
         mm_MA_cost=(OrderOpenPrice()*OrderLots()+mm_MA_cost*mm_Sum_volume)/(OrderLots()+mm_Sum_volume);
         mm_Sum_volume=OrderLots()+mm_Sum_volume;
         price=OrderOpenPrice();
         modifyAllOrders(mm_MA_cost);

        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void firstBuy()
  {
   ods.SetOrderDoSend(volumePerOrder,maxSlippage,stoploss+100,profit+100);
   orderNumber=ods.openBuyAction();
   if(orderNumber!=-1)
     {
      orderOpeningNow=true;

      orderType="buy";
      orderTimesInCycle=orderTimesInCycle+1;
      volumePerOrderNow=volumePerOrder;
      bool oooo=OrderSelect(orderNumber,SELECT_BY_TICKET);
      if(oooo)
        {
         mm_MA_cost=OrderOpenPrice();
         price=OrderOpenPrice();
         mm_Sum_volume=OrderLots();
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void nofirstBuy()
  {
   ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+100,profit+100);
   orderNumber=ods.openBuyAction();
//Alert(orderNumber+" is do "+addtimes+" times buy");
   if(orderNumber!=-1)
     {
      orderOpeningNow=true;

      orderType="buy";
      orderTimesInCycle=orderTimesInCycle+1;
      addtimes=addtimes+1;
      //volumePerOrderNow=volumePerOrder;
      bool oooo=OrderSelect(orderNumber,SELECT_BY_TICKET);
      if(oooo)
        {
         mm_MA_cost=(OrderOpenPrice()*OrderLots()+mm_MA_cost*mm_Sum_volume)/(OrderLots()+mm_Sum_volume);
         mm_Sum_volume=OrderLots()+mm_Sum_volume;
         price=OrderOpenPrice();
         modifyAllOrders(mm_MA_cost);

        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void closeAllOrders()
  {
   int total=OrdersTotal();
   for(int i=total-1;i>=0;i--)
     {
      bool sel=OrderSelect(i,SELECT_BY_POS);
      int type=OrderType();

      bool result=false;

      if(type==OP_BUY)
        {
         //Close opened long positions
         result=OrderClose(OrderTicket(),OrderLots(),MarketInfo(OrderSymbol(),MODE_BID),5,Red);
           }else{

         result=OrderClose(OrderTicket(),OrderLots(),MarketInfo(OrderSymbol(),MODE_ASK),5,Red);

        }

      if(result==false)
        {
         //Alert("Order ",OrderTicket()," failed to close. Error:",GetLastError());
         Sleep(3000);
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void modifyAllOrders(double mm_MA_cost)
  {
   int total=OrdersTotal();
   for(int i=total-1;i>=0;i--)
     {
      bool sel=OrderSelect(i,SELECT_BY_POS);
      int type=OrderType();

      bool result=false;

      if(type==OP_BUY)
        {
         //Close opened long positions
         OrderModify(OrderTicket(),OrderOpenPrice(),mm_MA_cost-(stoploss*Point+100*Point),mm_MA_cost+(profit*Point+100*Point),0,White);
           }else{

         OrderModify(OrderTicket(),OrderOpenPrice(),mm_MA_cost+(stoploss*Point+100*Point),mm_MA_cost-(profit*Point+100*Point),0,White);

        }

     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                        guava.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#include "../Libraries/OrderState.mq4"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class OrderDoSend
  {
protected:
   OrderState       *orderState;
   double            volumePerOrder;
   int               maxSlippage;
   int               points_bad;
   int               points_good;
public:
   void SetOrderDoSend(OrderState *os,double volumePerOrder_,int maxSlippage_,int points_bad_,int points_good_)
     {
      orderState=os;
      volumePerOrder=volumePerOrder_;
      maxSlippage=maxSlippage_;
      points_bad=points_bad_;
      points_good=points_good_;
     }

   void openBuyAction(int n)
     {

      
      points_good=iCustom(NULL,0,"ATR",14,0,0)/0.00001*2;
      //points_bad=iCustom(NULL,0,"ATR",14,0,0)/0.00001;
      orderState.orderNumber=OrderSend(Symbol(),OP_BUY,volumePerOrder,Ask,maxSlippage,Ask-points_bad*Point,Ask+points_good*Point);
      //orderState.orderNumber=OrderSend(Symbol(),OP_SELL,volumePerOrder,Bid,maxSlippage,Bid+points_bad*Point,Bid-points_good*Point);
      orderState.setOrderType(n);
      orderState.setHasOrder(true);
      orderState.price=Ask;
      orderState.hight=Ask;
      orderState.low=Ask;
     }

   void openSellAction(int n)
     {
      //Print("sell open:",isup);
      orderState.orderNumber=OrderSend(Symbol(),OP_SELL,volumePerOrder,Bid,maxSlippage,Bid+points_bad*Point,Bid-points_good*Point);
      orderState.setOrderType(n);
      orderState.setHasOrder(true);
      orderState.price=Bid;
      orderState.hight=Bid;
      orderState.low=Bid;
     }

   void buyCloseAction(int order_number)
     {
      OrderClose(order_number,volumePerOrder,Bid,3,White);
      orderState.setHasOrder(false);
      orderState.setOrderType(0);
     }

   void sellCloseAction(int order_number)
     {

      OrderClose(order_number,volumePerOrder,Ask,3,White);
      orderState.setHasOrder(false);
      orderState.setOrderType(0);
     }

  };
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                       Locker.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class Locker
  {
protected:
   bool              barsLock;
   bool              bsLock;
   int               barsnow;
public:
   void Locker()
     {
      barsLock=false;
      bsLock=false;
      barsnow=0;
     }
   void lock(string n)
     {
      if(n=="bars")
        {
         barsLock=true;
        }
      if(n=="bs")
        {
         bsLock=false;
        }
     };

   void unlock(string n)
     {
      if(n=="bars")
        {
         barsLock=false;
        }
      if(n=="bs")
        {
         bsLock=false;
        }
     };

   void barsChanged()
     {
      if(Bars>barsnow)
        {
         barsLock=false;
        }
     }

   bool get(string n)
     {
      bool value=false;
      if(n=="bars")
        {
         value=barsLock;
        }
      if(n=="bs")
        {
         value=bsLock;
        }
      return value;
     }
   void barsnowSet()
     {
      barsnow=Bars;
     }
  };
//+------------------------------------------------------------------+

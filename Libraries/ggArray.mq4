//+------------------------------------------------------------------+
//|                                                      ggArray.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| My function                                                      |
//+------------------------------------------------------------------+
// int MyCalculator(int value,int value2) export
//   {
//    return(value+value2);
//   }
//+------------------------------------------------------------------+
class ArrayHelper
  {
public:
   void RemoveIndexFromStringArray(string &MyArray[],int index)
     {
      string TempArray[];
      //Lets copy index 0-4 as the input index is to remove index 5
      ArrayCopy(TempArray,MyArray,0,0,index);
      //Now Copy index 6-9, start from 6  as the input index is to remove index 5
      ArrayCopy(TempArray,MyArray,index,(index+1));

      //copy Array back
      ArrayFree(MyArray);
      ArrayCopy(MyArray,TempArray,0,0);
     }
    void RemoveIndexFromIntArray(int &MyArray[],int index)
     {
      int TempArray[];
      //Lets copy index 0-4 as the input index is to remove index 5
      ArrayCopy(TempArray,MyArray,0,0,index);
      //Now Copy index 6-9, start from 6  as the input index is to remove index 5
      ArrayCopy(TempArray,MyArray,index,(index+1));

      //copy Array back
      ArrayFree(MyArray);
      ArrayCopy(MyArray,TempArray,0,0);
     }
     void RemoveIndexFromDoubleArray(double &MyArray[],int index)
     {
      double TempArray[];
      //Lets copy index 0-4 as the input index is to remove index 5
      ArrayCopy(TempArray,MyArray,0,0,index);
      //Now Copy index 6-9, start from 6  as the input index is to remove index 5
      ArrayCopy(TempArray,MyArray,index,(index+1));

      //copy Array back
      ArrayFree(MyArray);
      ArrayCopy(MyArray,TempArray,0,0);
     }
  };
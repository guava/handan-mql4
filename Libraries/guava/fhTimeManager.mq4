//+------------------------------------------------------------------+
//|                                                fhTimeManager.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class TimeManager
  {
protected:
   string            dateCurrent;//=TimeToStr(TimeCurrent(),TIME_DATE);
   int               hourOfStartTime;//=9;//开始时间
   int               hourOfEndTime;//=19;//结束时间
   bool              pofectTime;
   bool              closeOrderEveryday;
   void closeAllOrders()
     {
      int total=OrdersTotal();
      for(int i=total-1;i>=0;i--)
        {
         bool sel=OrderSelect(i,SELECT_BY_POS);
         int type=OrderType();

         bool result=false;

         if(type==OP_BUY)
           {
            result=OrderClose(OrderTicket(),OrderLots(),MarketInfo(OrderSymbol(),MODE_BID),5,Red);
              }else{
            result=OrderClose(OrderTicket(),OrderLots(),MarketInfo(OrderSymbol(),MODE_ASK),5,Red);
           }

         if(result==false)
           {
            Alert("Order ",OrderTicket()," failed to close. Error:",GetLastError());
            //Sleep(3000);
           }
        }
     };
public:
   void TimeManager(int vStart,int varEnd,bool var_closeOrderEveryday)
     {
      hourOfStartTime=vStart;
      hourOfEndTime=varEnd;
      pofectTime=false;
      closeOrderEveryday=var_closeOrderEveryday;
     }
   void isPofectTime()
     {
      int nowhour=TimeHour(TimeCurrent());
      if(nowhour>(hourOfStartTime-1) && nowhour<hourOfEndTime)
        {
         pofectTime=true;
           }else{
         pofectTime=false;
        }
      //return pofectTime;
     }
   bool getPofectTime()
     {
      return pofectTime;
     }
   void setPorfectime(bool varPofectime){
      pofectTime=varPofectime;
   }
   void auto_closeOreder()
     {
      //周五11：30关闭所有订单
      if(4<DayOfWeek()<6 && Hour()==hourOfEndTime && Minute()>30)
        {
         closeAllOrders();
        }

      //当日关闭所有订单
      if(0<DayOfWeek()<5 && Hour()==hourOfEndTime && closeOrderEveryday==true)
        {
         closeAllOrders();
        }
     }
    string get_dateCurrent(){
      string dateCurrent=TimeToStr(TimeCurrent(),TIME_DATE);
      return dateCurrent;
    }
  };
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                       GA-ind.mq4 |
//|                      Copyright ?2008, MetaQuotes Software Corp. |
//|                                        http://www.metaquotes.net |
//+------------------------------------------------------------------+
#property copyright "Copyright ?2008, MetaQuotes Software Corp."
#property link      "http://www.metaquotes.net"

//---- indicator settings
#import "VCMail.dll"
bool  VC_SendMail(int nIndicatorSignal,string strSymbol);
#import

#property  indicator_separate_window
#property  indicator_buffers 4
#property  indicator_color1  Gray
#property  indicator_color2  Blue
#property  indicator_color3  Red
#property  indicator_color4  Yellow
#property  indicator_width1  1
#property  indicator_width2  1
#property  indicator_width3  1

bool  vc_g_bIsStartupMail=TRUE;         //开启邮件提醒
int  vc_g_nStartTimeMail   = 11;              //邮件发送时间
int  vc_g_nEndTimeMail     = 23;             //邮件结束时间

//---- indicator parameters
int FastEMA=8;
int SlowEMA=21;
int SignalSMA=8;
//---- indicator buffers
double     MacdBufferUp[];
double     MacdBufferDn[];
double     SignalBuffer[];
double     MacdBuffer[];
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
//---- drawing settings
   SetIndexStyle(0,DRAW_ARROW);
   SetIndexStyle(1,DRAW_ARROW);
   SetIndexStyle(2,DRAW_ARROW);

   SetIndexStyle(3,DRAW_LINE);
   SetIndexDrawBegin(3,SignalSMA);
   IndicatorDigits(Digits+1);
//---- indicator buffers mapping
   SetIndexBuffer(0,MacdBuffer);
   SetIndexBuffer(1,MacdBufferUp);
   SetIndexBuffer(2,MacdBufferDn);

   SetIndexBuffer(3,SignalBuffer);
//---- name for DataWindow and indicator subwindow label
   IndicatorShortName("MACD-喊单("+FastEMA+","+SlowEMA+","+SignalSMA+")");
   SetIndexLabel(0,"");
   SetIndexLabel(1,"MACD UP");
   SetIndexLabel(2,"MACD DN");

   SetIndexLabel(3,"Signal");
//---- initialization done
bool ssss2=VC_SendMail(0,"（"+TimeToStr(TimeGMT()-TimeGMTOffset())+"）test；");
   
   return(0);
  }
//+------------------------------------------------------------------+
//| Moving Averages Convergence/Divergence                           |
//+------------------------------------------------------------------+
int barsnow=0;
int test=1;
//int start()
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])

  {
   int limit;
   int counted_bars=IndicatorCounted();
   if(counted_bars>0) counted_bars--;
   limit=Bars-counted_bars;

   for(int i=0; i<limit; i++)MacdBuffer[i]=iMA(NULL,0,FastEMA,0,MODE_EMA,PRICE_CLOSE,i)+iMA(NULL,0,SlowEMA,0,MODE_EMA,PRICE_CLOSE,i);

   for(i=0; i<limit; i++)SignalBuffer[i]=iMAOnArray(MacdBuffer,Bars,SignalSMA,0,MODE_SMA,i);

   for(i=0; i<limit; i++)
     {
      MacdBufferUp[i]=EMPTY_VALUE;
      MacdBufferDn[i]=EMPTY_VALUE;

      if(MacdBuffer[i]>SignalBuffer[i])
        {
         MacdBufferUp[i]=MacdBuffer[i];
        }
      if(MacdBuffer[i]<SignalBuffer[i])
        {
         MacdBufferDn[i]=MacdBuffer[i];
        }

     }

   if(vc_g_bIsStartupMail && TimeHour(TimeGMT()-TimeGMTOffset())>=vc_g_nStartTimeMail && TimeHour(TimeGMT()-TimeGMTOffset())<=vc_g_nEndTimeMail)
   {
      if(barsnow<Bars)
        {
         barsnow=Bars;
         if(MacdBuffer[1]>SignalBuffer[1] && MacdBuffer[2]<SignalBuffer[2]){
            bool ssss1=VC_SendMail(0,"（"+TimeToStr(TimeGMT()-TimeGMTOffset())+"）镑日在M5时间显示做多信号:");
         }
         if(MacdBuffer[1]<SignalBuffer[1] && MacdBuffer[2]>SignalBuffer[2]){
            bool ssss3=VC_SendMail(0,"（"+TimeToStr(TimeGMT()-TimeGMTOffset())+"）镑日在M5时间显示做空信号:");
         }
         
         }
   }

//---- done
   return(0);
  }
//+------------------------------------------------------------------+

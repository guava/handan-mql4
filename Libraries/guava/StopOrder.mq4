//+------------------------------------------------------------------+
//|                                                       Locker.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class StopOrder
  {
protected:
   double            l_price;
   double            h_price;

public:
   void StopOrder()
     {
      l_price=1000;
      h_price=-1000;
     }
   void flashPrice()
     {
/*
      if(Bid>h_price)
        {
         h_price=Bid;
        }
      if(Ask<l_price)
        {
         l_price=Ask;
        }
      if(OrderType()==OP_BUY && h_price>(OrderOpenPrice()+10*Point) && (Bid-OrderOpenPrice())<10*Point)
        {
         Print((Bid-OrderOpenPrice())+":"+30*Point);

         //closeAllOrders();
         Print("买单保护不亏");
         l_price=1000;
         h_price=-1000;
        }
      if(OrderType()==OP_SELL && l_price<(OrderOpenPrice()-10*Point) && (OrderOpenPrice()-Ask)<10*Point)
        {
         closeAllOrders();
         Print("卖单保护不亏");
         l_price=1000;
         h_price=-1000;
        }
*/
     }
   void bigLostToday()
     {
      OrderSelect(0,SELECT_BY_POS);
      if(OrderType()==OP_BUY && h_price>OrderOpenPrice() && (h_price-Bid)>100*Point)
        {
         closeAllOrders();
         Print("移动止盈");
         l_price=1000;
         h_price=-1000;
        }
      if(OrderType()==OP_SELL && l_price<OrderOpenPrice() && (Ask-l_price)>100*Point)
        {
         closeAllOrders();
         Print("移动止盈");
         l_price=1000;
         h_price=-1000;
        }

      if(AccountProfit()>0)
        {
         if(MathAbs(AccountProfit()/volumePerOrder)>400)
           {

            Print("固定止盈:"+AccountProfit());
            closeAllOrders();
           }
           }else{

         if(MathAbs(AccountProfit()/volumePerOrder)>1500)
           {
            Print("固定止损:"+AccountProfit());
            closeAllOrders();

           }

/*
         double lows=Low[iHighest(NULL,0,MODE_LOW,3,1)];

         double highs=High[iHighest(NULL,0,MODE_HIGH,3,1)];
         double highs2=iMA(NULL,NULL,3,0,MODE_EMA,PRICE_HIGH,0);
         if(OrderType()==OP_BUY && Bid<lows)
           {
            Print("前日止损法"+AccountProfit());
            closeAllOrders();

           }
         if(OrderType()==OP_SELL && Ask>highs)
           {
            Print(highs+":前日止损法"+AccountProfit());
            closeAllOrders();

           }
*/
        }
/*
      if(OrderType()==OP_BUY && OrderOpenPrice()-Bid>150*Point)
        {
         closeAllOrders();
         Print("移动止损");
         l_price=1000;
         h_price=-1000;
        }
      if(OrderType()==OP_SELL && Ask-OrderOpenPrice()>150*Point)
        {
         closeAllOrders();
         Print("移动止损");
         l_price=1000;
         h_price=-1000;
        }
        */
     }
  };
//+------------------------------------------------------------------+

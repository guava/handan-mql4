//+------------------------------------------------------------------+
//|                                                  DataCreater.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#include "../Libraries/OrderState.mq4"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class Notice
  {
protected:
   int               period_guava;
   double            o[4];
   double            c[4];
   double            H[4];
   double            L[4];
   double            gmacd;
   double            gmacd1;
   double            gmacd2;
   OrderState       *orderState;
   int greenBiggerRed1;
public:
/*
   void DataCreater()
     {
      period_guava=0;
      //period_guava=PERIOD_M30;
     }
*/

   int getNoticeInfo(OrderState *orderstate)
     {
      this.orderState=orderstate;
      int notice_info=0;
      
      
      double c0=iCustom(NULL,0,"test",3,1,0);
      double o0=iCustom(NULL,0,"test",3,0,0);
      
      double c1=iCustom(NULL,0,"test",3,1,1);
      double o1=iCustom(NULL,0,"test",3,0,1);
      
      double c14=iMA(NULL,period_guava,14,0,MODE_EMA,PRICE_CLOSE,0);
      
      double bias1=(MathAbs(100*(Close[0]- iMA(NULL,0,6,0,MODE_EMA,PRICE_CLOSE,0))/iMA(NULL,0,6,0,MODE_EMA,PRICE_CLOSE,0))
      +MathAbs(100*(Close[1]- iMA(NULL,0,6,0,MODE_EMA,PRICE_CLOSE,1))/iMA(NULL,0,6,0,MODE_EMA,PRICE_CLOSE,1))
      +MathAbs(100*(Close[2]- iMA(NULL,0,6,0,MODE_EMA,PRICE_CLOSE,2))/iMA(NULL,0,6,0,MODE_EMA,PRICE_CLOSE,2)) )/3.0;
      
      
      //买入信号
      //if((c[1]<o[1]||c[1]==o[1]||) && c[0]>o[0] && gmacd>gmacd1 && gmacd1>gmacd2 && bias1>0.045)notice_info=21; //前日上传
      //if(c[1]>o[1] && c[0]>o[0] && gmacd>gmacd1 && gmacd1>gmacd2 && bias1>0.045)notice_info=21; //01两天上升
      if(c0>o0 && gmacd>gmacd1 && Ask<c14 && bias1>0.031)notice_info=21; //01两天上升
      //if(o[1]>o[0] && c[0]>c[1] && Ask>c[1] && gmacd>gmacd1 && gmacd1>gmacd2)notice_info=21; //上穿H
                                                                              //if(c[2]>o[2] && c[1]<o[1] &&  orderState.getOrderType()==21)notice_info=22;

      
      if(c1>o1 && o0>c0)notice_info=22; //down
      //if(c[2]<o[2] && o[1]>c[1])notice_info=22; //H上 平行
      return notice_info;

     }
   void initPerTick()
     {
      period_guava=0;
      
      o[0]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_OPEN,0);
      o[1]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_OPEN,1);
      o[2]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_OPEN,2);
      o[3]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_OPEN,3);
      c[0]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_CLOSE,0);
      c[1]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_CLOSE,1);
      c[2]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_CLOSE,2);
      c[3]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_CLOSE,3);

      H[0]=iMA(NULL,period_guava,8,0,MODE_EMA,PRICE_HIGH,0);
      H[1]=iMA(NULL,period_guava,8,0,MODE_EMA,PRICE_HIGH,1);
      H[2]=iMA(NULL,period_guava,8,0,MODE_EMA,PRICE_HIGH,2);
      L[0]=iMA(NULL,period_guava,8,0,MODE_EMA,PRICE_LOW,0);
      L[1]=iMA(NULL,period_guava,8,0,MODE_EMA,PRICE_LOW,1);
      L[2]=iMA(NULL,period_guava,8,0,MODE_EMA,PRICE_LOW,2);
      //gmacd=iMACD(NULL,0,12,26,9,PRICE_CLOSE,MODE_MAIN,0);
      gmacd=iCustom(NULL,0,"MACD",12,26,9,0,0);
      gmacd1=iCustom(NULL,0,"MACD",12,26,9,0,1);
      gmacd2=iCustom(NULL,0,"MACD",12,26,9,0,2);
      
     }
  };
//+------------------------------------------------------------------+

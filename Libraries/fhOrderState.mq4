//+------------------------------------------------------------------+
//|                                                    OderState.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class OrderState
  {
protected:
   bool              hasOrder;
   int               orderType;

public:
   double            price;
   double            hight;
   double            low;
   int               orderNumber;
   int               trailingStop;
   void OrderState()
     {
      hasOrder=false;
      orderType=0;
      trailingStop=50;
      hight=0;
      low=0;
     }
   void set(bool hasorder,int ordertype)
     {
      hasOrder=hasorder;
      orderType=ordertype;
     }
   bool getHasorder()
     {
      return hasOrder;
     }
   int getOrderType()
     {
      return orderType;
     }
   void setOrderType(int n)
     {
      orderType=n;
     }
   void setHasOrder(bool n)
     {
      this.hasOrder=n;
     }

   bool reflashHL()
     {
      //Print(hight,"---"+low+"("+Ask+","+Bid+")");
      if(Ask>hight)hight=Ask;
      if(Bid<low)low=Bid;
      if(orderType>0)
        {
         //buy action
         if((hight-Ask)>(50)*Point && Ask>price)
           {
            return true;
           }
         if((hight-Ask)>50*Point && Ask<price)//这里还是有点问题的
           {
            return true;
           }
        }
      if(orderType<0)
        {
        //2016.05.03 13:50:00.495	2016.04.27 23:58  model1 EURUSD,M15: Alert: 0.0005---0.0005699999999999594


     
         //Alert((price-Bid)+"="+price+"-"+Bid);
         //buy action
         if((Bid-low)>(50*Point) && Bid<price)
           {
            return true;
           }
         if((Bid-price)>(50*Point) && Bid>price)//这里还是有点问题的
           {
            //Alert("止损");
            return true;
           }
        }

      return false;
     }
     
  };
//+------------------------------------------------------------------+

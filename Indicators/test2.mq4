//+------------------------------------------------------------------+
//|                                                         test.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <MovingAverages.mqh>
//--- indicator settings
#property  indicator_separate_window 
//#property indicator_chart_window  //主图
#property  indicator_buffers 4
#property  indicator_color1  Red//Silver
#property  indicator_color2  Green
#property  indicator_color3  Yellow 
#property  indicator_color4  White 
//--- input parameters
int      step=7;
int stepHL=12;
//--- buffers
double openp[];
double closep[];
double hight[];
double blow[];
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   IndicatorBuffers(4);
   SetIndexBuffer(0,openp);
   SetIndexBuffer(1,closep);
   SetIndexBuffer(2,hight);
   SetIndexBuffer(3,blow); 
   
   //--- indicator line
   SetIndexStyle(0,DRAW_NONE);
   //SetIndexBuffer(0,openp);
   //--- indicator line
   SetIndexStyle(1,DRAW_NONE);
   //SetIndexBuffer(1,closep);
   
   SetIndexStyle(2,DRAW_NONE);
   //SetIndexBuffer(2,hight);
   SetIndexStyle(3,DRAW_LINE);
   
   
   IndicatorShortName("test相减");
   SetIndexLabel(0,"test");
   SetIndexDrawBegin(0,stepHL);
   SetIndexDrawBegin(1,stepHL);
   SetIndexDrawBegin(2,stepHL);
   SetIndexDrawBegin(3,stepHL);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   int i=0,limit;
   if(rates_total<=8)
      return(0);
   limit=rates_total-prev_calculated;
   if(prev_calculated>0)
      limit++;
      
//--- macd counted in the 1-st buffer
   for(i=0; i<limit; i++){
   /*
      openp[i]=iMA(NULL,PERIOD_M30,step,0,MODE_EMA,PRICE_OPEN,i);
      closep[i]=iMA(NULL,PERIOD_M30,step,0,MODE_EMA,PRICE_CLOSE,i);
      hight[i]=iMA(NULL,PERIOD_M30,8,0,MODE_EMA,PRICE_HIGH,i);
      blow[i]=iMA(NULL,PERIOD_M30,8,0,MODE_EMA,PRICE_LOW,i);
      */
      openp[i]=iMA(NULL,NULL,step,0,MODE_EMA,PRICE_OPEN,i);
      closep[i]=iMA(NULL,NULL,step,0,MODE_EMA,PRICE_CLOSE,i);
      //hight[i]=iMA(NULL,NULL,stepHL,0,MODE_EMA,PRICE_HIGH,i);
      //blow[i]=iMA(NULL,NULL,stepHL,0,MODE_EMA,PRICE_LOW,i);
      hight[i]=closep[i]-openp[i];
      //Print(closep[0]+":"+openp[0]+":    "+hight[0]);
      blow[i]=(iMA(NULL,NULL,step,0,MODE_EMA,PRICE_OPEN,i)-iMA(NULL,NULL,step,0,MODE_EMA,PRICE_CLOSE,i)
      +iMA(NULL,NULL,step,0,MODE_EMA,PRICE_OPEN,i+1)-iMA(NULL,NULL,step,0,MODE_EMA,PRICE_CLOSE,i+1)
      +iMA(NULL,NULL,step,0,MODE_EMA,PRICE_OPEN,i+2)-iMA(NULL,NULL,step,0,MODE_EMA,PRICE_CLOSE,i+2)
      )/-3;
      
   }
   
 
   

   return(rates_total);
  }
//+------------------------------------------------------------------+
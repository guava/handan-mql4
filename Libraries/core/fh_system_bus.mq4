//+------------------------------------------------------------------+
//|                                                fhTimeManager.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#include "../guava/fhLocker.mq4"
#include "../guava/fhTimeManager.mq4"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class BUS_System
  {
public:
   virtual void barsReflash(Locker *locker){Alert("bars reflash");}
   virtual void timeManager(TimeManager *tm){Alert("timeManager");}
   
   //mxxxxx 反手获利倍数
   virtual double calculateVolume(OrderDoSend *ods,string method,double volumePerOrderNow,double volumeDouble,double mxxxxx,double profit){return 0.01;}
private:
   double            m_legs_count;  // The number of the animal's legs
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+

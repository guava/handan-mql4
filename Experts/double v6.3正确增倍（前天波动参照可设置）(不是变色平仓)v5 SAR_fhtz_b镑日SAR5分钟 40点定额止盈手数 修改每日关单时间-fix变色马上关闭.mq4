//+------------------------------------------------------------------+
//|                                                     SAR_fhtz.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#include "../Libraries/fhtz2OrderDoSend.mq4"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
input double volumePerOrder=0.1;//手数
double volumePerOrderNow;
input int maxSlippage=5;//最大滑点
input double mxxxxx=0.8;//反手获利倍数 (1.5)
input double volumeDouble=2;//订单量增倍

int maxOrderNumber=5;//最大单数(4)
int hourOfStartTime=9;//开始时间
input int hourOfEndTime=19;//结束时间

input bool closeOrderEveryday=true;//是否每日关闭订单
int orderNumber;
double price;

bool cycleFinished=false; //当天完成了购买周期
int orderTimesInCycle=0;
string dateSave;

string orderType;

int stoploss=1500;
double profit=400;
OrderDoSend *ods;
bool pofectTime=false;
bool  orderOpeningNow=false;

double profit2;
//tomorrow higher-lower
double higher=0;
double   lower=100000;
int higher_low_point=1500;//19-次日9点波动 默认1500，不采用该策略设置为1500000
double higher_lower_lock=false;
double higher_lower_too_big=false;

//变色操作相关
bool bars_lock=false;
int bars_now=0;
int protect_close_ourSelf=23456;//变色平仓设23456  不变色平仓设100 
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
   dateSave=TimeToStr(TimeCurrent(),TIME_DATE);
   ods=new OrderDoSend();
   profit2=profit;
   bars_now=Bars;
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   if(bars_now<Bars)
     {
      bars_lock=false;
      bars_now=Bars;
     }

   string dateCurrent=TimeToStr(TimeCurrent(),TIME_DATE);
   int nowhour=TimeHour(TimeCurrent());
   if(nowhour>(hourOfEndTime-1) || nowhour<hourOfStartTime)
     {

      if(Ask>higher)higher=Ask;
      if(Bid<lower)lower=Bid;
      //Alert("now is h-l:"+higher+":"+lower);
      if(higher_lower_lock==true)
        {
         higher_lower_lock=false;
         higher_lower_too_big=false;
        }
     }

   if(nowhour==hourOfStartTime && higher_lower_lock==false)
     {

      if((higher-lower)>higher_low_point*Point)
        {
         Alert(higher+":"+lower);
         higher_lower_too_big=true;
        }
      higher=0;
      lower=10000;
      higher_lower_lock=true;
     }
   if(nowhour>(hourOfStartTime-1) && nowhour<hourOfEndTime && higher_lower_too_big==false)
     {
      pofectTime=true;
        }else{
      pofectTime=false;
     }
   int totalOrderNumbers=OrdersTotal();
//周五11：30关闭所有订单
   if(4<DayOfWeek()<6 && Hour()==hourOfEndTime && Minute()>30)
     {
      closeAllOrders();
     }

//当日关闭所有订单
   if(0<DayOfWeek()<5 && Hour()==hourOfEndTime && closeOrderEveryday==true)
     {
      closeAllOrders();
     }

   double green1=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",0,1);
   double red1=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",1,1);
   double green2=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",0,2);
   double red2=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",1,2);
//Alert(green+":"+red);
   ods=new OrderDoSend();

//防止超大滑点
   bool sel=OrderSelect(orderNumber,SELECT_BY_TICKET);
   bool close_price=OrderClosePrice();
//Alert("偷袭:"+close_price+":"+orderOpeningNow);
//if(close_price!=true && orderOpeningNow==true)
   if(OrdersTotal()==0 && orderOpeningNow==true)
     {
      //Alert("偷袭:"+close_price);
      if(orderType=="sell")
        {
         double closePrice=OrderClosePrice();

         if(closePrice>price && orderTimesInCycle<maxOrderNumber && pofectTime && cycleFinished==false)
           {
            //Alert(closePrice+":"+price);
            //Alert(orderTimesInCycle);
            //profit=(stoploss*volumePerOrderNow*mxxxxx)/(volumePerOrderNow*volumeDouble);
            volumePerOrderNow=volumePerOrderNow*volumeDouble;
            ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
            orderNumber=ods.openBuyAction();
            if(orderNumber!=-1)orderOpeningNow=true;
            bool sel2=OrderSelect(orderNumber,SELECT_BY_TICKET);
            price=OrderOpenPrice();
            orderType="buy";
            orderTimesInCycle=orderTimesInCycle+1;
           }
         if(closePrice<price)
           {
            cycleFinished=true;
            profit=profit2;
            Alert("sell："+cycleFinished);
            orderOpeningNow=false;
           }

        }
      if(orderType=="buy")
        {
         if(OrderClosePrice()<price && orderTimesInCycle<maxOrderNumber && pofectTime && cycleFinished==false)
           {
            //profit=(stoploss*volumePerOrderNow*mxxxxx)/(volumePerOrderNow*volumeDouble);
            volumePerOrderNow=volumePerOrderNow*volumeDouble;
            ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
            orderNumber=ods.openSellAction();
            if(orderNumber!=-1)orderOpeningNow=true;
            bool sel2=OrderSelect(orderNumber,SELECT_BY_TICKET);
            price=OrderOpenPrice();
            orderType="sell";
            orderTimesInCycle=orderTimesInCycle+1;
              }else{
            if(OrderClosePrice()>price)
              {
               cycleFinished=true;
               profit=profit2;
               Alert("buy："+cycleFinished+OrderClosePrice()+":"+price);
               orderOpeningNow=false;
              }
           }
        }
     }
//首次开仓
   if(dateCurrent==dateSave)
     {
      //red2>0 green1>0 sell
      if(cycleFinished==false)
        {
         int total=OrdersTotal();
         //if(green1>0 && total==0 && pofectTime && orderTimesInCycle==0)
         if(green1>0 && total==0 && pofectTime && orderTimesInCycle<maxOrderNumber)
           {
            Alert("cycle finished:"+cycleFinished+" (orderTimesInCycle):"+orderTimesInCycle);
            ods.SetOrderDoSend(volumePerOrder,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
            orderNumber=ods.openSellAction();
            if(orderNumber!=-1)orderOpeningNow=true;
            price=Bid;
            orderType="sell";
            orderTimesInCycle=orderTimesInCycle+1;
            volumePerOrderNow=volumePerOrder;
           }

         if(red1>0 && total==0 && pofectTime && orderTimesInCycle<maxOrderNumber)
           {
            Alert("cycle finished:"+cycleFinished);
            ods.SetOrderDoSend(volumePerOrder,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
            orderNumber=ods.openBuyAction();
            if(orderNumber!=-1)orderOpeningNow=true;
            price=Ask;
            orderType="buy";
            orderTimesInCycle=orderTimesInCycle+1;
            volumePerOrderNow=volumePerOrder;
           }

        }
        }else{//日期不同开始新周期
      dateSave=dateCurrent;
      cycleFinished=false;
      orderTimesInCycle=0;
      pofectTime=false;
      //closeAllOrders();
      orderOpeningNow=false;
      //volumePerOrderNow=volumePerOrder;//非当日亏损是否double 不设置结果比较好
     }

//变色
   int orderCounter1=OrdersTotal();
   if(orderCounter1>0 && pofectTime)
     {
      bool colorChanged=false;
      if(red1>0 && green2>0 && bars_lock==false)
        {
         Alert("变色关闭检测1:green->red:"+cycleFinished);
         //if(orderType=="sell")sellStoplossToBuy();
         //if(orderType=="buy")buyStoplossToSell();
         //bars_lock=true;
         colorChanged=true;
        }
      if(red2>0 && green1>0 && bars_lock==false)
        {
         Alert("变色关闭检测1:red->green:"+cycleFinished);
         //if(orderType=="sell")sellStoplossToBuy();
         //if(orderType=="buy")buyStoplossToSell();
         //bars_lock=true;
         colorChanged=true;
        }
      if(colorChanged==true)
        {
         ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
         bool closeOK=ods.sellCloseAction(orderNumber);
         if(isTakingProfit())
           {
            Alert("盈利了");
            cycleFinished=true;
            profit=profit2;
              }else{
            //do buy or sell
            Alert("没有盈利"+orderTimesInCycle+":"+maxOrderNumber+":"+pofectTime+":"+cycleFinished);
            if(orderTimesInCycle<maxOrderNumber && pofectTime && cycleFinished==false)
              {
               Alert("come here success");
               //profit=(stoploss*volumePerOrderNow*mxxxxx)/(volumePerOrderNow*volumeDouble);
               volumePerOrderNow=volumePerOrderNow*volumeDouble;
               //volumePerOrderNow=(stoploss*volumePerOrderNow*mxxxxx)/profit;
               ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
               //检查是buy还是sell
               if(red1>0)
                 {
                  orderNumber=ods.openBuyAction();
                  if(orderNumber!=-1)orderOpeningNow=true;
                  bool sel2=OrderSelect(orderNumber,SELECT_BY_TICKET);
                  price=OrderOpenPrice();
                  orderType="buy";
                  orderTimesInCycle=orderTimesInCycle+1;
                  bars_lock=true;
                    }else{
                  orderNumber=ods.openSellAction();
                  if(orderNumber!=-1)orderOpeningNow=true;
                  bool sel2=OrderSelect(orderNumber,SELECT_BY_TICKET);
                  price=OrderOpenPrice();
                  orderType="sell";
                  orderTimesInCycle=orderTimesInCycle+1;
                  bars_lock=true;
                 }

              }
           }
        }
     }
   if(protect_close_ourSelf==100)
     {
      int orderCounter=OrdersTotal();
      if(orderCounter>0 && pofectTime)
        {
         if(orderType=="sell")
           {
            if((price-Bid)>profit*Point || (Bid-price)>stoploss*Point)
              {
               Alert("doing something");
               sellStoplossToBuy();
              }
           }
         if(orderType=="buy")
           {
            if((price-Ask)>stoploss*Point || (Ask-price)>profit*Point)
              {
               Alert("buy action SL/TP doing something");
               buyStoplossToSell();
              }
           }
        }
     }

  }
//+------------------------------------------------------------------+
void closeAllOrders()
  {
   int total=OrdersTotal();
   for(int i=total-1;i>=0;i--)
     {
      bool sel=OrderSelect(i,SELECT_BY_POS);
      int type=OrderType();

      bool result=false;

      if(type==OP_BUY)
        {
         //Close opened long positions
         result=OrderClose(OrderTicket(),OrderLots(),MarketInfo(OrderSymbol(),MODE_BID),5,Red);
           }else{

         result=OrderClose(OrderTicket(),OrderLots(),MarketInfo(OrderSymbol(),MODE_ASK),5,Red);

        }

      if(result==false)
        {
         //Alert("Order ",OrderTicket()," failed to close. Error:",GetLastError());
         Sleep(3000);
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool isTakingProfit()
  {
   bool sel=OrderSelect(orderNumber,SELECT_BY_TICKET);
   if(OrderClosePrice()<price && orderType=="sell")return true;
   if(orderType=="buy" && OrderClosePrice()>price)return true;
   return false;
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
void sellStoplossToBuy()
  {
   ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
   bool closeOK=ods.sellCloseAction(orderNumber);
   if(closeOK)
     {
      orderOpeningNow=false;
      bool sel=OrderSelect(orderNumber,SELECT_BY_TICKET);
      double closePrice=OrderClosePrice();
      Alert(closePrice+":"+price);
      if(closePrice>price && orderTimesInCycle<maxOrderNumber && pofectTime && cycleFinished==false)
        {
         Alert(orderTimesInCycle);
         //profit=(stoploss*volumePerOrderNow*mxxxxx)/(volumePerOrderNow*volumeDouble);
         //volumePerOrderNow=volumePerOrderNow*volumeDouble;
         volumePerOrderNow=(stoploss*volumePerOrderNow*mxxxxx)/profit;
         ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
         orderNumber=ods.openBuyAction();
         if(orderNumber!=-1)orderOpeningNow=true;
         bool sel2=OrderSelect(orderNumber,SELECT_BY_TICKET);
         price=OrderOpenPrice();
         orderType="buy";
         orderTimesInCycle=orderTimesInCycle+1;
        }
      if(closePrice<price)
        {
         cycleFinished=true;
         profit=profit2;
         Alert("sell："+cycleFinished);
        }

     }
  }
//+------------------------------------------------------------------+
void buyStoplossToSell()
  {
   ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
   bool closeOK=ods.buyCloseAction(orderNumber);
   if(closeOK)
     {
      orderOpeningNow=false;
      bool sel=OrderSelect(orderNumber,SELECT_BY_TICKET);
      if(OrderClosePrice()<price && orderTimesInCycle<maxOrderNumber && pofectTime && cycleFinished==false)
        {
         //profit=(stoploss*volumePerOrderNow*mxxxxx)/(volumePerOrderNow*volumeDouble);
         //volumePerOrderNow=volumePerOrderNow*volumeDouble;
         volumePerOrderNow=(stoploss*volumePerOrderNow*mxxxxx)/profit;
         ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
         orderNumber=ods.openSellAction();
         if(orderNumber!=-1)orderOpeningNow=true;
         bool sel2=OrderSelect(orderNumber,SELECT_BY_TICKET);
         price=OrderOpenPrice();
         orderType="sell";
         orderTimesInCycle=orderTimesInCycle+1;
           }else{
         if(OrderClosePrice()>price)
           {
            cycleFinished=true;
            profit=profit2;
            Alert("buy："+cycleFinished+OrderClosePrice()+":"+price);
           }
        }
     }
  }
//+------------------------------------------------------------------+

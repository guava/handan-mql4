//+------------------------------------------------------------------+
//|                                                          ROC.mq4 |
//|                                    Copyright ?2006, Robert Hill |
//+------------------------------------------------------------------+

#property  copyright "Copyright ?2006, Robert Hill"
//---- indicator settings
#property  indicator_separate_window
#property  indicator_buffers 1
#property  indicator_color1  Red
//---- indicator parameters
extern int RPeriod = 10;
extern bool UsePercent = false;
//---- indicator buffers
double RateOfChange[];
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
//---- drawing settings
   SetIndexStyle(0, DRAW_LINE);
   SetIndexDrawBegin(0, RPeriod);
   IndicatorDigits(Digits + 1);
//---- indicator buffers mapping
   if(!SetIndexBuffer(0, RateOfChange))
       Print("cannot set indicator buffers!");
//---- name for DataWindow and indicator subwindow label
   IndicatorShortName("ROC(" + RPeriod + ")");
//---- initialization done
   return(0);
  }
//+------------------------------------------------------------------+
//| Moving Averages Convergence/Divergence                           |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
   int limit;
   double ROC, CurrentClose, PrevClose;
   
   if(rates_total<=8)
      return(0);
   limit=rates_total-prev_calculated;
   if(prev_calculated>0)
      limit++;
      
   for(int i = 0; i < limit; i++)
     {
       CurrentClose = iClose(NULL, 0, i);
       PrevClose = iClose(NULL, 0, i + RPeriod);
       ROC = CurrentClose - PrevClose;
       //----
       if(UsePercent)
         {
           if(PrevClose != 0)
               RateOfChange[i] = 100 * ROC / PrevClose;
         }
       else
           RateOfChange[i] = ROC;
     }   
//---- done
   return(0);
  }
//+------------------------------------------------------------------+
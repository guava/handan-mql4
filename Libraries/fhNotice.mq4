//+------------------------------------------------------------------+
//|                                                  DataCreater.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#include "../Libraries/OrderState.mq4"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class Notice
  {
protected:
   int               period_guava;
   double            o[4];
   double            c[4];
   double            H[4];
   double            L[4];
   double            gmacd;
   double            gmacd1;
   double            gmacd2;
   OrderState       *orderState;
public:

   void DataCreater()
     {
      period_guava=0;
      //period_guava=PERIOD_M30;
     }

   int getNoticeInfo(OrderState *orderstate)
     {
      this.orderState=orderstate;
      int notice_info=0;

      //买入信号
      //if(c[2]<H[2] && c[1]>H[1] && gmacd>0)notice_info=21; //上穿H
      double c2=iCustom(NULL,0,"test",3,1,2);
      double c1=iCustom(NULL,0,"test",3,1,1);
      double o2=iCustom(NULL,0,"test",3,0,2);
      double o1=iCustom(NULL,0,"test",3,0,1);
      
      //double bias1=iCustom(NULL,0,"biasabs",3,0);
      
      double bias1=(MathAbs(100*(Close[0]- iMA(NULL,0,6,0,MODE_EMA,PRICE_CLOSE,0))/iMA(NULL,0,6,0,MODE_EMA,PRICE_CLOSE,0))
      +MathAbs(100*(Close[1]- iMA(NULL,0,6,0,MODE_EMA,PRICE_CLOSE,1))/iMA(NULL,0,6,0,MODE_EMA,PRICE_CLOSE,1))
      +MathAbs(100*(Close[2]- iMA(NULL,0,6,0,MODE_EMA,PRICE_CLOSE,2))/iMA(NULL,0,6,0,MODE_EMA,PRICE_CLOSE,2)) )/3.0;
      
      if(c1>o1 && c2<o2 && c1>c2 && gmacd>gmacd1 && gmacd1>gmacd2 && bias1>0.031)
        {
         notice_info=21; //上穿H
         //Print("gg",bias1);
        }
      
      if(c1<o1 && c2>o2 && c1<c2 && gmacd<gmacd1 && gmacd1<gmacd2 && bias1>0.031)
        {
         notice_info=-21; //上穿H
         //Print("gg",bias1);
        }
        
      //if(c1>o1 && c2>o2 && c1>c2 && gmacd>gmacd1 && gmacd1>gmacd2 && bias1>0.06)notice_info=21; //上穿H
      //if(c[2]>o[2] && c[1]<o[1] &&  orderState.getOrderType()==21)notice_info=22;


      //if(c[2]>H[2] && c[1]>H[1] && gmacd>0)notice_info=31; //H上 平行

      return notice_info;

     }
   void initPerTick()
     {

      o[0]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_OPEN,0);
      o[1]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_OPEN,1);
      o[2]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_OPEN,2);
      o[3]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_OPEN,3);
      c[0]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_CLOSE,0);
      c[1]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_CLOSE,1);
      c[2]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_CLOSE,2);
      c[3]=iMA(NULL,period_guava,3,0,MODE_EMA,PRICE_CLOSE,3);

      H[0]=iMA(NULL,period_guava,8,0,MODE_EMA,PRICE_HIGH,0);
      H[1]=iMA(NULL,period_guava,8,0,MODE_EMA,PRICE_HIGH,1);
      H[2]=iMA(NULL,period_guava,8,0,MODE_EMA,PRICE_HIGH,2);
      L[0]=iMA(NULL,period_guava,8,0,MODE_EMA,PRICE_LOW,0);
      L[1]=iMA(NULL,period_guava,8,0,MODE_EMA,PRICE_LOW,1);
      L[2]=iMA(NULL,period_guava,8,0,MODE_EMA,PRICE_LOW,2);
      gmacd=iMACD(NULL,0,12,26,9,PRICE_CLOSE,MODE_MAIN,0);
      gmacd1=iMACD(NULL,0,12,26,9,PRICE_CLOSE,MODE_MAIN,1);
      gmacd2=iMACD(NULL,0,12,26,9,PRICE_CLOSE,MODE_MAIN,2);

     }
  };
//+------------------------------------------------------------------+

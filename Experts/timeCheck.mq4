//+------------------------------------------------------------------+
//|                                                    timeCheck.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
int iiiiii=0;
void OnTick()
  {
      int h=TimeHour(TimeCurrent());
      int d=TimeDayOfWeek(TimeCurrent());
      if(h==0)Alert("小时0:"+TimeCurrent());
      if(d==1)Alert(d+":周一:"+TimeCurrent());
     if(d==1 && h==0){
         iiiiii=iiiiii+1;
         Alert(d+""+h);
     }
     
     
     if(DayOfWeek()==2 && iiiiii>0){
        Alert("0点有交易");
        iiiiii=0;
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//--- v6
//|正确增倍
//|前天波动参照可设置
//--- v7
//--- 精简重复代码
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#include "../Libraries/fhtz2OrderDoSend.mq4"
#include "../Libraries/fhLocker.mq4"
#include "../Libraries/temple.mq4"
#include "../Libraries/fhTimeManager.mq4"

#include "../Libraries/core/fh_system_bus.mq4"
#include "../Libraries/sarindicator/fhtz_Sar_indicator.mq4"
#include "../Libraries/sarindicator/fhtz_yesterday.mq4"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
input double volumePerOrder=0.1;//手数
double volumePerOrderNow;
input int maxSlippage=5;//最大滑点
input double mxxxxx=0.8;//反手获利倍数 (1.5)
input double volumeDouble=3;//订单量增倍

int maxOrderNumber=5;//最大单数(4)
int hourOfStartTime=9;//开始时间
input int hourOfEndTime=19;//结束时间

input bool closeOrderEveryday=true;//是否每日关闭订单

bool cycleFinished=false; //当天完成了购买周期
int orderTimesInCycle=0;
string dateSave;

string orderType;

int stoploss=1300;
double profit=400;
OrderDoSend *ods;
//bool pofectTime=false;

bool  orderOpeningNow=false;
int orderNumber;
double price;

double profit2;
//tomorrow higher-lower
double higher=0;
double   lower=100000;
int higher_low_point=1500000;//19-次日9点波动 默认1500，不采用该策略设置为1500000
double higher_lower_lock=false;
double higher_lower_too_big=false;

//变色操作相关
//bool bars_lock=false;
//int bars_now=0;
Locker *locker;
int protect_close_ourSelf=100;//变色平仓设23456  不变色平仓设100 

                                //bus
//Sar_bus *sarbus;
BUS_System *bus_system;

TimeManager *time_manager;
YesterdaySituation *yesterday_situation;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
//bbbbb(new CCat);
   bus_system=new Sar_bus();

   dateSave=TimeToStr(TimeCurrent(),TIME_DATE);
   ods=new OrderDoSend();
   profit2=profit;
   locker=new Locker();

   time_manager=new TimeManager(hourOfStartTime,hourOfEndTime,closeOrderEveryday);
   
   yesterday_situation=new YesterdaySituation(time_manager,hourOfStartTime,higher_low_point);
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
/*
void bbbbb(CAnimal *animal)
  {
   animal.Sound();
  }
  */
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//locker.barsChanged();
//新柱要做的操作
   bus_system.barsReflash(locker);

//交易时间 自动平仓
   bus_system.timeManager(time_manager);
//对昨天数据处理
   yesterday_situation.everTickDo();
   
   double green1=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",0,1);
   double red1=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",1,1);
   double green2=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",0,2);
   double red2=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",1,2);
   //double gmacd=iMACD(NULL,0,12,26,9,PRICE_CLOSE,MODE_MAIN,0);
   double gmacd=iMA(NULL,0,100,0,MODE_EMA,PRICE_LOW,0);
//防止超大滑点
//非主动平仓处理
   bool sel=OrderSelect(orderNumber,SELECT_BY_TICKET);
   bool close_price=OrderClosePrice();

   if(OrdersTotal()==0 && orderOpeningNow==true)
     {
      //Alert("偷袭:"+close_price);
      if(orderType=="sell")
        {
         double closePrice=OrderClosePrice();

         if(closePrice>price && orderTimesInCycle<maxOrderNumber && time_manager.getPofectTime() && cycleFinished==false && gmacd<Ask)
           {

            volumeSet();
            orderNumber=doBuy();
           }
         if(closePrice<price)
           {
            afterTakeProfit();
           }

        }
      if(orderType=="buy")
        {
         if(OrderClosePrice()<price && orderTimesInCycle<maxOrderNumber && time_manager.getPofectTime() && cycleFinished==false && gmacd>Bid)
           {
            volumeSet();
            orderNumber=doSell();
              }else{
            if(OrderClosePrice()>price)
              {
               afterTakeProfit();
              }
           }
        }
     }
//首次开仓
   if(time_manager.get_dateCurrent()==dateSave)
     {
      //red2>0 green1>0 sell
      if(cycleFinished==false)
        {
         int total=OrdersTotal();
         //if(green1>0 && total==0 && pofectTime && orderTimesInCycle==0)
         if(green1>0 && total==0 && time_manager.getPofectTime() && orderTimesInCycle<maxOrderNumber && gmacd>Bid)
           {
            Alert("cycle finished:"+cycleFinished+" (orderTimesInCycle):"+orderTimesInCycle);
            ods.SetOrderDoSend(volumePerOrder,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
            orderNumber=doSell();
            volumePerOrderNow=volumePerOrder;
           }

         if(red1>0 && total==0 && time_manager.getPofectTime() && orderTimesInCycle<maxOrderNumber && Ask>gmacd)
           {
            Alert("cycle finished:"+cycleFinished);
            ods.SetOrderDoSend(volumePerOrder,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
            orderNumber=doBuy();
            volumePerOrderNow=volumePerOrder;
           }

        }
        }else{
      //开始第二天
      dateSave=time_manager.get_dateCurrent();
      cycleFinished=false;
      orderTimesInCycle=0;
      time_manager.setPorfectime(false);
      orderOpeningNow=false;
      //volumePerOrderNow=volumePerOrder;//非当日亏损是否double 不设置结果比较好
     }

//变色
   int orderCounter1=OrdersTotal();
   if(orderCounter1>0 && time_manager.getPofectTime())
     {
      bool colorChanged=false;
      if(red1>0 && green2>0 && locker.get_lock()==false)
        {
         Alert("变色关闭检测1:green->red:"+cycleFinished);
         colorChanged=true;
        }
      if(red2>0 && green1>0 && locker.get_lock()==false)
        {
         Alert("变色关闭检测1:red->green:"+cycleFinished);
         colorChanged=true;
        }
      if(colorChanged==true)
        {
         //ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
         if(orderType=="buy")bool buyCloseOK=ods.buyCloseAction(orderNumber);
         if(orderType=="sell")bool closeOK=ods.sellCloseAction(orderNumber);
         if(isTakingProfit())
           {
            afterTakeProfit();
              }else{
            //do buy or sell
            Alert("没有盈利"+orderTimesInCycle+":"+maxOrderNumber+":"+time_manager.getPofectTime()+":"+cycleFinished);
            if(orderTimesInCycle<maxOrderNumber && time_manager.getPofectTime() && cycleFinished==false)
              {
               Alert("come here success");

               
               //检查是buy还是sell
               if(red1>0 && Ask>gmacd)
                 {
                 volumeSet();
                  orderNumber=doBuy();
                  locker.set_lock();
                    }
                if(green1>0 && gmacd>Bid){
                volumeSet();
                  orderNumber=doSell();
                  locker.set_lock();
                 }

              }
           }
        }
     }
   if(protect_close_ourSelf==100)
     {
      int orderCounter=OrdersTotal();
      if(orderCounter>0 && time_manager.getPofectTime())
        {
         if(orderType=="sell")
           {
            if((price-Bid)>profit*Point || (Bid-price)>stoploss*Point)
              {
               Alert("doing something");
               sellStoplossToBuy();
              }
           }
         if(orderType=="buy")
           {
            if((price-Ask)>stoploss*Point || (Ask-price)>profit*Point)
              {
               Alert("buy action SL/TP doing something");
               buyStoplossToSell();
              }
           }
        }
     }

  }
//+------------------------------------------------------------------+

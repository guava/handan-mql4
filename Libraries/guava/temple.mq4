//+------------------------------------------------------------------+
//|                                                       temple.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| My function                                                      |
//+------------------------------------------------------------------+
// int MyCalculator(int value,int value2) export
//   {
//    return(value+value2);
//   }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
void closeAllOrders()
  {
   int total=OrdersTotal();
   for(int i=total-1;i>=0;i--)
     {
      bool sel=OrderSelect(i,SELECT_BY_POS);
      int type=OrderType();

      bool result=false;

      if(type==OP_BUY)
        {
         result=OrderClose(OrderTicket(),OrderLots(),MarketInfo(OrderSymbol(),MODE_BID),5,Red);
           }else{
         result=OrderClose(OrderTicket(),OrderLots(),MarketInfo(OrderSymbol(),MODE_ASK),5,Red);
        }

      if(result==false)
        {
         Alert("Order ",OrderTicket()," failed to close. Error:",GetLastError());
         //Sleep(3000);
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool isTakingProfit()
  {
   bool sel=OrderSelect(orderNumber,SELECT_BY_TICKET);
   if(OrderClosePrice()<price && orderType=="sell")return true;
   if(orderType=="buy" && OrderClosePrice()>price)return true;
   return false;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void afterTakeProfit()
  {
   cycleFinished=true;
   profit=profit2;
   orderOpeningNow=false;
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
void sellStoplossToBuy()
  {
   ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
   bool closeOK=ods.sellCloseAction(orderNumber);
   if(closeOK)
     {
      orderOpeningNow=false;
      bool sel=OrderSelect(orderNumber,SELECT_BY_TICKET);
      double closePrice=OrderClosePrice();
      Alert(closePrice+":"+price);
      if(closePrice>price && orderTimesInCycle<maxOrderNumber && time_manager.getPofectTime() && cycleFinished==false)
        {
         Alert(orderTimesInCycle);
         //profit=(stoploss*volumePerOrderNow*mxxxxx)/(volumePerOrderNow*volumeDouble);
         //volumePerOrderNow=volumePerOrderNow*volumeDouble;
         volumePerOrderNow=(stoploss*volumePerOrderNow*mxxxxx)/profit;
         ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
         orderNumber=ods.openBuyAction();
         if(orderNumber!=-1)orderOpeningNow=true;
         bool sel2=OrderSelect(orderNumber,SELECT_BY_TICKET);
         price=OrderOpenPrice();
         orderType="buy";
         orderTimesInCycle=orderTimesInCycle+1;
        }
      if(closePrice<price)
        {
         cycleFinished=true;
         profit=profit2;
         Alert("sell："+cycleFinished);
        }

     }
  }
//+------------------------------------------------------------------+
void buyStoplossToSell()
  {
   ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
   bool closeOK=ods.buyCloseAction(orderNumber);
   if(closeOK)
     {
      orderOpeningNow=false;
      bool sel=OrderSelect(orderNumber,SELECT_BY_TICKET);
      if(OrderClosePrice()<price && orderTimesInCycle<maxOrderNumber && time_manager.getPofectTime() && cycleFinished==false)
        {
         //profit=(stoploss*volumePerOrderNow*mxxxxx)/(volumePerOrderNow*volumeDouble);
         //volumePerOrderNow=volumePerOrderNow*volumeDouble;

         volumePerOrderNow=(stoploss*volumePerOrderNow*mxxxxx)/profit;
         ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);

         orderNumber=ods.openSellAction();
         if(orderNumber!=-1)orderOpeningNow=true;
         bool sel2=OrderSelect(orderNumber,SELECT_BY_TICKET);
         price=OrderOpenPrice();
         orderType="sell";
         orderTimesInCycle=orderTimesInCycle+1;
           }else{
         if(OrderClosePrice()>price)
           {
            cycleFinished=true;
            profit=profit2;
            Alert("buy："+cycleFinished+OrderClosePrice()+":"+price);
           }
        }
     }
  }
//+------------------------------------------------------------------+
void volumeSet()
  {
   volumePerOrderNow=volumePerOrderNow*volumeDouble;
   ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+protect_close_ourSelf,profit+protect_close_ourSelf);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int doBuy()
  {
   int orderNumber=ods.openBuyAction();
   if(orderNumber!=-1)orderOpeningNow=true;
   bool sel2=OrderSelect(orderNumber,SELECT_BY_TICKET);
   price=OrderOpenPrice();
   orderType="buy";
   orderTimesInCycle=orderTimesInCycle+1;
   return orderNumber;
  }
//+------------------------------------------------------------------+
int doSell()
  {
   int orderNumber=ods.openSellAction();
   if(orderNumber!=-1)orderOpeningNow=true;
   bool sel2=OrderSelect(orderNumber,SELECT_BY_TICKET);
   price=OrderOpenPrice();
   orderType="sell";
   orderTimesInCycle=orderTimesInCycle+1;
   return orderNumber;
  }
//+------------------------------------------------------------------+
void timesChecker()
  {
   string dateCurrent=TimeToStr(TimeCurrent(),TIME_DATE);
   int nowhour=TimeHour(TimeCurrent());
   
   
   //交易时间
   if(nowhour>(hourOfStartTime-1) && nowhour<hourOfEndTime)
     {
      //pofectTime=true;
        }else{
      //pofectTime=false;
     }
   int totalOrderNumbers=OrdersTotal();
//周五11：30关闭所有订单
   if(4<DayOfWeek()<6 && Hour()==hourOfEndTime && Minute()>30)
     {
      closeAllOrders();
     }

//当日关闭所有订单
   if(0<DayOfWeek()<5 && Hour()==hourOfEndTime && closeOrderEveryday==true)
     {
      closeAllOrders();
     }


//最高最低价
   if(nowhour>(hourOfEndTime-1) || nowhour<hourOfStartTime)
     {
      if(Ask>higher)higher=Ask;
      if(Bid<lower)lower=Bid;
      if(higher_lower_lock==true)
        {
         higher_lower_lock=false;
         higher_lower_too_big=false;
        }
     }
    if(nowhour==hourOfStartTime && higher_lower_lock==false)
     {

      if((higher-lower)>higher_low_point*Point)
        {
         Alert(higher+":"+lower);
         higher_lower_too_big=true;
        }
      higher=0;
      lower=10000;
      higher_lower_lock=true;
     }
     
   
  }
//+------------------------------------------------------------------+

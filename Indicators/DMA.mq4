//+------------------------------------------------------------------+
//|                                                          DMA.mq4 |
//|                                      Copyright ?2010, LenIFCHIK |
//|软滂赅蝾?DMA (Displaced Moving Averages)铗钺疣驵弪 ?铖眍忭铎    |
//|铌礤 3 耜铍?耩邃龛?                                        |
//| - 3-镥痂钿磬 镳铖蜞 耜铍 耩邃? 铗 鲥?玎牮?,        |
//|   耢妁屙磬 怙屦邃 磬 蝠?镥痂钿?(怩钿眍?镟疣戾蝠 - SR_DSMA);  |
//| - 7-镥痂钿磬 镳铖蜞 耜铍 耩邃? 铗 鲥?玎牮?,        |
//|   耢妁屙磬 怙屦邃 磬 ?螯 镥痂钿钼 (怩钿眍?镟疣戾蝠 - MR_DSMA);|
//| -	25-镥痂钿磬 镳铖蜞 耜铍 耩邃? 铗 鲥?玎牮?,       |
//|   耢妁屙磬 怙屦邃 磬 ?螯 镥痂钿钼(怩钿眍?镟疣戾蝠 - LR_DSMA). |
//+------------------------------------------------------------------+
#property copyright "LenIFCHIK"

//#property indicator_chart_window                          // ?铖眍忭铎 铌礤     
#property indicator_separate_window                            
#property indicator_buffers 3                             // 觐腓麇耱忸 狍翦痤?
#property  indicator_color1  Red                          // 鲡弪 SR DSMA
#property  indicator_color2  Blue                         // 鲡弪 MR DSMA
#property  indicator_color3  Lime                         // 鲡弪 LR DSMA
//---- 忭屮龛?镟疣戾蝠?桧滂赅蝾疣
//---- 潆 SR DSMA
extern int SR_DSMA_Period=3;                              // 镥痂钿 SR DSMA
extern int SR_DSMA_Shift=3;                               // 皲忤?SR DSMA
//---- 潆 MR DSMA
extern int MR_DSMA_Period=7;                              // 镥痂钿 MR DSMA
extern int MR_DSMA_Shift=5;                               // 皲忤?MR DSMA       
//---- 潆 LR DSMA
extern int LR_DSMA_Period=25;                             // 镥痂钿 LR DSMA
extern int LR_DSMA_Shift=5;                               // 皲忤?LR DSMA
//---- 镟疣戾蝠?铗钺疣驽龛 腓龛?(1-on,2-off)
extern string Display_mode="0 - on, 1 - off";
extern int SR_DSMA_disp=1;                                // 疱骅?铗钺疣驽龛 SR DSMA 
extern int MR_DSMA_disp=1;                                // 疱骅?铗钺疣驽龛 MR DSMA 
extern int LR_DSMA_disp=1;                                // 疱骅?铗钺疣驽龛 LR DSMA 
//---- 狍翦瘥 桧滂赅蝾疣
double     SR_DSMA_Buffer[];                              // 狍翦?SR DSMA
double     MR_DSMA_Buffer[];                              // 狍翦?MR DSMA
double     LR_DSMA_Buffer[];                              // 狍翦?LR DSMA
//----
int ExtCountedBars=0;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
//---- 疱骅?铗钺疣驽龛
//---- 疱骅?铗钺疣驽龛 SR DSMA   
   switch(SR_DSMA_disp)                                   
     {
      case 0 :   
        SetIndexStyle(0,DRAW_NONE);break;                 // 团 我瘟欣评乓堰
      case 1 :   
        SetIndexStyle(0,DRAW_LINE,STYLE_SOLID,1);break;   // 我瘟欣评乓堰
      default :
        SR_DSMA_disp=1;                                   // 镱 箪铍鬣龛?我瘟欣评乓堰
     }   
//---- 疱骅?铗钺疣驽龛 MR DSMA 
   switch(MR_DSMA_disp)                                   
     {
      case 0 :   
        SetIndexStyle(1,DRAW_NONE);break;                 // 团 我瘟欣评乓堰
      case 1 :   
        SetIndexStyle(1,DRAW_LINE,STYLE_SOLID,1);break;   // 我瘟欣评乓堰
      default :
        MR_DSMA_disp=1;                                   // 镱 箪铍鬣龛?我瘟欣评乓堰
     }   
//---- 疱骅?铗钺疣驽龛 LR DSMA 
   switch(LR_DSMA_disp)                                   
     {
      case 0 :   
        SetIndexStyle(2,DRAW_NONE);break;                 // 团 我瘟欣评乓堰
      case 1 :   
        SetIndexStyle(2,DRAW_LINE,STYLE_SOLID,1);break;   // 我瘟欣评乓堰
      default :
        LR_DSMA_disp=1;                                   // 镱 箪铍鬣龛?我瘟欣评乓堰
     }   
//---- 镟疣戾蝠?耢妁屙?
   SetIndexShift(0,SR_DSMA_Shift);                        // 耢妁屙? 腓龛?SR DSMA 铗眍耔蝈朦眍 磬鬣豚 沭圄桕?
   SetIndexShift(1,MR_DSMA_Shift);                        // 耢妁屙? 腓龛?MR DSMA 铗眍耔蝈朦眍 磬鬣豚 沭圄桕?
   SetIndexShift(2,LR_DSMA_Shift);                        // 耢妁屙? 腓龛?LR DSMA 铗眍耔蝈朦眍 磬鬣豚 沭圄桕?
   IndicatorDigits(MarketInfo(Symbol(),MODE_DIGITS));     // 篑蜞眍怅?纛痨囹?蝾黜铖蜩 潆 忤珞嚯桤圉梃 珥圜屙栝 桧滂赅蝾疣
//---- 桁屙?腓龛? 潆 铗钺疣驽龛 桧纛痨圉梃 ?铌礤 DataWindow ?怦镫帼?镱漶赅珀?
   SetIndexLabel(0,"SR DSMA");                            
   SetIndexLabel(1,"MR DSMA");                            
   SetIndexLabel(2,"LR DSMA");                            
//---- 篑蜞眍怅?镱?潢钼钽?眍戾疣 徉疣 铗 磬鬣豚 溧眄, ?觐蝾痤泐 漕腈磬 磬麒磬螯? 铗痂耦怅? 
   SetIndexDrawBegin(0,SR_DSMA_Period-1);                 
   SetIndexDrawBegin(1,MR_DSMA_Period-1);
   SetIndexDrawBegin(2,LR_DSMA_Period-1);   
//---- 疋琰 镥疱戾眄铋, 钺?怆屙眍?磬 汶钺嚯铎 箴钼礤, ?镳邃铒疱溴脲眄 狍翦痤?桧滂赅蝾疣
   SetIndexBuffer(0,SR_DSMA_Buffer);
   SetIndexBuffer(1,MR_DSMA_Buffer);
   SetIndexBuffer(2,LR_DSMA_Buffer);   
//---- 觐礤?桧桷栲腓玎鲨?
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start()
  {
   if(Bars<=LR_DSMA_Period) return(0);
   ExtCountedBars=IndicatorCounted();
//---- 
   if (ExtCountedBars<0) return(-1);
//---- 
   if (ExtCountedBars>0) ExtCountedBars--;
//---- 疣聍蛤 牮囹觐耩铟眍?耢妁疙眍?镳铖蝾?耜铍?耩邃礤?
   double sum=0;
   int    i,pos=Bars-ExtCountedBars-1;
   if(pos<SR_DSMA_Period) pos=SR_DSMA_Period;
   for(i=1;i<SR_DSMA_Period;i++,pos--)
      sum+=Close[pos];
   while(pos>=0)
     {
      sum+=Close[pos];
      SR_DSMA_Buffer[pos]=sum/SR_DSMA_Period;
	   sum-=Close[pos+SR_DSMA_Period-1];
 	   pos--;
     }
   if(ExtCountedBars<1)
      for(i=1;i<SR_DSMA_Period;i++) SR_DSMA_Buffer[Bars-i]=0;
//---- 疣聍蛤 耩邃礤耩铟眍?耢妁疙眍?镳铖蝾?耜铍?耩邃礤?
   double sum1=0;
   int    pos1=Bars-ExtCountedBars-1;
   if(pos1<MR_DSMA_Period) pos1=MR_DSMA_Period;
   for(i=1;i<MR_DSMA_Period;i++,pos1--)
      sum1+=Close[pos1];
   while(pos1>=0)
     {
      sum1+=Close[pos1];
      MR_DSMA_Buffer[pos1]=sum1/MR_DSMA_Period;
	   sum1-=Close[pos1+MR_DSMA_Period-1];
 	   pos1--;
     }
   if(ExtCountedBars<1)
      for(i=1;i<MR_DSMA_Period;i++) MR_DSMA_Buffer[Bars-i]=0; 
//---- 疣聍蛤 漕脬铖痤黜铋 耢妁疙眍?镳铖蝾?耜铍?耩邃礤?
   double sum2=0;
   int    pos2=Bars-ExtCountedBars-1;
   if(pos2<LR_DSMA_Period) pos2=LR_DSMA_Period;
   for(i=1;i<LR_DSMA_Period;i++,pos2--)
      sum2+=Close[pos2];
   while(pos2>=0)
     {
      sum2+=Close[pos2];
      LR_DSMA_Buffer[pos2]=sum2/LR_DSMA_Period;
	   sum2-=Close[pos2+LR_DSMA_Period-1];
 	   pos2--;
     }
   if(ExtCountedBars<1)
      for(i=1;i<LR_DSMA_Period;i++) LR_DSMA_Buffer[Bars-i]=0; 
  }

//+------------------------------------------------------------------+
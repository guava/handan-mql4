//+------------------------------------------------------------------+
//|                                                      Actions.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#include "../Libraries/OrderState.mq4"
#include "../Libraries/OrderDoSend.mq4"
#include "../Libraries/Locker.mq4"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class Actions
  {
protected:
   OrderDoSend      *orderDoSend;
   Locker           *locker;
   double volumePerOrder;
   int maxSlippage;
   int points_bad;
   int points_good;
public:
   void Actions(Locker *locker1,double volumePerOrder_,int maxSlippage_,int points_bad_,int points_good_)
     {
      locker=locker1;
      volumePerOrder=volumePerOrder_;
      maxSlippage=maxSlippage_;
      points_bad=points_bad_;
      points_good=points_good_;
      orderDoSend=new OrderDoSend();
     }
   int chooseAction(int notice_info,OrderState *os)
     {
         
         orderDoSend.SetOrderDoSend(os,volumePerOrder,maxSlippage,points_bad,points_good);
         //orderDoSend.SetOrderDoSend(os);
         if(notice_info>0 && (notice_info%2)==1)
         {
         orderDoSend.openBuyAction(notice_info);
         locker.lock("bars");
         }
         if(notice_info>0 && (notice_info%2)==0){
         orderDoSend.buyCloseAction(os.orderNumber);
         locker.unlock("bars");
         }

         if(notice_info<0 && (notice_info%2)==-1)
         {
         //Alert("-21");
         orderDoSend.openSellAction(notice_info);
         locker.lock("bars");
         }
         if(notice_info<0 && (notice_info%2)==0)
         {
         orderDoSend.sellCloseAction(os.orderNumber);
         locker.unlock("bars");
        }
      return 0;
     }
   //自动关闭
   void closeAuto(OrderState *os)
     {
      if(os.getOrderType()>0)
        {

         if((Ask>(os.price+780*Point)) || (Ask<(os.price-180*Point)))
           {
            orderDoSend.buyCloseAction(os.orderNumber);
           }
        }

      if(os.getOrderType()<0)
        {

         if((Bid<(os.price-780*Point)) || (Bid>(os.price+180*Point)))
           {
            orderDoSend.sellCloseAction(os.orderNumber);
           }
        }
     }
  };
//+------------------------------------------------------------------+

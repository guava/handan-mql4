//+------------------------------------------------------------------+
//|                                                    AutoClose.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include "../Libraries/ggArray.mq4"
//+------------------------------------------------------------------+
//| My function                                                      |
//+------------------------------------------------------------------+
// int MyCalculator(int value,int value2) export
//   {
//    return(value+value2);
//   }
//+------------------------------------------------------------------+
class AutoClose
  {

protected:
   int               orderSum;
   bool              closeOnEveryday; //每天关闭所有定单
   datetime          tBegin_Data;//每天关闭所有订单时间
   bool              closeOnFriday;   //周五23:30关闭所有定单
   int               orderTicket;
   double            stopLost;
   double            profit;
   int               ticketList[];
   double            stopLossList[];
   double            orderTakeProfitList[];
   string            jianyi;
   void closeAll()
     {
      int total=OrdersTotal();
      for(int i=total-1;i>=0;i--)
        {
         bool sel=OrderSelect(i,SELECT_BY_POS);
         int type=OrderType();

         bool result=false;

         if(type==OP_BUY)
           {
            //Close opened long positions
            result=OrderClose(OrderTicket(),OrderLots(),MarketInfo(OrderSymbol(),MODE_BID),5,Red);
              }else{

            result=OrderClose(OrderTicket(),OrderLots(),MarketInfo(OrderSymbol(),MODE_ASK),5,Red);

           }

         if(result==false)
           {
            //Alert("Order ",OrderTicket()," failed to close. Error:",GetLastError());
            Sleep(3000);
           }
        }
     }

public:
   void AutoClose(bool closeOnEveryday_,datetime tBegin_Data_,bool closeOnFriday_,string jianyi_)
     {
      orderSum=OrdersTotal();
      this.closeOnEveryday=closeOnEveryday_;
      this.tBegin_Data=tBegin_Data_;
      this.closeOnFriday=closeOnFriday_;
      this.jianyi=jianyi_;
     }
   void initOrder()
     {
      int totalOrder=OrdersTotal();
      ArrayResize(ticketList,totalOrder);
      ArrayResize(stopLossList,totalOrder);
      ArrayResize(orderTakeProfitList,totalOrder);
      for(int i=0;i<totalOrder;i++)
        {
         bool sel=OrderSelect(i,SELECT_BY_POS);
         if(sel==true)
           {
            ticketList[i]=OrderTicket();
            stopLossList[i]=OrderStopLoss();
            orderTakeProfitList[i]=OrderTakeProfit();
           }
        }
        int sizea=ArraySize(this.ticketList);
        //Alert(sizea);
     }
   void getNewOrder()
     {
      int totalOrder=OrdersTotal();
      //Alert(totalOrder+"new---old"+this.orderSum);
      if(totalOrder>this.orderSum && totalOrder>0)
        {
         //Alert("订单增加");
         //Alert(totalOrder+"---"+this.orderSum);
         bool sel=OrderSelect(totalOrder-1,SELECT_BY_POS);
         //if(OrderTakeProfit()!=0 && OrderStopLoss()!=0)
         //{
         //this.orderSum=totalOrder;

         string direction;
         if(OrderType()==0)
           {
            direction="买";
           }
         else
           {
            direction="卖";
           }

         //MT4 SendMail function
         bool res=SendMail("OrderSent浩投资咨询有限公司","订单号 #"+string(OrderTicket())+" Opened Succesfully\n"
                           +"货币兑: "+OrderSymbol()+"\n"
                           +"操作:  "+direction+"\n"
                           +"开仓时间: "+string(OrderOpenTime())+"(北京时间："+TimeLocal()+")\n"
                           +"开仓价格: "+str_replace("000","",DoubleToString(OrderOpenPrice()))+"\n"
                           +"操作手数："+string(OrderLots())+"(账号本金：10000美元)\n"
                           +"止损位: "+string(OrderStopLoss())+"\n"
                           +"止盈位: "+string(OrderTakeProfit())+"\n"
                           +jianyi+"\n"
                           );

         if(res==false)
           {
            Print("Error sending email");
              }else{
            
            this.orderSum=totalOrder;
            int sizea=ArraySize(this.ticketList);
            ArrayResize(ticketList,sizea+1);
            ArrayResize(stopLossList,sizea+1);
            ArrayResize(orderTakeProfitList,sizea+1);
            ticketList[sizea]=OrderTicket();
            stopLossList[sizea]=OrderStopLoss();
            orderTakeProfitList[sizea]=OrderTakeProfit();
            //Alert("增加订单finished");
           }

        }

      //订单量不变 订单修改
      if(totalOrder==this.orderSum && totalOrder>0)
        {
         Print("订单修改");
         int sizea=ArraySize(this.ticketList);
         for(int i=0;i<totalOrder;i++)
           {
            bool sel=OrderSelect(i,SELECT_BY_POS);
            if(sel==true)
              {
               //ticketList[i]=OrderTicket();
               //stopLossList[i]=OrderStopLoss();
               //orderTakeProfitList[i]=OrderTakeProfit();
               for(int ii=0;ii<sizea;ii++)
                 {
                  if(ticketList[ii]==OrderTicket())
                    {
                     if(this.orderTakeProfitList[ii]!=OrderTakeProfit() || this.stopLossList[ii]!=OrderStopLoss())
                       {
                        bool res=SendMail("OrderSent浩投资咨询有限公司","订单号 #"+string(OrderTicket())+" Opened Succesfully\n"
                                          +"货币兑: "+OrderSymbol()+"\n"
                                          +"操作:  修改止盈止损\n"
                                          +"开仓时间: "+string(OrderOpenTime())+"(北京时间："+TimeLocal()+")\n"
                                          +"开仓价格: "+str_replace("000","",DoubleToString(OrderOpenPrice()))+"\n"
                                          +"操作手数："+string(OrderLots())+"(账号本金：10000美元)\n"
                                          +"止损位: "+string(OrderStopLoss())+"\n"
                                          +"止盈位: "+string(OrderTakeProfit())+"\n"
                                          +jianyi+"\n"
                                          );

                        if(res==false)
                          {
                           Print("Error sending email");
                             }else{
                           
                           this.orderTakeProfitList[ii]=OrderTakeProfit();
                           this.stopLossList[ii]=OrderStopLoss();
                          }
                       }
                    }
                 }
              }
           }

        }

      //订单量变小 订单修改
      if(totalOrder<this.orderSum && totalOrder>=0)
        {
         //Alert("订单减少");
         int sizea=ArraySize(this.ticketList);
         //Alert(sizea);
         for(int i=0;i<sizea;i++)
           {
            int ticketNum=this.ticketList[i];
            bool hasThisOrder=false;
            for(int ii=0;ii<totalOrder;ii++)
              {
               bool sel=OrderSelect(ii,SELECT_BY_POS);
               //Alert(this.ticketList[i]+"--"+OrderTicket());
               if(sel && this.ticketList[i]==OrderTicket())hasThisOrder=true;
              }
            if(hasThisOrder==false)
              {
               //Alert("订单减少了");
               bool sel=OrderSelect(ticketNum,SELECT_BY_TICKET);
               bool res=SendMail("OrderSent浩投资咨询有限公司","订单号 #"+string(ticketNum)+" Opened Succesfully\n"
                                 +"货币兑: "+OrderSymbol()+"\n"
                                 +"操作:  平仓操作\n"
                                 +"平仓时间: "+string(OrderCloseTime())+"(北京时间："+TimeLocal()+")\n"
                                 +"平仓价格: "+str_replace("000","",DoubleToString(OrderClosePrice()))+"\n"
                                 +"操作手数："+string(OrderLots())+"(账号本金：10000美元)\n"
                                 +jianyi+"\n"
                                 );
               if(res==false)
                 {
                  //Alert("Error sending email");
                    }else{
                  this.orderSum=totalOrder;
                  ArrayHelper *ah=new ArrayHelper();
                  ah.RemoveIndexFromIntArray(this.ticketList,i);
                  ah.RemoveIndexFromDoubleArray(this.orderTakeProfitList,i);
                  ah.RemoveIndexFromDoubleArray(this.stopLossList,i);
                  //Alert("平仓操作完成");
                 }
              }
           }

        }
     }
   void doclose()
     {
      //Alert(Hour());
      int totalOrder=OrdersTotal();
      //Alert(DayOfWeek());
      if(4<DayOfWeek()<6 && closeOnFriday==true && Hour()==23 && Minute()>30 && totalOrder>0)
        {
         closeAll();
        }
      //周五 23：30处理

      if(0<DayOfWeek()<6 && closeOnEveryday==true && Hour()==TimeHour(tBegin_Data) && Minute()>TimeMinute(tBegin_Data) && totalOrder>0)
        {
         if(0<DayOfWeek()<5)closeAll(); //周一到周四处理

         if(DayOfWeek()==5 && closeOnFriday==false)closeAll(); //周五 特殊处理

        }
     }

   string str_replace(string search,string replace,string str)
     {
      int start=0,index,i=0,search_len,replace_len;
      search_len=StringLen(search);
      replace_len=StringLen(replace);
      index=StringFind(str,search,start);
      while(index!=-1)
        {
         str=StringSubstr(str,start,index)+replace+StringSubstr(str,index+search_len);
         index=StringFind(str,search,index+replace_len);
        }
      return (str);
     }
  };
//+------------------------------------------------------------------+

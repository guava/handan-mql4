//+------------------------------------------------------------------+
//|                                                fhTimeManager.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CAnimal
  {
public:
   virtual void Sound(){Alert("animal");}
private:
   double            m_legs_count;  // The number of the animal's legs
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CCat : public CAnimal
  {
public:
   virtual void Sound(){ Alert("Myau"); } // PURE is overridden, CCat is not abstract and can be created
  };
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+

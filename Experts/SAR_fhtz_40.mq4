//+------------------------------------------------------------------+
//|                                                     SAR_fhtz.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#include "../Libraries/fhtz2OrderDoSend.mq4"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
input double volumePerOrder=0.1;//手数
double volumePerOrderNow;
input int maxSlippage=5;//最大滑点
input double mxxxxx=1.5;//反手获利倍数
int maxOrderNumber=3;//最大单数
int hourOfStartTime=9;//开始时间
input int hourOfEndTime=19;//结束时间

input bool closeOrderEveryday=true;//是否每日关闭订单
int orderNumber;
double price;

bool cycleFinished=false; //当天完成了购买周期
int orderTimesInCycle=0;
string dateSave;

string orderType;

int stoploss=1500;
double profit=400;
OrderDoSend *ods;
bool pofectTime=false;
bool  orderOpeningNow=false;

double profit2;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
   dateSave=TimeToStr(TimeCurrent(),TIME_DATE);
   ods=new OrderDoSend();
   profit2=profit;
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   string dateCurrent=TimeToStr(TimeCurrent(),TIME_DATE);
   int nowhour=TimeHour(TimeCurrent());
   if(nowhour>(hourOfStartTime-1) && nowhour<hourOfEndTime)pofectTime=true;
//周五11：30关闭所有订单
   if(4<DayOfWeek()<6 && Hour()==23 && Minute()>30)
     {
      closeAllOrders();
     }

//当日关闭所有订单
   if(0<DayOfWeek()<5 && Hour()==23 && Minute()>50 && closeOrderEveryday==true)
     {
      closeAllOrders();
     }

   double green1=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",0,1);
   double red1=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",1,1);
//Alert(green+":"+red);
   ods=new OrderDoSend();


//防止超大滑点
   bool sel=OrderSelect(orderNumber,SELECT_BY_TICKET);
   bool close_price=OrderClosePrice();
//Alert("偷袭:"+close_price+":"+orderOpeningNow);
//if(close_price!=true && orderOpeningNow==true)
   if(OrdersTotal()==0 && orderOpeningNow==true)
     {
      //Alert("偷袭:"+close_price);
      if(orderType=="sell")
        {
         double closePrice=OrderClosePrice();

         if(closePrice>price && orderTimesInCycle<maxOrderNumber && pofectTime && cycleFinished==false)
           {
            //Alert(closePrice+":"+price);
            //Alert(orderTimesInCycle);
            profit=(stoploss*volumePerOrderNow*mxxxxx)/(volumePerOrderNow*2);
            volumePerOrderNow=volumePerOrderNow*2;
            ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+100,profit+100);
            orderNumber=ods.openBuyAction();
            if(orderNumber!=-1)orderOpeningNow=true;
            bool sel2=OrderSelect(orderNumber,SELECT_BY_TICKET);
            price=OrderOpenPrice();
            orderType="buy";
            orderTimesInCycle=orderTimesInCycle+1;
           }
         if(closePrice<price)
           {
            cycleFinished=true;
            profit=profit2;
            Alert("sell："+cycleFinished);
            orderOpeningNow=false;
           }

        }
      if(orderType=="buy")
        {
         if(OrderClosePrice()<price && orderTimesInCycle<maxOrderNumber && pofectTime && cycleFinished==false)
           {
            profit=(stoploss*volumePerOrderNow*mxxxxx)/(volumePerOrderNow*2);
            volumePerOrderNow=volumePerOrderNow*2;
            ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+100,profit+100);
            orderNumber=ods.openSellAction();
            if(orderNumber!=-1)orderOpeningNow=true;
            bool sel2=OrderSelect(orderNumber,SELECT_BY_TICKET);
            price=OrderOpenPrice();
            orderType="sell";
            orderTimesInCycle=orderTimesInCycle+1;
              }else{
            if(OrderClosePrice()>price)
              {
               cycleFinished=true;
               profit=profit2;
               Alert("buy："+cycleFinished+OrderClosePrice()+":"+price);
               orderOpeningNow=false;
              }
           }
        }
     }

   if(dateCurrent==dateSave)
     {
      //red2>0 green1>0 sell
      if(cycleFinished==false)
        {
         int total=OrdersTotal();
         //if(green1>0 && total==0 && pofectTime && orderTimesInCycle==0)
         if(green1>0 && total==0 && pofectTime && orderTimesInCycle<maxOrderNumber)
           {
            Alert("cycle finished:"+cycleFinished+" (orderTimesInCycle):"+orderTimesInCycle);
            ods.SetOrderDoSend(volumePerOrder,maxSlippage,stoploss+100,profit+100);
            orderNumber=ods.openSellAction();
            if(orderNumber!=-1)orderOpeningNow=true;
            price=Bid;
            orderType="sell";
            orderTimesInCycle=orderTimesInCycle+1;
            volumePerOrderNow=volumePerOrder;
           }

         if(red1>0 && total==0 && pofectTime && orderTimesInCycle<maxOrderNumber)
           {
            Alert("cycle finished:"+cycleFinished);
            ods.SetOrderDoSend(volumePerOrder,maxSlippage,stoploss+100,profit+100);
            orderNumber=ods.openBuyAction();
            if(orderNumber!=-1)orderOpeningNow=true;
            price=Ask;
            orderType="buy";
            orderTimesInCycle=orderTimesInCycle+1;
            volumePerOrderNow=volumePerOrder;
           }

        }
        }else{//日期不同开始新周期
      dateSave=dateCurrent;
      cycleFinished=false;
      orderTimesInCycle=0;
      pofectTime=false;
      //closeAllOrders();
      orderOpeningNow=false;
      //volumePerOrderNow=volumePerOrder;//非当日亏损是否double 不设置结果比较好
     }
   int orderCounter1=OrdersTotal();
   if(orderCounter1>0)
     {
      if(orderType=="sell" && red1>0)
        {
         sellStoplossToBuy();
        }
      if(orderType=="buy" && green1>0)
        {
         buyStoplossToSell();
        }
     }
   int orderCounter=OrdersTotal();
   if(orderCounter>0)
     {
      if(orderType=="sell")
        {
         if((price-Bid)>profit*Point || (Bid-price)>stoploss*Point)
           {
            Alert("doing something");
            sellStoplossToBuy();
           }
        }
      if(orderType=="buy")
        {
         if((price-Ask)>stoploss*Point || (Ask-price)>profit*Point)
           {
            buyStoplossToSell();
           }
        }
     }

  }
//+------------------------------------------------------------------+
void sellStoplossToBuy()
  {
   ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+100,profit+100);
   bool closeOK=ods.sellCloseAction(orderNumber);
   if(closeOK)
     {
      orderOpeningNow=false;
      bool sel=OrderSelect(orderNumber,SELECT_BY_TICKET);
      double closePrice=OrderClosePrice();
      Alert(closePrice+":"+price);
      if(closePrice>price && orderTimesInCycle<maxOrderNumber && pofectTime && cycleFinished==false)
        {
         Alert(orderTimesInCycle);
         //profit=(stoploss*volumePerOrderNow*mxxxxx)/(volumePerOrderNow*2);
         //volumePerOrderNow=volumePerOrderNow*2;
         volumePerOrderNow=(stoploss*volumePerOrderNow*mxxxxx)/profit;
         ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+100,profit+100);
         orderNumber=ods.openBuyAction();
         if(orderNumber!=-1)orderOpeningNow=true;
         bool sel2=OrderSelect(orderNumber,SELECT_BY_TICKET);
         price=OrderOpenPrice();
         orderType="buy";
         orderTimesInCycle=orderTimesInCycle+1;
        }
      if(closePrice<price)
        {
         cycleFinished=true;
         profit=profit2;
         Alert("sell："+cycleFinished);
        }

     }
  }
//+------------------------------------------------------------------+
void buyStoplossToSell()
  {
   ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+100,profit+100);
   bool closeOK=ods.buyCloseAction(orderNumber);
   if(closeOK)
     {
      orderOpeningNow=false;
      bool sel=OrderSelect(orderNumber,SELECT_BY_TICKET);
      if(OrderClosePrice()<price && orderTimesInCycle<maxOrderNumber && pofectTime && cycleFinished==false)
        {
         //profit=(stoploss*volumePerOrderNow*mxxxxx)/(volumePerOrderNow*2);
         //volumePerOrderNow=volumePerOrderNow*2;
         volumePerOrderNow=(stoploss*volumePerOrderNow*mxxxxx)/profit;
         ods.SetOrderDoSend(volumePerOrderNow,maxSlippage,stoploss+100,profit+100);
         orderNumber=ods.openSellAction();
         if(orderNumber!=-1)orderOpeningNow=true;
         bool sel2=OrderSelect(orderNumber,SELECT_BY_TICKET);
         price=OrderOpenPrice();
         orderType="sell";
         orderTimesInCycle=orderTimesInCycle+1;
           }else{
         if(OrderClosePrice()>price)
           {
            cycleFinished=true;
            profit=profit2;
            Alert("buy："+cycleFinished+OrderClosePrice()+":"+price);
           }
        }
     }
  }
//+------------------------------------------------------------------+
void closeAllOrders()
  {
   int total=OrdersTotal();
   for(int i=total-1;i>=0;i--)
     {
      bool sel=OrderSelect(i,SELECT_BY_POS);
      int type=OrderType();

      bool result=false;

      if(type==OP_BUY)
        {
         //Close opened long positions
         result=OrderClose(OrderTicket(),OrderLots(),MarketInfo(OrderSymbol(),MODE_BID),5,Red);
           }else{

         result=OrderClose(OrderTicket(),OrderLots(),MarketInfo(OrderSymbol(),MODE_ASK),5,Red);

        }

      if(result==false)
        {
         //Alert("Order ",OrderTicket()," failed to close. Error:",GetLastError());
         Sleep(3000);
        }
     }
  }
//+------------------------------------------------------------------+

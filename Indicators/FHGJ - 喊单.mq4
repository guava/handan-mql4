//修改内容：
//    1、增加了邮件提醒。
//
//修改时间：2016年1月20日 11:06:01
//

#import "VCMail.dll"
bool  VC_SendMail(int nIndicatorSignal,string strSymbol);
#import

#property indicator_chart_window
#property indicator_buffers 5
#property indicator_color1 Green
#property indicator_color2 Red
#property indicator_color3 Red
#property indicator_color4 Aqua



extern bool VCFX_ShowTime=FALSE;           //显示背景时间    
extern bool VCFX_SoundWarning = TRUE;           //警告声音
extern int  vc_g_nStartTime   = 9;              //开始时间
extern int  vc_g_nEndTime     = 19;             //结束时间

extern bool  vc_g_bIsStartupMail=TRUE;         //开启邮件提醒
extern int  vc_g_nStartTimeMail   = 9;              //邮件发送时间
extern int  vc_g_nEndTimeMail     = 19;             //邮件结束时间

extern double  VCFX_Leverage  = 50.0;           //杠杆倍数
extern double  VCFX_BaseLots  = 0.2;            //下单量
extern double  vc_g_dTPP      = 30.0;           //黄金获利点数
extern double  VCFX_MaxLots   = 2.0;            //平台最大单笔下单量限制

extern string  VCFX_Symbol="GBPJPY";       //货币对

extern string MusicBuy  = "alert.wav";    //多单信号
extern string MusicSell = "news.wav";     //空单信号

#define  VC_LABLE_OFFSET       30             //买卖信号当前K线之间的距离

string gs_unused_180="变色龙外汇";

int Timeframe=0;

int gi_220 = 7;
int gi_224 = 17;
int gi_228 = 6;
int g_str2int_232=30;
bool gi_unused_236=TRUE;
double vc_nSARStepSmall = 0.001;
double vc_nSARMaxValume = 0.2;

double vc_iGreen[];
double vc_iRed[];
//double g_ibuf_284[];
//double g_ibuf_288[];
//double g_ibuf_292[];

double vc_iSell[];
double vc_iBuy[];

int gi_300=315532800;
int g_bars_304=240;
int gi_308 = 0;
int gi_312 = 0;
double gd_316=0.0;
int gi_324 = -1;
int gi_328 = -1;
int gi_332 = 0;
double g_price_336 = 0.0;
double g_price_344 = 0.0;
double g_price_352 = 0.0;
int gi_unused_360=0;
int g_bars_364=300;
int gi_368 = 0;
int gi_372 = 0;
double gd_376=0.0;
double gd_384;
int g_datetime_392;
int g_datetime_396;

string gcf_Logo="方浩智能分析系统 V4.1";    //这里设定系统名称

string gcf_Logo1 = "方浩智能分析系统 V4.1 试用版";
string gcf_Logo2 = "方浩智能分析系统 V4.1 正式版";

string gcf_Email="EMail：fanghao@fanghaotz.com";           //这里设定Email号码
string gcf_website="网址：www.fanghaotz.com";      //这里设定网站地址

string vc_g_strLableLot ="手数：";   //应该下的手数
string vc_g_strLableTP  ="盈利：";   //盈利
int vc_g_nLableLot_X = 5;
int vc_g_nLableLot_Y = 100;
int vc_g_nLableLotVolume_X = 40;
int vc_g_nLableLotVolume_Y = 100;


string   VC_GOLD="GOLD";       //交易品种
double   vc_g_dAccumulatedLoss   = 0.0;     //亏损累计
double   vc_g_dFixedProfit       = 0.0;     //亏损累计

double   vc_TPP   = 30.0;  //获利点数
double   vc_STP   = -1.0;  //变色点数
double   vc_Lots  = 0.2;   //应下手数

int vc_g_nLableTP_X = 5;
int vc_g_nLableTP_Y = 115;
int vc_g_nLableTPVolume_X = 40;
int vc_g_nLableTPVolume_Y = 115;


string color_now="";
#define VC_WINGDINGS_SYMBOLS     167   //Z指标线符号
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int init()
  {

//if (StringSubstr(Symbol(), 0, 6)   ==   "AUDCAD") magic = 101101;
//if (StringSubstr(Symbol(), 0, 6)   ==   "AUDJPY") magic = 101102;
//if (StringSubstr(Symbol(), 0, 6)   ==   "AUDNZD") magic = 101103;
//if (StringSubstr(Symbol(), 0, 6)   ==   "AUDUSD") magic = 101104;
//if (StringSubstr(Symbol(), 0, 6)   ==   "CHFJPY") magic = 101105;
//if (StringSubstr(Symbol(), 0, 6)   ==   "EURAUD") magic = 101106;
//if (StringSubstr(Symbol(), 0, 6)   ==   "EURCAD") magic = 101107;
//if (StringSubstr(Symbol(), 0, 6)   ==   "EURCHF") magic = 101108;
//if (StringSubstr(Symbol(), 0, 6)   ==   "EURGBP") magic = 101109;
//if (StringSubstr(Symbol(), 0, 6)   ==   "EURJPY") magic = 101110;
//if (StringSubstr(Symbol(), 0, 6)   ==   "EURUSD") magic = 101111;
//if (StringSubstr(Symbol(), 0, 6)   ==   "GBPCHF") magic = 101112;
//if (StringSubstr(Symbol(), 0, 6)   ==   "GBPJPY") magic = 101113;
//if (StringSubstr(Symbol(), 0, 6)   ==   "GBPUSD") magic = 101114;
//if (StringSubstr(Symbol(), 0, 6)   ==   "GBPAUD") magic = 101115;
//if (StringSubstr(Symbol(), 0, 6)   ==   "GBPNZD") magic = 101116;
//if (StringSubstr(Symbol(), 0, 6)   ==   "NZDJPY") magic = 101117;
//if (StringSubstr(Symbol(), 0, 6)   ==   "NZDUSD") magic = 101118;
//if (StringSubstr(Symbol(), 0, 6)   ==   "USDCHF") magic = 101119;
//if (StringSubstr(Symbol(), 0, 6)   ==   "USDJPY") magic = 101120;
//if (StringSubstr(Symbol(), 0, 6)   ==   "USDCAD") magic = 101121;
//if (StringSubstr(Symbol(), 0, 6)   ==   "XAUUSD") magic = 101122;
//if (StringSubstr(Symbol(), 0, 4)   ==   "GOLD")   magic = 101123;
//if (StringSubstr(Symbol(), 0, 6)   ==   "XAGUSD") magic = 101124;
//if (StringSubstr(Symbol(), 0, 6)   ==   "SILVER") magic = 101125;
//if (magic == 0)
//   magic = 701999;

   string ls_0;
   ObjectsDeleteAll();
   VC_DrawLable("Email",gcf_Email,5,30,9);
   VC_DrawLable("website",gcf_website,5,15,9);
//VC_DrawLable("Lots", vc_g_strLableLot, vc_g_nLableLot_X, vc_g_nLableLot_Y, 9);
//VC_DrawLable("TP", vc_g_strLableTP, vc_g_nLableTP_X, vc_g_nLableTP_Y, 9);

   if(StringLen(ls_0)>10) gi_unused_236=FALSE;
   g_str2int_232=StrToInteger(ls_0);
   SetIndexBuffer(0,vc_iGreen);
   SetIndexStyle(0,DRAW_ARROW);
   SetIndexArrow(0,VC_WINGDINGS_SYMBOLS);

   SetIndexBuffer(1,vc_iRed);
   SetIndexStyle(1,DRAW_ARROW);
   SetIndexArrow(1,VC_WINGDINGS_SYMBOLS);

   IndicatorShortName("开仓指标");

   SetIndexBuffer(2,vc_iBuy);
   SetIndexStyle(2,DRAW_ARROW,EMPTY,2);
   SetIndexArrow(2,233);

   SetIndexBuffer(3,vc_iSell);
   SetIndexStyle(3,DRAW_ARROW,EMPTY,2);
   SetIndexArrow(3,234);

   CTime();
   VC_ReadConfig();
   //bool ssssuu2=VC_SendMail(0,"建议操作");
   return (0);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int deinit()
  {
   Comment("");
   DelObj();
   ObjectsDeleteAll(0,OBJ_LABEL);
   ObjectsDeleteAll(0,OBJ_TEXT);
   ObjectsDeleteAll(0,OBJ_ARROW);
   ObjectsDeleteAll(0,OBJ_RECTANGLE);
   ObjectsDeleteAll(0,OBJ_TREND);

   ObjectDelete("title");
   ObjectDelete("website");
   ObjectDelete("Email");
   return (0);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void DelObj()
  {
   ObjectDelete("TL1");
   ObjectDelete("TL2");
   ObjectDelete("MIDL");
  }
//int start() 
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])

  {
   DrawObj("title",gcf_Logo,420,5,Yellow);
//Alert(Bid);
   vc_Lots=VC_GetLots(VCFX_Symbol);
   if(vc_Lots<= 0.1)
      vc_Lots = VCFX_BaseLots;

//DrawObj("Lot", NormalizeDouble(vc_Lots, 1), vc_g_nLableLotVolume_X, vc_g_nLableLotVolume_Y, Yellow);

//DrawObj("TPA", NormalizeDouble(vc_g_dAccumulatedLoss, 1), vc_g_nLableTPVolume_X, vc_g_nLableTPVolume_Y, Yellow);

   int lia_4[];
   int lia_8[];
   double iMa_100;
   double diSAR_1;
   int li_28;
   int index_32=0;
   int ind_counted_36=IndicatorCounted();
   int li_40=Bars-ind_counted_36;
   int index_44=0;

   for(index_32=0; index_44<li_40; index_44++)
     {

      iMa_100 = iMA(NULL, Timeframe, 50, 0, MODE_EMA, PRICE_MEDIAN, index_44 + 1);
      diSAR_1 = iSAR(NULL, Timeframe, vc_nSARStepSmall, vc_nSARMaxValume, index_44 + 1);
      //Print("0、ima_12 = ", ima_12, ", ----- ima_20 = ", ima_20); 
      VC_DrawHLine("VC_ChangeColor",diSAR_1);  //画止损线

      if(diSAR_1>High[index_44+1])//&& ima_12 <= ima_20 )
        {
         vc_iGreen[index_44]  = iMa_100;
         vc_iRed[index_44]    = -1;
         //Print("1、vc_iGreen = ", vc_iGreen[index_44], ", vc_iRed = ", vc_iRed[index_44],", g_ibuf_288 = ", g_ibuf_288[index_44], ", g_ibuf_292 = ", g_ibuf_292[index_44]); 
        }
      else
        {

         vc_iRed[index_44]    = iMa_100;
         vc_iGreen[index_44]  = -1;
         //Print("2、vc_iGreen = ", vc_iGreen[index_44], ", vc_iRed = ", vc_iRed[index_44],", g_ibuf_288 = ", g_ibuf_288[index_44], ", g_ibuf_292 = ", g_ibuf_292[index_44]); 
        }
     }
//guava

   index_44=0;
   double diSAR_2=iSAR(NULL,Timeframe,vc_nSARStepSmall,vc_nSARMaxValume,index_44+1);
   if(diSAR_1>High[index_44+1])
     {
      vc_iGreen[index_44]  = iMa_100;
      vc_iRed[index_44]    = -1;
      if(Bid>diSAR_2)
        {
         vc_iRed[index_44]    = iMa_100;
         vc_iGreen[index_44]  = -1;
        }
     }
   else
     {
      vc_iRed[index_44]    = iMa_100;
      vc_iGreen[index_44]  = -1;
      if(Bid<diSAR_2)
        {
         vc_iGreen[index_44]  = iMa_100;
         vc_iRed[index_44]    = -1;
        }
     }

//guava
//   RefreshRates();
   gd_384=WindowPriceMin()+20.0*Point;
   if(VCFX_ShowTime)
     {
      for(index_44=Bars; index_44>0; index_44--)
        {
         if(TimeHour(Time[index_44])==gi_224 && TimeMinute(Time[index_44])==0)
            li_28=Time[index_44];

         if(TimeHour(Time[index_44])==gi_220 && TimeMinute(Time[index_44])==0)
            drawt(TimeToStr(Time[index_44],TIME_DATE),"                            "+TimeToStr(Time[index_44],TIME_DATE)+""+week(Time[index_44]),Time[index_44],Low[index_44+1]-30.0*Point);

         if(TimeHour(Time[index_44])==gi_220 && TimeMinute(Time[index_44])==0 && li_28!=0)
           {
            drawx(TimeToStr(li_28,TIME_DATE)+"-Close",Time[index_44],li_28,DarkSlateGray);
            li_28=0;
           }
        }
      if(TimeHour(Time[0])>gi_224 || TimeHour(Time[0])<gi_220 && li_28!=0)
         drawx(TimeToStr(li_28,TIME_DATE)+"-Close",Time[0],li_28,DarkSlateGray);
      WindowRedraw();
     }

   if(VCFX_SoundWarning==TRUE && vc_iRed[0]>vc_iGreen[0] && vc_iRed[1]<=vc_iGreen[1] && gi_300!=Time[0])
     {
      vc_iBuy[index_44]=Low[index_44]-VC_PointToPrice(VC_LABLE_OFFSET);
      VC_SendSignal(1);
      PlaySound(MusicBuy);
      gi_300=Time[0];
     }

   if(VCFX_SoundWarning==TRUE && vc_iRed[0]<vc_iGreen[0] && vc_iRed[1]>=vc_iGreen[1] && gi_300!=Time[0])
     {
      vc_iSell[index_44]=High[index_44]+VC_PointToPrice(VC_LABLE_OFFSET);
      VC_SendSignal(0);
      PlaySound(MusicSell);
      gi_300=Time[0];
     }

//guava
//Print(TimeHour(TimeCurrent())+":"+vc_g_nStartTimeMail+":"+vc_g_nEndTimeMail);
   if(vc_g_bIsStartupMail && TimeHour(TimeCurrent())>vc_g_nStartTimeMail && TimeHour(TimeCurrent())<vc_g_nEndTimeMail)
     {
     //Print(color_now);
      if(vc_iRed[0]>0)
        {
         string color_jit="red";
         if(color_jit!=color_now && vc_iRed[0]!=vc_iRed[1] && color_now!="")
           {
            bool ssss=VC_SendMail(0,"（"+TimeToStr(TimeGMT()-TimeGMTOffset())+"）镑日在M5时间显示做多信号；");
            Print("send mail:buy");
            color_now=color_jit;
           }
           }else{
         string color_jit2="green";
         if(color_jit2!=color_now && vc_iRed[0]!=vc_iRed[1] && color_now!="")
           {
            bool ssss2=VC_SendMail(0,"（"+TimeToStr(TimeGMT()-TimeGMTOffset())+"）镑日在M5时间显示做空信号；");
            Print("send mail:sell");
            color_now=color_jit2;
           }
        }
       
     }
//-------------画趋势线-------------
/*   if (g_bars_304 == 0 || Bars < g_bars_304) 
      g_bars_364 = Bars;
   else 
      g_bars_364 = g_bars_304;
      
   if (gi_308 > 0) 
      gi_368 = gi_308;
   else 
   {
      switch (Period()) 
      {
      case PERIOD_M1:
         gi_368 = 12;
         break;
      case PERIOD_M5:
         gi_368 = 48;
         break;
      case PERIOD_M15:
         gi_368 = 24;
         break;
      case PERIOD_M30:
         gi_368 = 24;
         break;
      case PERIOD_H1:
         gi_368 = 12;
         break;
      case PERIOD_H4:
         gi_368 = 15;
         break;
      case PERIOD_D1:
         gi_368 = 10;
         break;
      case PERIOD_W1:
         gi_368 = 6;
         break;
      default:
         DelObj();
         return (-1);
      }
   }
   gi_312 = 2;
   gi_324 = -1;
   gi_328 = -1;
   gi_332 = 0;
   while (gi_324 == -1 || gi_328 == -1 && gi_312 < g_bars_364) {
      if (gi_332 < 1 && gi_312 == iLowest(Symbol(), Period(), MODE_LOW, gi_368 * 2 + 1, gi_312 - gi_368)) {
         if (gi_332 == 0) {
            gi_332 = -1;
            gi_324 = gi_312;
            g_price_336 = Low[gi_324];
         } else {
            gi_328 = gi_312;
            g_price_344 = Low[gi_328];
         }
      }
      if (gi_332 > -1 && gi_312 == iHighest(Symbol(), Period(), MODE_HIGH, gi_368 * 2 + 1, gi_312 - gi_368)) {
         if (gi_332 == 0) {
            gi_332 = 1;
            gi_324 = gi_312;
            g_price_336 = High[gi_324];
         } else {
            gi_328 = gi_312;
            g_price_344 = High[gi_328];
         }
      }
      gi_312++;
   }
   if (gi_324 == -1 || gi_328 == -1) {
      DelObj();
      return (-1);
   }
   gd_316 = (g_price_344 - g_price_336) / (gi_328 - gi_324);
   g_price_336 -= gi_324 * gd_316;
   gi_324 = 0;
   gi_372 = 0;
   gd_376 = 0;
   if (gi_332 == 1) {
      g_price_352 = Low[2] - 2.0 * gd_316;
      for (index_44 = 3; index_44 <= gi_328; index_44++)
         if (Low[index_44] < g_price_352 + gd_316 * index_44) g_price_352 = Low[index_44] - index_44 * gd_316;
      if (Low[0] < g_price_352) {
         gi_372 = 0;
         gd_376 = g_price_352;
      }
      if (Low[1] < g_price_352 + gd_316) {
         gi_372 = 1;
         gd_376 = g_price_352 + gd_316;
      }
      if (High[0] > g_price_336) {
         gi_372 = 0;
         gd_376 = g_price_336;
      }
      if (High[1] > g_price_336 + gd_316) {
         gi_372 = 1;
         gd_376 = g_price_336 + gd_316;
      }
   } else {
      g_price_352 = High[2] - 2.0 * gd_316;
      for (index_44 = 3; index_44 <= gi_328; index_44++)
         if (High[index_44] > g_price_352 + gd_316 * index_44) g_price_352 = High[index_44] - index_44 * gd_316;
      if (Low[0] < g_price_336) {
         gi_372 = 0;
         gd_376 = g_price_336;
      }
      if (Low[1] < g_price_336 + gd_316) {
         gi_372 = 1;
         gd_376 = g_price_336 + gd_316;
      }
      if (High[0] > g_price_352) {
         gi_372 = 0;
         gd_376 = g_price_352;
      }
      if (High[1] > g_price_352 + gd_316) {
         gi_372 = 1;
         gd_376 = g_price_352 + gd_316;
      }
   }
   g_price_344 = g_price_336 + g_bars_364 * gd_316;
   g_datetime_392 = Time[gi_324];
   g_datetime_396 = Time[g_bars_364];
   if (gd_376 != 0.0) g_ibuf_284[gi_372] = gd_376;
   DelObj();
   ObjectCreate("TL1", OBJ_TREND, 0, g_datetime_396, g_price_352 + gd_316 * g_bars_364, g_datetime_392, g_price_352);
   ObjectSet("TL1", OBJPROP_COLOR, Fuchsia);
   ObjectSet("TL1", OBJPROP_WIDTH, 1);
   ObjectSet("TL1", OBJPROP_STYLE, STYLE_SOLID);
   ObjectCreate("TL2", OBJ_TREND, 0, g_datetime_396, g_price_344, g_datetime_392, g_price_336);
   ObjectSet("TL2", OBJPROP_COLOR, Fuchsia);
   ObjectSet("TL2", OBJPROP_WIDTH, 1);
   ObjectSet("TL2", OBJPROP_STYLE, STYLE_SOLID);
   ObjectCreate("MIDL", OBJ_TREND, 0, g_datetime_396, (g_price_344 + g_price_352 + gd_316 * g_bars_364) / 2.0, g_datetime_392, (g_price_336 + g_price_352) / 2.0);
   ObjectSet("MIDL", OBJPROP_COLOR, Fuchsia);
   ObjectSet("MIDL", OBJPROP_WIDTH, 1);
   ObjectSet("MIDL", OBJPROP_STYLE, STYLE_DOT);
   */
   return (0);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void drawx(string a_name_0,int a_datetime_8,int a_datetime_12,color a_color_16)
  {
   RefreshRates();
   if(ObjectFind(a_name_0)==-1)
     {
      ObjectCreate(a_name_0,OBJ_RECTANGLE,0,a_datetime_8,0,a_datetime_12,3000);
      ObjectSet(a_name_0,OBJPROP_COLOR,a_color_16);
      ObjectSet(a_name_0,OBJPROP_STYLE,STYLE_SOLID);
      ObjectSet(a_name_0,OBJPROP_BACK,TRUE);
      ObjectSet(a_name_0,OBJPROP_RAY,FALSE);
      ObjectSet(a_name_0,OBJPROP_WIDTH,2);
      return;
     }
   ObjectMove(a_name_0,0,a_datetime_8,0);
   ObjectMove(a_name_0,1,a_datetime_12,3000);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void drawt(string strName,string strText,int nTime,double dPrice)
  {
   RefreshRates();
   if(ObjectFind(strName)==-1)
     {
      ObjectCreate(strName,OBJ_TEXT,0,nTime,dPrice,nTime,dPrice);
      ObjectSetText(strName,strText,9,"Arial",Yellow);
      ObjectSet(strName,OBJPROP_WIDTH,1);
      return;
     }
   ObjectMove(strName,0,nTime,dPrice);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string week(int ai_0)
  {
   if(TimeDayOfWeek(ai_0) == 1) return ("周一");
   if(TimeDayOfWeek(ai_0) == 2) return ("周二");
   if(TimeDayOfWeek(ai_0) == 3) return ("周三");
   if(TimeDayOfWeek(ai_0) == 4) return ("周四");
   if(TimeDayOfWeek(ai_0) == 5) return ("周五");
   if(TimeDayOfWeek(ai_0) == 6) return ("周六");
   if(TimeDayOfWeek(ai_0) == 7) return ("周日");
   return ("");
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CTime()
  {
   if(GlobalVariableCheck("timezone")==FALSE) GlobalVariableSet("timezone",gi_228);
   gi_228 = GlobalVariableGet("timezone");
   gi_220 = 12 - gi_228;
   gi_224 = 22 - gi_228;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void VC_DrawLable(string strName,string strText,int nX,int nY,int nFontSize)
  {
   RefreshRates();
   ObjectCreate(strName,OBJ_LABEL,0,0,0);
   ObjectSetText(strName,strText,nFontSize,"Arial",Yellow);
   ObjectSet(strName,OBJPROP_CORNER,0);
   ObjectSet(strName,OBJPROP_XDISTANCE,nX);
   ObjectSet(strName,OBJPROP_YDISTANCE,nY);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void DrawObj(string strName,string strText,int nX,int nY,color crColor)
  {
   if(ObjectFind(strName)!=0)
     {
      ObjectCreate(strName,OBJ_LABEL,0,0,0);
      ObjectSetText(strName,strText,10,"Arial",crColor);
      ObjectSet(strName,OBJPROP_XDISTANCE,nX);
      ObjectSet(strName,OBJPROP_YDISTANCE,nY);
     }
  }
//把价格转换成点数。
double VC_PriceToPoint(double dPrice)
  {
   double dPoint=0.0;

   if(5 ==  MarketInfo(Symbol(), MODE_DIGITS) ||
      3 ==  MarketInfo(Symbol(), MODE_DIGITS) ||
      ("XAUUSD"==StringSubstr(Symbol(),0,6) && 2==MarketInfo(Symbol(),MODE_DIGITS)) || 
      ("GOLD"==StringSubstr(Symbol(),0,4) && 2==MarketInfo(Symbol(),MODE_DIGITS)))
     {
      dPoint=MarketInfo(Symbol(),MODE_POINT)*10;
     }
   else
     {
      dPoint=MarketInfo(Symbol(),MODE_POINT);
     }
   return( dPrice * (1 / dPoint));
  }
//功能：把点数转换成价格。
//参数：
//    double dPoints：最新开仓价格。
//返回值：
//    返回价格。

double VC_PointToPrice(double dPoints)
  {
   double dPoint=0.0;

   if(5==MarketInfo(Symbol(),MODE_DIGITS) ||
      3==MarketInfo(Symbol(),MODE_DIGITS) ||
      ("XAUUSD"==StringSubstr(Symbol(),0,6) && 2==MarketInfo(Symbol(),MODE_DIGITS)) || 
      ("GOLD"==StringSubstr(Symbol(),0,4) && 2==MarketInfo(Symbol(),MODE_DIGITS)))
     {
      return( dPoints *  MarketInfo(Symbol(), MODE_POINT) * 10 );
     }
   else
     {
      return( dPoints * MarketInfo(Symbol(), MODE_POINT) );
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double VC_GetLoss(string strSymbol)
  {

   double   dTakeProfit = 0.0;    //止损/获利订单的盈利金额。
   double   dTotalTP    = 0.0;    //总利润
   int      nTotal      = 0;

   nTotal=OrdersHistoryTotal();

   if(0<nTotal)
     {
      for(int i=nTotal-1; i>=0; --i)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY)==true)
           {
            if(TimeDay(OrderCloseTime())==Day())
              {
               if(StringSubstr(OrderSymbol(),0,6)==strSymbol || (StringSubstr(OrderSymbol(),0,4)==strSymbol && strSymbol==VC_GOLD))
                 {
                  dTakeProfit=OrderProfit();
                  dTotalTP+=dTakeProfit;

                  //    Print(strSymbol, ": 1、OrderProfit() = ", dTakeProfit);
                  break;
                 }
              }
           }
        }
     }
   return(dTotalTP);
  }
//功能：计算下单量
//描述：
//    盈利金额固定时，根据上一单的亏损来计算额度/点数来计算下一单的下单量。
//公式：下单量 = (6000 + (已亏损点数 * 10)) / (应获利点数 * 一标手获利金额）
//修改人：方正欧
//创建时间：2014年7月30日18:47:34
//修改时间：2014年9月30日03:13:49
double VC_GetLots(string strSymbol)
  {
//return 1.0;
   double dVolume=0.0;
   double nLoss=VC_GetLoss(strSymbol);

   vc_g_dAccumulatedLoss+= nLoss;
   vc_g_dAccumulatedLoss = NormalizeDouble(vc_g_dAccumulatedLoss,1);
   dVolume=(vc_g_dFixedProfit+vc_g_dAccumulatedLoss)/(vc_g_dTPP*10.0);
//   Print("XAUUSD VC_GetLots()  上一单亏损： ",nLoss, ",  累计亏损： ", vc_g_dAccumulatedLoss, ",  单量：", dVolume);

   double dCurrentBalance=0.0; // /2;
   dCurrentBalance=AccountBalance();

//当下单所需保证金大于总资金50%时，下单手数为总资金的50%。
   if((dCurrentBalance*0.5)<=(1000.0*dVolume*(100.0/VCFX_Leverage)))
      dVolume=NormalizeDouble(dCurrentBalance/2.0/1000.0*(VCFX_Leverage/100.0),1);

//   dVolume = dVolume - VC_GetNetVolume(strSymbol);
   if(dVolume>VCFX_MaxLots)//平台对最大下单量的限制
      dVolume=VCFX_MaxLots;
   dVolume=NormalizeDouble(dVolume,1); //改成整数后增加3%的收益
                                       //   Print("-----------------dVolume： ",dVolume);

   return(dVolume);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void VC_DrawHLine(string strName,double price)
  {
   RefreshRates();
   ObjectDelete(strName);
   ObjectCreate(strName,OBJ_HLINE,0,0,price);
   ObjectSet(strName,OBJPROP_COLOR,Yellow);
   ObjectSet(strName,OBJPROP_STYLE,STYLE_DOT);
   ObjectSet(strName,OBJPROP_PRICE1,price);
   ObjectSet(strName,OBJPROP_WIDTH,1);
   ObjectSet(strName,OBJPROP_BACK,false);
   ObjectSet(strName,OBJPROP_RAY,false);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool VC_ReadConfig()
  {
//-------读取文件列表名------------------------------------       
   int Handle;                         // File descriptor

   string   strItem;
   string   strFile;
   int      nType=0;
   strFile="VCFX_Configure.txt";
   Handle=FileOpen(strFile,FILE_CSV|FILE_READ);// File opening
   if(Handle<0) // File opening fails
     {
      if(GetLastError()==4103)
         Alert("No file named ",strFile);
      else
         Alert("Error while opening file ",strFile);
      return(false);                          // Exit start()      
     }
//--------------------------------------------------------- 5 --
   nType=FileReadNumber(Handle);    // OpenTime

   if(1000==nType)
     {
      gcf_Logo=gcf_Logo1;
     }
   else
      gcf_Logo=gcf_Logo2;

   FileClose(Handle);                        // Close file
   return(true);

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void VC_SendSignal(int nSignal)
  {
//      Print("0-----**-*-*-*-16> ", TimeHour(TimeCurrent())," >= 6");
   if(vc_g_bIsStartupMail && TimeHour(TimeCurrent())>vc_g_nStartTimeMail && TimeHour(TimeCurrent())<vc_g_nEndTimeMail)
     {
      //      Print("1-----**-*-*-*- ");
      string strSymbol=Symbol();
      switch(Period())
        {
         case 1:
           {
            strSymbol=strSymbol+"-1分钟图";
            break;
           }
         case 5:
           {
            strSymbol=strSymbol+"-5分钟图";
            break;
           }
         case 15:
           {
            strSymbol=strSymbol+"-15分钟图";
            break;
           }
         case 30:
           {
            strSymbol=strSymbol+"-30分钟图";
            break;
           }
         case 60:
           {
            strSymbol=strSymbol+"-1小时图";
            break;
           }
         case 240:
           {
            strSymbol=strSymbol+"-4小时图";
            break;
           }
         case 1440:
           {
            strSymbol=strSymbol+"-日线图";
            break;
           }
         case 10080:
           {
            strSymbol=strSymbol+"-周线图";
            break;
           }
         default:
           {
            strSymbol=strSymbol+"-月线图";
            break;
           }
        }

     }
  }
//+------------------------------------------------------------------+

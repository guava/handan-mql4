//+------------------------------------------------------------------+
//|                                               fhtz_yesterday.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#include "../guava/fhTimeManager.mq4"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class YesterdaySituation
  {

protected:
   double            higher;
   double            lower;
   TimeManager      *time_manager;
   bool              higher_lower_lock;
   bool              higher_lower_too_big;
   int               hourOfStartTime;
   int               higher_low_point;
   bool              var_YesterdaySituation_isopen;
public:
   void YesterdaySituation(TimeManager *tm,int startTime,int var_higher_low_point,bool YesterdaySituation_isopen)
     {
      double higher=0;
      double   lower=100000;
      time_manager=tm;
      higher_lower_lock=false;
      higher_lower_too_big=false;
      hourOfStartTime=startTime;
      higher_low_point=var_higher_low_point;
      var_YesterdaySituation_isopen=YesterdaySituation_isopen;
     }
   void everTickDo2()
     {
      if(time_manager.getPofectTime())
        {
         if(Ask>higher)higher=Ask;
         if(Bid<lower)lower=Bid;
         if(higher_lower_lock==true)
           {
            higher_lower_lock=false;
            higher_lower_too_big=false;
           }
        }
      int nowhour=TimeHour(TimeCurrent());
      if(nowhour==hourOfStartTime && higher_lower_lock==false)
        {

         if((higher-lower)>higher_low_point*Point)
           {
            Alert(higher+":"+lower);
            higher_lower_too_big=true;
           }
         higher=0;
         lower=10000;
         higher_lower_lock=true;
        }
     }

   void everTickDo()
     {

      if(Ask>higher)higher=Ask;
      if(Bid<lower)lower=Bid;

      int nowhour=TimeHour(TimeCurrent());
      if(nowhour==hourOfStartTime)
        {

         if((higher-lower)>higher_low_point*Point)
           {
            Alert(higher+":"+lower);
            higher_lower_too_big=true;
           }
         higher=0;
         lower=10000;
        }
     }
   bool get_is_higher_lower_too_big()
     {
      if(var_YesterdaySituation_isopen==false)return true;
      return !higher_lower_too_big;
     }
  };
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                              fhtzforcomplite.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#property indicator_chart_window
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int file_handle;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
//---
   file_handle=FileOpen("complite.csv",FILE_READ|FILE_WRITE|FILE_CSV);
   return(INIT_SUCCEEDED);
  }
int accoutnumber=0;
double accountBalance=0;
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---

//--- return value of prev_calculated for next call
//Alert(AccountBalance()-50000);
//Alert(AccountProfit());
//Alert(ACCOUNT_BALANCE+"---guava");

   if(accoutnumber!=AccountNumber()&& accountBalance!=AccountBalance() )
     {
      //Sleep(5000);
      double jl=(AccountBalance()-50000)/50000;
      //Alert(AccountNumber()+":"+jl*100+"%");
      FileWrite(file_handle,AccountNumber(),jl*100+"%");
      accoutnumber=AccountNumber();
      accountBalance=AccountBalance();
      Alert("请更换账号2");
     }

   return(rates_total);
  }
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
   if(file_handle!=INVALID_HANDLE)
        {
         FileClose(file_handle);
        }
  }
//+------------------------------------------------------------------+

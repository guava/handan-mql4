//+------------------------------------------------------------------+
//|                                                         test.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <MovingAverages.mqh>
//--- indicator settings
//#property  indicator_separate_window 
#property indicator_chart_window  //主图
#property  indicator_buffers 4
#property  indicator_color1  Red//Silver
#property  indicator_color2  Green
#property  indicator_color3  Yellow 
#property  indicator_color4  White 
//--- input parameters
int      step=7;
int stepHL=12;
//--- buffers
double openp[];
double closep[];
double hight[];
double blow[];
int file_handle;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   IndicatorBuffers(4);
   SetIndexBuffer(0,openp);
   SetIndexBuffer(1,closep);
   SetIndexBuffer(2,hight);
   SetIndexBuffer(3,blow);

//--- indicator line
   SetIndexStyle(0,DRAW_LINE);
//SetIndexBuffer(0,openp);
//--- indicator line
   SetIndexStyle(1,DRAW_LINE);
//SetIndexBuffer(1,closep);

   SetIndexStyle(2,DRAW_LINE);
//SetIndexBuffer(2,hight);
   SetIndexStyle(3,DRAW_LINE);

   IndicatorShortName("test");
   SetIndexLabel(0,"test");
   SetIndexDrawBegin(0,stepHL);
   SetIndexDrawBegin(1,stepHL);
   SetIndexDrawBegin(2,stepHL);
   SetIndexDrawBegin(3,stepHL);
//---

   file_handle=FileOpen("test_tj.csv",FILE_READ|FILE_WRITE|FILE_CSV);
   FileWrite(file_handle,"test红","test绿","test绿-红","开盘","收盘","最高","最低","fhtz2绿","fhtz2红","MACD","CCI","RSI","ATP","ROC","DMA","test2");

   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+

int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
   return(rates_total);
  }

int times_guava=1;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnStart()
  {
   if(times_guava<2)
     {
      int i;
      for(i=0; i<3600; i++)
        {
/*
      openp[i]=iMA(NULL,PERIOD_M30,step,0,MODE_EMA,PRICE_OPEN,i);
      closep[i]=iMA(NULL,PERIOD_M30,step,0,MODE_EMA,PRICE_CLOSE,i);
      hight[i]=iMA(NULL,PERIOD_M30,8,0,MODE_EMA,PRICE_HIGH,i);
      blow[i]=iMA(NULL,PERIOD_M30,8,0,MODE_EMA,PRICE_LOW,i);
      */
         //openp[i]=iMA(NULL,NULL,step,0,MODE_EMA,PRICE_OPEN,i);
         //closep[i]=iMA(NULL,NULL,step,0,MODE_EMA,PRICE_CLOSE,i);
         //hight[i]=iMA(NULL,NULL,stepHL,0,MODE_EMA,PRICE_HIGH,i);
         //blow[i]=iMA(NULL,NULL,stepHL,0,MODE_EMA,PRICE_LOW,i);
         double red=iMA(NULL,NULL,step,0,MODE_EMA,PRICE_OPEN,i);
         double green=iMA(NULL,NULL,step,0,MODE_EMA,PRICE_CLOSE,i);
         double green_red=green-red;
         double open_=Open[i];
         double close_=Close[i];
         double high_=High[i];
         double low_=Low[i];
         double green0=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",0,i);
         double red0=iCustom(NULL,0,"fhtz2",true,true,9,19,false,9,19,50.0,0.2,30.0,2.0,"GBPJPY","alert.wav","news.wav",1,i);
         double indicate_macd=iCustom(NULL,0,"MACD",12,26,9,0,i);
         double indicate_cci=iCustom(NULL,0,"CCI",14,0,i);
         double indicate_rsi=iCustom(NULL,0,"RSI",14,0,i);
         double indicate_atr=iCustom(NULL,0,"ATR",14,0,i);
         double indicate_roc=iCustom(NULL,0,"ROC1",10,false,0,i);
         double indicate_dma=iCustom(NULL,0,"DMA",3,3,7,5,25,5,0,1,1,1,2,i);
         
         double indicate_test2=iCustom(NULL,0,"test2",3,i);
         //FileWrite("test红","test绿","test绿-红","开盘","收盘","最高","最低","fhtz2绿","fhtz2红","MACD","CCI","RSI","ATP","ROC","DMA");
         FileWrite(file_handle,red,green,green_red,open_,close_,high_,low_,green0,red0,indicate_macd,indicate_cci,indicate_rsi,indicate_atr,indicate_roc,indicate_dma,indicate_test2);
        }
      Alert("统计完成");
      if(file_handle!=INVALID_HANDLE)
        {
         FileClose(file_handle);
        }
      times_guava=times_guava+1;
     }

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {

  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                       Locker.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class Locker
  {
protected:
   bool              isbarsLock;
   int               barsnow;
public:
   void Locker()
     {
      isbarsLock=false;
      barsnow=Bars;
     }
   void barsChanged()
     {
      if(barsnow<Bars)
        {
         isbarsLock=false;
         barsnow=Bars;
        }
     }
     void set_lock(){
      isbarsLock=true;
     }
     bool get_lock(){
      return isbarsLock;
     }
  };
//+------------------------------------------------------------------+
